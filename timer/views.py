# -*- coding: utf-8 -*-
from django.db.models import Q

from heizmanager.render import render_response, render_redirect
from django.http import JsonResponse
import pytz
from datetime import datetime, timedelta
import heizmanager.cache_helper as ch
from pytz.tzinfo import StaticTzInfo
from heizmanager.models import Haus, Raum, Etage, Regelung
from heizmanager.mobile.m_setup import set_regelung_entity, get_regelung_entity
import logging


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def is_hidden_offset():
    return False


def calculate_always_anew():
    return False


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/timer/'>Timer</a>" % str(haus.id)


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    context = dict()
    params = haus.get_module_parameters()

    if request.method == "GET":
        context['haus'] = haus
        context['r_m_active'] = params.get('timer_right_menu', True)
        return render_response(request, "m_settings_timer.html", context)

    if 'right_menu_ajax' in request.POST:
        params = haus.get_module_parameters()
        params['timer_right_menu'] = bool(int(request.POST['right_menu_ajax']))
        haus.set_module_parameters(params)
        return JsonResponse({'message': 'done'})

    return render_redirect(request, '/m_setup/%s/timer/' % haus.id)


def get_offset(haus, raum=None):
    if raum:
        params = raum.get_module_parameters()
    else:
        params = haus.get_module_parameters()

    start_time = params.get("timer", {}).get('start', None)
    end_time = params.get("timer", {}).get('end', None)
    offset = 0.0
    ttl = 3600

    if start_time and end_time:
        berlin_tz = pytz.timezone('Europe/Berlin')

        start_dt = berlin_tz.localize(datetime.strptime(start_time, '%Y-%m-%dT%H:%M'))
        end_dt = berlin_tz.localize(datetime.strptime(end_time, '%Y-%m-%dT%H:%M'))

        berlin_dt = _get_datetime_now()
        if _check_within_range(start_dt, end_dt, berlin_dt):
            ttl = (end_dt - berlin_dt).total_seconds()
            offset = params['timer']['offset']
        elif start_dt > berlin_dt:
            ttl = (start_dt-berlin_dt).total_seconds()

    if raum:
        ch.set_module_offset_raum('timer', haus.id, raum.id, offset, ttl=ttl)
    else:
        ch.set_module_offset_haus('timer', haus.id, offset, ttl=ttl)
    return offset


def get_offset_regelung(haus, module, objid):

    config = get_regelung_entity(haus=haus, objid=objid, modulename=module)
    params = config.get('module_params', dict()).get("timer", dict())

    offset = 0.0
    ttl = 600
    start_time = params.get('start_time')
    end_time = params.get('end_time')
    if start_time and end_time:
        try:
            berlin_tz = pytz.timezone('Europe/Berlin')
            start_dt = berlin_tz.localize(datetime.strptime(start_time, '%Y-%m-%dT%H:%M'))
            end_dt = berlin_tz.localize(datetime.strptime(end_time, '%Y-%m-%dT%H:%M'))
            berlin_dt = _get_datetime_now()

            if _check_within_range(start_dt, end_dt, berlin_dt):
                ttl = (end_dt - berlin_dt).total_seconds()
                offset = params.get('offset', 0.0)

            elif start_dt > berlin_dt:
                ttl = (start_dt-berlin_dt).total_seconds()

        except Exception as e:
            logging.exception("exception getting timer for regelung")

    ch.set_module_offset_regelung("timer", haus.id, module, objid, offset, ttl)

    return offset


def upcoming_event_type(typ):
    types = {
        'start': 'starting-offset',
        'end': 'ending-offset',
        'end_list': 'ending-list',
    }
    return types[typ]


def entity_params_factory(haus, entity, entity_id):

    # get model name
    if isinstance(entity, Regelung):
        entity_str = unicode(entity)
        params = get_regelung_entity(haus=haus, objid=entity_id,
                                     modulename=entity_str).get('module_params', dict())
    else:
        params = entity.get_module_parameters()
    #
    timer_params = params.get("timer", {})
    return timer_params


# unfortunatly we saved start and end parameter name in raum and regelung with different names, this function is needed
def param_names_factory(entity_str, param_name):
    entities = {
        'Raum': {'start': 'start', 'end': 'end'},
        'Haus': {'start': 'start', 'end': 'end'},
        'differenzregelung': {'start': 'start_time', 'end': 'end_time'},
        'vorlauftemperaturregelung': {'start': 'start_time', 'end': 'end_time'},
    }
    return entities[entity_str][param_name] if entity_str in entities and param_name in entities[entity_str] else None


def get_module_upcoming_events(haus, entity, entity_id, lookahead=12):
    """
    :param haus:
    :param entity: gives an entity object like (room, haus, or regelung)
    :param entity_id: gives object Id
    :param lookahead: gives a value that shows how many hours should function calculate with
    :return: returns changing offset within given lookahead and show with
                 a list of tuples ('offset', 'time', 'type') type here is offset,
    """
    # get model name
    if isinstance(entity, Regelung):
        entity_str = unicode(entity)
    else:
        entity_str = entity.__class__.__name__
    #

    timer_params = entity_params_factory(haus, entity, entity_id)
    start_time = timer_params.get(param_names_factory(entity_str, 'start'), None)
    end_time = timer_params.get(param_names_factory(entity_str, 'end'), None)
    # get 1h hour later
    time_at_nowh = _get_datetime_now(0)
    # get lookahead-value hour later
    time_at_lookahead_h = _get_datetime_now(lookahead)
    ret = [(time_at_lookahead_h, None, 'ending-list')]
    if start_time and end_time:
        berlin_tz = pytz.timezone('Europe/Berlin')
        start_dt = berlin_tz.localize(datetime.strptime(start_time, '%Y-%m-%dT%H:%M'))
        end_dt = berlin_tz.localize(datetime.strptime(end_time, '%Y-%m-%dT%H:%M'))

        # check is there any entry in look ahead duration
        if (start_dt <= time_at_lookahead_h) and (end_dt >= time_at_nowh):
            end_of_list = max(time_at_lookahead_h, end_dt)
            ret = [
                (start_dt, timer_params.get('offset', 0.0), upcoming_event_type('start')),
                (end_dt, 0, upcoming_event_type('end')),
                (end_of_list, None, upcoming_event_type('end_list')),
            ]
    ch.set_cache_upcoming_events('timer', haus.id, entity_id, entity_str, ret, ttl=lookahead * 60 * 60)
    return ret


def get_name():
    return 'Timer'


def get_global_description_link():
    desc = "Einmalige Absenkungen einfach und schnell konfigurieren."
    desc_link = "http://support.controme.com/auswahl-und-aktivierung-von-modulen/modul-zeitschalter/"
    return desc, desc_link


def get_local_settings_link(request, raum):
    offset = 0
    for params in [raum.get_module_parameters(), raum.etage.haus.get_module_parameters()]:
        if 'timer' in params:
            if 'offset' in params['timer']:
                offset = params['timer']['offset']
                if params['timer']['offset'] != 0.0 and 'end' in params['timer'] and 'start' in params['timer']:
                    start_time = params['timer']['start']
                    end_time = params['timer']['end']

                    berlin_tz = pytz.timezone('Europe/Berlin')

                    start_dt = berlin_tz.localize(datetime.strptime(start_time, '%Y-%m-%dT%H:%M'))
                    end_dt = berlin_tz.localize(datetime.strptime(end_time, '%Y-%m-%dT%H:%M'))

                    berlin_dt = _get_datetime_now()

                    if _check_within_range(start_dt, end_dt, berlin_dt):
                        return "Timer", "Timer", "%.2f" % offset, 75, "/m_raum/%s/timer/" % raum.id

    offset = 0.0
    return "Timer", "Timer", "%.2f" % offset, 75, "/m_raum/%s/timer/" % raum.id


def _set_timer(obj, offset, start, end):

    params = obj.get_module_parameters()

    if 'timer' not in params:
        params['timer'] = {'offset': 0.0}

    params['timer']['offset'] = offset
    params['timer']['start'] = start
    params['timer']['end'] = end

    obj.set_module_parameters(params)

    if type(obj) is Raum:
        ch.set_module_offset_raum('timer', obj.etage.haus.id, obj.id, 0.0, ttl=1)
    else:
        ch.set_module_offset_haus('timer', obj.id, 0.0, ttl=1)

    if type(obj) is Haus:
        for e in obj.etagen.all():
            for raum in e.raeume.all().only("modules"):
                activate(raum)

    elif type(obj) is Raum:
        activate(obj)
    return


def _remove_timer(obj, haus=None, modulename=None, modulesettings=None):
    if isinstance(obj, unicode):
        params = {"start_time": '', "end_time": '', "offset": 0.0}
        set_regelung_entity(haus, modulename, obj, params, modulesettings)
        return
    else:
        params = obj.get_module_parameters()

        params['timer']['offset'] = 0.0
        params['timer']['end'] = ''
        params['timer']['start'] = ''

        obj.set_module_parameters(params)
        return


def activate(obj):
    if 'timer' not in obj.get_modules():
        obj.set_modules(obj.get_modules() + ['timer'])


def _get_timer_settings(obj):

    deltastr = ''

    params = obj.get_module_parameters()

    if 'timer' not in params:
        params['timer'] = {'offset': 0.0}

    offset = params['timer']['offset']

    if offset != 0.0:

        start_time = params['timer']['start']
        end_time = params['timer']['end']

        berlin_tz = pytz.timezone('Europe/Berlin')

        start_dt = berlin_tz.localize(datetime.strptime(start_time, '%Y-%m-%dT%H:%M'))
        end_dt = berlin_tz.localize(datetime.strptime(end_time, '%Y-%m-%dT%H:%M'))

        completed_time, deltastr = _get_time_duration(start_dt, end_dt, offset, obj)

        if completed_time:
            offset, start_time, end_time = _get_default_values()

    else:
        offset, start_time, end_time = _get_default_values()

    return offset, start_time, end_time, deltastr


def _get_time_duration(start_time, end_time, offset, obj, haus=None):

    if type(obj) is Raum:
        link = " href='/m_raum/%s/timer/'" % obj.id
    if type(obj) is Haus:
        link = " href='/m_timer/%s/'" % obj.id
    if type(obj) is Regelung and obj.regelung == 'differenzregelung':
            link = ("href='/m_differenzregelung/%s/show/%s/timer/'" % (haus.id, obj.get_parameters()['dreg']))
    if type(obj) is Regelung and obj.regelung == 'vorlauftemperaturregelung':
        link = ("href='/m_vorlauftemperaturregelung/%s/show/%s/timer/'" % (haus.id, obj.get_parameters()['mid']))

    berlin_dt = _get_datetime_now()

    if _check_within_range(start_time, end_time, berlin_dt):

        delta = end_time - berlin_dt
        days = delta.days
        hours, remainder = divmod(delta.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)

        message = "<li><a {link}><h2>{offset} &deg;C {bis}</h2>" \
                  "<p>" \
                  "{days} {tage}, {hours} {stunden}, {minutes} {minuten}</p></a>"\
            .format(
                link=link, offset=offset, days=days, hours=hours, minutes=minutes, tage="Tage", stunden="Stunden",
                minuten="Minuten", bis="bis"
            )
        return False, message

    if _check_outside_range_passed(end_time, berlin_dt):
        return True, ''

    if _check_outside_range_not_passed(start_time, berlin_dt):

        message = "<li><a {0}><h2>{1} &deg;C </h2>" \
                   "<p>" \
                   "Startzeit noch nicht erreicht.</p></a>".format(link, offset)

        return False, message


def _check_within_range(start_time, end_time, now):
    if start_time < now < end_time:
        return True
    else:
        return False


def _check_outside_range_passed(end_time, now):
    if end_time < now:
        return True
    else:
        return False


def _check_outside_range_not_passed(start_time, now):
    if now < start_time:
        return True
    else:
        return False


def get_local_settings_page(request, raum):
    if request.method == "GET":

        if 'zsdel' in request.GET:
            _remove_timer(raum)

            # invalidate caches
            ch.set_module_offset_raum('timer', raum.etage.haus.id, raum.id, 0.0, ttl=1)
            ch.delete_cache_upcoming_events(raum.etage.haus.id, 'timer', raum.id, "Raum")
            return render_redirect(request, '/m_raum/%s/' % raum.id)

        else:
            deltastr = []

            _t, _t, _t, hausstr = _get_timer_settings(raum.etage.haus)

            offset, starttime, endtime, delta = _get_timer_settings(raum)

            if len(delta):
                delta += "<a href='/m_raum/%s/timer/?zsdel'>l&ouml;schen</a>" % raum.id
                delta += '</li>'
                deltastr.append(delta)

            if len(hausstr):
                deltastr.append(hausstr)

            return render_response(request, "m_raum_timer.html",
                                   {"raum": raum, "starttime": starttime, "endtime": endtime,
                                    'delta': deltastr, 'default': "%.2f" % offset})

    if request.method == "POST":
        start_time = str(request.POST['starttime'])
        end_time = str(request.POST['endtime'])
        offset = request.POST['offset']
        error = ''
        if start_time == '':
            error += "Bitte Startzeit ausw&auml;hlen.<br/>"
        if end_time == '':
            error += "Bitte Endzeit ausw&auml;hlen.<br/>"

        berlin_tz = pytz.timezone('Europe/Berlin')

        start_dt = datetime
        end_dt = datetime
        try:
            start_dt = berlin_tz.localize(datetime.strptime(start_time, '%Y-%m-%dT%H:%M'))
            end_dt = berlin_tz.localize(datetime.strptime(end_time, '%Y-%m-%dT%H:%M'))
        except:
           error += "falsches Datumsformat!<br/>"

        if start_dt > end_dt:
            error += u"Die gewählte Endzeit ist nach Startzeit.<br/>"
        if not len(offset):
            error += "Bitte Offset eingeben.<br/>"
        else:
            try:
                offset = float(offset)
            except:
                error += "ungültiger Offsetwert!<br/>"

        if error:
            offset, start_time, end_time, delta = _get_timer_settings(raum)

            if len(delta):
                deltastr = []
                deltastr.append(delta)
            else:
                deltastr = None

            return render_response(request, 'm_raum_timer.html',
                                   {"raum": raum, 'starttime': start_time, 'endtime': end_time,
                                    'error': error, 'default': offset, 'delta': deltastr})

        _set_timer(raum, offset, start_time, end_time)
        # invalidate cache
        ch.delete_cache_upcoming_events(raum.etage.haus.id, 'timer', raum.id, "Raum")
        return render_redirect(request, '/m_raum/%s/' % raum.id)


def get_local_settings_page_haus(request, haus):

    if request.method == "GET":
        if 'asdel' in request.GET and request.user == haus.eigentuemer:
            _remove_timer(haus)
            ch.set_module_offset_haus('timer', haus.id, 0.0, ttl=1)
            for etage in haus.etagen.all():
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    ch.delete("%s_roffsets_dict" % raum.id)

            # invalidate cache for upcoming offsets
            ch.delete_cache_upcoming_events(raum.etage.haus.id, 'timer', haus.id, "Haus")

            return render_redirect(request, '/')

        else:
            offset, starttime, endtime, delta = _get_timer_settings(haus)

            if len(delta):
                delta += "<a href='/m_timer/%s/?asdel'>l&ouml;schen</a>" % haus.id
                delta += '</li>'

            return render_response(request, "m_timer.html",
                                   {"haus": haus, "starttime": starttime, "endtime": endtime,
                                    'delta': delta, 'default': offset})

    elif request.method == "POST":
        start_time = str(request.POST['starttime'])
        end_time = str(request.POST['endtime'])
        offset = request.POST['offset']

        error = ''
        if start_time == '':
            error += "Bitte Startzeit ausw&auml;hlen.<br/>"
        if end_time == '':
            error += "Bitte Endzeit ausw&auml;hlen.<br/>"

        berlin_tz = pytz.timezone('Europe/Berlin')

        start_dt = berlin_tz.localize(datetime.strptime(start_time, '%Y-%m-%dT%H:%M'))
        end_dt = berlin_tz.localize(datetime.strptime(end_time, '%Y-%m-%dT%H:%M'))

        if start_dt > end_dt:
            error += u"Die gewählte Endzeit ist nach Startzeit.<br/>"
        if not len(offset):
            error += "Bitte Offset eingeben."
        else:
            offset = float(offset)

        if error:
            offset, start_time, end_time, delta = _get_timer_settings(haus)

            if not len(delta):
                delta = None

            offset, start_time, end_time, deltastr = _get_timer_settings(haus)
            return render_response(request, 'm_timer.html',
                                   {"haus": haus, 'starttime': start_time, 'endtime': end_time,
                                    'error': error, 'offset': offset, 'delta': delta})

        for etage in haus.etagen.all():
            for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                ch.delete("%s_roffsets_dict" % raum.id)
        _set_timer(haus, offset, start_time, end_time)
        # invalidate cache for upcoming offsets
        ch.delete_cache_upcoming_events(raum.etage.haus.id, 'timer', haus.id, "Haus")
        return render_redirect(request, '/')


def _get_datetime_now(lookahead=0):
    berlin_timezone = pytz.timezone('Europe/Berlin')

    berlin_dt = datetime.now(berlin_timezone) + timedelta(hours=lookahead)

    return berlin_dt


def _get_default_values():
    # Default Values
    offset = -1
    start_time = _get_datetime_now().strftime('%Y-%m-%dT%H:%M')
    end_time = (_get_datetime_now() + timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M')
    return offset, start_time, end_time


def get_global_settings_page_help(request, haus):
    return None


def get_local_settings_link_haus(request, haus):
    params = haus.get_module_parameters()
    a = '<span class="ui-li-aside" style="margin-top:0px;">inaktiv</span>'
    if params['timer']['offset'] > 0.0:
        a = '<span class="ui-li-aside" style="margin-top:0px;color:#cb0963">aktiv</span>'
    if params['timer']['offset'] < 0.0:
        a = '<span class="ui-li-aside" style="margin-top:0px;color:blue">aktiv</span>'
    return ["<li data-role='list-divider'>Away-Timer</li>",
            "<li style='max-height:43px'>"
            "<a style='max-height:20px' href='/m_timer/%s/'>Away-Timer %s</a></li>" % (haus.id, a)]


class OffsetTime(StaticTzInfo):
    def __init__(self, offset):
        """A dumb timezone based on offset such as +0530, -0600, etc."""
        hours = int(offset[:3])
        minutes = int(offset[0] + offset[3:])
        self._utcoffset = timedelta(hours=hours, minutes=minutes)


def get_local_settings_page_regelung(request, haus, modulename, objid, params):

    if request.method == "GET":
        if 'zsdel' in request.GET and request.user == haus.eigentuemer:
            _remove_timer(objid, haus, modulename, 'timer')

            # invalidate caches
            ch.delete("%s_offset_%s_%s_%s" % ("timer", haus.id, modulename, objid))
            ch.delete_cache_upcoming_events(haus.id, 'timer', entity_id=objid, entity_type=modulename)
            return render_redirect(request, ('/m_%s/%s/show/%s/' % (modulename, haus.id, objid)))
        else:
            context = {}
            berlin = pytz.timezone('Europe/Berlin')
            offset = None

            try:
                starttime = berlin.localize(datetime.strptime(params.get("start_time"), '%Y-%m-%dT%H:%M'))
                endtime = berlin.localize(datetime.strptime(params.get("end_time"), '%Y-%m-%dT%H:%M'))
                offset = params.get("offset", None)

                if not _check_within_range(starttime, endtime, _get_datetime_now()):
                    starttime = berlin.localize(datetime.now(berlin))
                    endtime = berlin.localize(starttime + timedelta(hours=1))
            except:
                starttime = _get_datetime_now()
                endtime = starttime + timedelta(hours=1)

            deltastr = ''
            if offset != None:
                deltastr = ''
                regs = Regelung.objects.filter_for_user(request.user).filter(Q(regelung='differenzregelung') | Q(regelung='vorlauftemperaturregelung')).all()
                for reg in regs:
                    if reg.regelung == 'differenzregelung':
                        if reg.get_parameters()['dreg'] == objid:
                            completed_time, deltastr = _get_time_duration(starttime, endtime, offset, reg, haus)
                            if len(deltastr):
                                deltastr += ("<a href='/m_%s/%s/show/%s/timer/?zsdel'>l&ouml;schen</a>" % (
                                reg.regelung, haus.id, objid))
                                deltastr += '</li>'
                                context['delta'] = deltastr

                    if reg.regelung == 'vorlauftemperaturregelung':
                        if reg.get_parameters()['mid'] == objid:
                            completed_time, deltastr = _get_time_duration(starttime, endtime, offset, reg, haus)
                            if len(deltastr):
                                deltastr += ("<a href='/m_%s/%s/show/%s/timer/?zsdel'>l&ouml;schen</a>" % (
                                    reg.regelung, haus.id, objid))
                                deltastr += '</li>'
                                context['delta'] = deltastr

            context["starttime"] = starttime.strftime("%Y-%m-%dT%H:%M")
            context["endtime"] = endtime.strftime("%Y-%m-%dT%H:%M")
            context['modulename'] = modulename
            context['haus'] = haus
            context['objid'] = objid
            return render_response(request, 'm_regelung_timer.html', context)

    elif request.method == "POST":
        error = ''
        try:
            start_time = str(request.POST['starttime'])
            end_time = str(request.POST['endtime'])
        except ValueError as e:
            error = e
            context = {"starttime": '', "endtime": '', "modulename": modulename, "haus": haus,
                       "objid": objid, "error": error}
            return render_response(request, "m_regelung_timer.html", context)

        offset = request.POST['offset']

        berlin_tz = pytz.timezone('Europe/Berlin')
        start_dt = berlin_tz.localize(datetime.strptime(start_time, '%Y-%m-%dT%H:%M'))
        end_dt = berlin_tz.localize(datetime.strptime(end_time, '%Y-%m-%dT%H:%M'))

        if start_dt > end_dt:
            error += u"Die gewählte Endzeit ist nach Startzeit.<br/>"
        if not len(offset):
            error += "Bitte Offset eingeben.<br/>"
        else:
            offset = float(offset)

        if error != '':
            context = {"starttime": '', "endtime": '', "modulename": modulename, "haus": haus,
                       "objid": objid, "error": error}
            return render_response(request, "m_regelung_timer.html", context)

        params = {"start_time": start_dt.strftime('%Y-%m-%dT%H:%M'), "end_time": end_dt.strftime('%Y-%m-%dT%H:%M'), "offset": offset}
        set_regelung_entity(haus, modulename, objid, params, 'timer')

        # invalidate cache
        ch.delete_cache_upcoming_events(haus.id, 'timer', entity_id=objid, entity_type=modulename)

        ch.delete("%s_offset_%s_%s_%s" % ("timer", haus.id, modulename, objid))

        return render_redirect(request, '/m_%s/%s/show/%s/' % (modulename, haus.id, objid))


def get_local_settings_link_regelung(haus, modulename, objid):
    offset = get_offset_regelung(haus, modulename, objid)
    return get_name(), 'timer', offset, 75, '/m_%s/%s/show/%s/timer/' % (modulename, haus.id, objid)


def time_validate(date_text):
    try:
        strptime = datetime.strptime(date_text, '%Y-%m-%dT%H:%M')
        strptime.strftime('%Y-%m-%dT%H:%M')
        return strptime
    except ValueError:
        raise ValueError("Incorrect data format")


def get_jsonapi(haus, usr, entityid=None):
    berlin_tz = pytz.timezone('Europe/Berlin')
    berlin_dt = datetime.now(berlin_tz)
    all_room_names = []
    ret = []
    # rooms
    etagen = Etage.objects.filter(haus=haus)
    for etage in etagen:
        for raum in Raum.objects.filter_for_user(usr, etage_id=etage.id).order_by("position"):
            all_room_names.append(raum.name)
            r_params = raum.get_module_parameters()
            timer_params = r_params.get('timer', {})
            start_time = timer_params.get('start', None)
            end_time = timer_params.get('end', None)
            if start_time and end_time:
                start_dt = berlin_tz.localize(datetime.strptime(start_time, '%Y-%m-%dT%H:%M'))
                end_dt = berlin_tz.localize(datetime.strptime(end_time, '%Y-%m-%dT%H:%M'))
                if start_dt >= berlin_dt or (start_dt < berlin_dt < end_dt):
                    ret.append({'source': 'Timer',
                                'next_due_time': datetime.strftime(start_dt, '%Y-%m-%d %H:%M %p'),
                                'next_end_time': datetime.strftime(end_dt, '%Y-%m-%d %H:%M %p'),
                                'offset': timer_params.get('offset', 0.0),
                                'raeume': raum.name
                                })

    params = haus.get_module_parameters()
    start_time = params.get('timer', {}).get('start', None)
    end_time = params.get('timer', {}).get('end', None)

    if start_time and end_time:
        start_dt = berlin_tz.localize(datetime.strptime(start_time, '%Y-%m-%dT%H:%M'))
        end_dt = berlin_tz.localize(datetime.strptime(end_time, '%Y-%m-%dT%H:%M'))
        if start_dt >= berlin_dt or (start_dt < berlin_dt < end_dt):
            ret.append({'source': 'Timer',
                        'next_due_time': datetime.strftime(start_dt, '%Y-%m-%d %H:%M %p'),
                        'next_end_time': datetime.strftime(end_dt, '%Y-%m-%d %H:%M %p'),
                        'offset': params.get('timer', {}).get('offset', 0.0),
                        'raeume': all_room_names
                        })

    return ret
