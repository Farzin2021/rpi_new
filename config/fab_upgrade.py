from fabric.operations import local as lrun
from fabric.api import lcd, settings, sudo, hide
import requests
from fabric.state import env


env.hosts = ['localhost']
path = "/home/pi/rpi"
REMOTE_HOST = "http://controme-main.appspot.com"


def setup_virtualenv():
    pass


def install_packages():
    # cuisine.package_ensure_apt("nginx")  # 1.2.1
    # cuisine.package_ensure_apt("postgresql-9.1")
    # cuisine.package_ensure_apt("supervisor")  # 3.0
    # cuisine.package_ensure_apt("rabbitmq-server")  # 2.8.4 ... lokal 3.3.0
    # cuisine.package_ensure_apt("python-pip")
    # cuisine.package_ensure_apt("memcached")  # 1.4.13
    # cuisine.python_package_ensure_pip(r="%s/config/requirements.txt")
    # ufw?
    # python-openzwave
    pass


def install_software():
    lrun("cd %s && git clone https://bitbucket.org/hcerny/cms_upgrade.git ." % path)


def configure_uwsgi():
    # cuisine.sudo("cp %s/config/uwsgi-rpi.ini /etc/uwsgi/apps-available/rpi.ini")
    # cuisine.sudo("ln -s /etc/uwsgi/apps-available/rpi.ini /etc/uwsgi/apps-enabled/rpi.ini")
    pass


def configure_nginx():
    # cuisine.sudo("cp %s/config/nginx.conf /etc/nginx/")
    # cuisine.sudo("service nginx reload")
    pass


def configure_supervisor():
    # cuisine.sudo("cp %s/config/supervisor-messaging.conf /etc/supervisor/conf.d/messaging.conf")
    # cuisine.sudo("cp %s/config/supervisor-uwsgi.conf /etc/supervisor/conf.d/uwsgi.conf")
    # cuisine.sudo("cp %s/config/supervisor-celery.conf /etc/supervisor/conf.d/celery.conf")
    # cuisine.sudo("supervisorctl start zwavehandler messageserver uwsgi celery celerybeat")
    pass


def get_gw_cryptkeys():
    # dafuer fehlt die server schnittstelle noch
    pass


def upgrade(old_head):

    with lcd(path) and settings(warn_only=True):
        commitlist = lrun("git log %s..HEAD | grep ^commit" % old_head, capture=True)

    commits = []
    for commit in commitlist.split('\n'):
        try:
            commits.append(commit.split(' ')[1])
        except IndexError:
            continue

    for commit in commits:
        try:
            globals()["upgrade%s" % commit[:8]]()
        except Exception:
            pass

    last = commits[0] if len(commits) else None
    if last:
        with open('/sys/class/net/eth0/address') as f:
            macaddr = f.read().strip().replace(':', '-')
        requests.get("%s/get/update/%s/?ver=%s" % (REMOTE_HOST, macaddr, last))

    err = []
    try:
        lrun("sudo chown pi /var/log/uwsgi")
        lrun("sudo pip install django-cache-machine==0.9.1")
    except:
        err.append("Fehler in 1")

    try:
        result = lrun("pip freeze | grep Django", capture=True)
        if "1.11.26" not in result:
            branch = lrun("git branch | grep '^*'", capture=True)
            with open('/sys/class/net/eth0/address') as f:
                macaddr = f.read().strip().replace(':', '-')
            r = requests.get("%s/get/update/%s/?mu" % (REMOTE_HOST, macaddr), timeout=20)
            result = lrun("git fetch origin %s" % ("master:master" if "beta" in branch else "beta:beta"), capture=True)  # pull again, to make sure that both branches are up-to-date
            lrun("sudo pip install -U Django==1.11.26")
    except:
        err.append("wrong django version")

    try:
        result = lrun("cat /etc/crontab | grep svhc", capture=True)
    except:
        try:
            lrun("""echo '*/15 * * * * pi /usr/bin/python /home/pi/rpi/config/svhc.py' | sudo tee -a /etc/crontab""")
        except:
            err.append("Fehler: svhc konnte nicht gesetzt werden")

    try:
        result = lrun("cat /etc/nginx/nginx.conf | grep 'favicon.ico'", capture=True)
    except:
        try:
            lrun("sudo cp /home/pi/rpi/config/nginx.conf /etc/nginx/")
            lrun("sudo service nginx restart")
        except:
            err.append("nginx konnte nicht neu gestartet werden")

    try:
        result = lrun("cat /etc/nginx/nginx.conf | grep 'uwsgi_read_timeout'", capture=True)
    except:
        try:
            lrun("sudo cp /home/pi/rpi/config/nginx.conf /etc/nginx/")
            lrun("sudo service nginx restart")
        except:
            err.append("nginx konnte nicht neu gestartet werden")

    try:
        lrun("grep 'named pipe' /etc/rsyslog.conf")
    except:
        pass
    else:
        try:
            lrun("sudo sed -i '/# The named pipe \/dev\/xconsole/,$d' /etc/rsyslog.conf")
            lrun("sudo service rsyslog restart")
        except:
            err.append("sed/restart error")

    u = lrun("ls /etc/logrotate.d/", capture=True)
    if 'uwsgi' not in u:
        try:
            lrun("sudo cp /home/pi/rpi/config/logrotate-uwsgi /etc/logrotate.d/uwsgi")
        except:
            err.append("couldnt copy logrotate uwsgi")
    if 'logrotate-service-monitor' not in u:
        try:
            lrun("sudo cp /home/pi/rpi/config/logrotate-service-monitor /etc/logrotate.d/logrotate-service-monitor")
        except:
            err.append("couldnt copy logrotate service monitor")
    try:
        lrun("rm /home/pi/rpi/heizmanager/migrations/*")
    except:
        try:
            lrun("mkdir /home/pi/rpi/heizmanager/migrations/")
        except:
            pass

    try:
        lrun("cp /home/pi/rpi/heizmanager/_migrations/* /home/pi/rpi/heizmanager/migrations/")
    except:
        pass

    try:
        result = lrun("sqlite3 /home/pi/rpi/db.sqlite3 '.schema django_content_type'", capture=True)
        if 'name' not in result:
            lrun("""sqlite3 /home/pi/rpi/db.sqlite3 "alter table django_content_type add column 'name' varchar(100)" """)
    except:
        err.append("couldnt add column django_content_type.name")

    try:
        result = lrun("ls /home/pi/rpi/db.sqlite3")
    except:
        try:
            lrun("python /home/pi/rpi/manage.py migrate")
        except:
            err.append("db konnte nicht angelegt werden")
    else:
        try:
            migrations = lrun("sqlite3 db.sqlite3 'select name from django_migrations'", capture=True)
            if "0002_gwparams" not in migrations or "0003_rfprotocol" not in migrations or "0004_luftfeuchtigkeit" not in migrations or "0005_gwparams" not in migrations:
                try:
                    result = lrun("sqlite3 /home/pi/rpi/db.sqlite3 '.schema heizmanager_gateway'", capture=True)
                    if 'parameters' not in result:
                        lrun("python /home/pi/rpi/manage.py migrate heizmanager")
                except:
                    err.append("couldnt migrate db for gw params")

                try:
                    result = lrun("sqlite3 /home/pi/rpi/db.sqlite3 'select * from heizmanager_luftfeuchtigkeitssensor'")
                    lrun("python /home/pi/rpi/manage.py migrate --fake heizmanager 0004")
                except:
                    try:
                        lrun("python /home/pi/rpi/manage.py migrate heizmanager")
                    except:
                        err.append("db konnte nicht angelegt werden")

                try:
                    result = lrun("sqlite3 /home/pi/rpi/db.sqlite3 'select * from benutzerverwaltung_objectpermission'")
                    lrun("python /home/pi/rpi/manage.py migrate --fake benutzerverwaltung")
                except:
                    try:
                        lrun("python /home/pi/rpi/manage.py makemigrations benutzerverwaltung")
                        lrun("python /home/pi/rpi/manage.py migrate benutzerverwaltung")
                        lrun("""sqlite3 /home/pi/rpi/db.sqlite3 "update users_userprofile set role = 'A'" """)
                    except:
                        err.append("benutzerverwaltung konnte nicht migriert werden")

                try:
                    result = lrun("sqlite3 /home/pi/rpi/db.sqlite3 '.schema benutzerverwaltung_objectpermission'", capture=True)
                    if '"object_id", "content_type_id"' not in result:
                        lrun("python /home/pi/rpi/manage.py makemigrations benutzerverwaltung")
                        lrun("python /home/pi/rpi/manage.py migrate benutzerverwaltung")
                except:
                    pass

                try:
                    result = lrun("sqlite3 /home/pi/rpi/db.sqlite3 'select * from geolocation_geodevice'")
                    lrun("python /home/pi/rpi/manage.py migrate --fake geolocation")
                except:
                    try:
                        lrun("python /home/pi/rpi/manage.py makemigrations geolocation")
                        lrun("python /home/pi/rpi/manage.py migrate geolocation")
                    except:
                        err.append("geolocation konnte nicht migriert werden")

                try:
                    result = lrun("sqlite3 /home/pi/rpi/db.sqlite3 '.schema users_userprofile'", capture=True)
                    if 'parameters' not in result:
                        lrun("python /home/pi/rpi/manage.py makemigrations users")
                        try:
                            lrun("ls /home/pi/rpi/users/migrations/0002*py")
                            lrun("""sqlite3 /home/pi/rpi/db.sqlite3 'delete from django_migrations where app="users"' """)
                            lrun("python /home/pi/rpi/manage.py migrate --fake users 0001")
                            lrun("python /home/pi/rpi/manage.py migrate users")
                        except:
                            # todo jetzt haben wir ein problem. eigentlich muesste man jetzt zu master wechseln.
                            lrun("rm -rf /home/pi/rpi/users/migrations/")
                            lrun("python /home/pi/rpi/manage.py makemigrations users")
                            lrun("python /home/pi/rpi/manage.py migrate users --fake-initial")
                            lrun("""sqlite3 /home/pi/rpi/db.sqlite3 "alter table users_userprofile add column 'parameters' text NULL" """)
                except:
                    err.append("problem with migrating users_userprofile")

                try:  # alle anderen sind spezifisch, aber wenn die hier schiefgehen, funktioniert auth_user nicht
                    lrun("python /home/pi/rpi/manage.py migrate --fake raumgruppen")
                    lrun("python /home/pi/rpi/manage.py migrate --fake users")
                    lrun("python /home/pi/rpi/manage.py migrate --fake vsensoren")
                except Exception as e:
                    err.append("error faking: %s" % str(e))

                try:
                    result = lrun("sqlite3 /home/pi/rpi/db.sqlite3 '.schema auth_user'", capture=True)
                    if '"last_login" datetime NULL' not in result:
                        lrun("python /home/pi/rpi/manage.py migrate")
                except:
                    err.append("problem with migrating auth_user")
        except:
            err.append("error with migrations")
    restartsupervisor = False

    pv = lrun("python3 -V", capture=True)
    try:
        l = lrun("sqlite3 db.sqlite3 '.schema knx_knxgateway'", capture=True)
        if not len(l):
            try:
                lrun("rm /home/pi/rpi/knx/migrations/*")
            except:
                try:
                    lrun("mkdir /home/pi/rpi/knx/migrations/")
                except:
                    pass
            lrun("cp /home/pi/rpi/knx/_migrations/* /home/pi/rpi/knx/migrations/")
            lrun("python /home/pi/rpi/manage.py migrate knx")
        if pv >= "Python 3.7":
            try:
                lrun("pip3 show xknx", capture=True)
            except:
                lrun("sudo apt-get update")
                lrun("sudo apt-get install -y python3-pip")
                lrun("pip3 install pyzmq==19.0.2")
                lrun("pip3 install xknx==0.14.4")
                lrun("sudo cp /home/pi/rpi/config/supervisor-messaging.conf /etc/supervisor/conf.d/messaging.conf")
                restartsupervisor = True
    except:
        err.append("exc installing knx")

    try:
        res = lrun("ls -al /etc/ntp.conf", capture=True)
        if 'root root' in res:
            lrun("sudo chown ntp:ntp /etc/ntp.conf")
        lrun("sudo service ntp restart")
    except Exception as e:
        err.append("%s bei ntp" % str(e))

    try:
        with hide('output', 'running', 'warnings'):
            res = lrun("cat /home/pi/rpi/rpi/aeskey.py", capture=True)
            key = res[7:-1]
            res = lrun("""sqlite3 /home/pi/rpi/db.sqlite3 "select aeskey from heizmanager_cryptkeys where id=1 and aeskey != '%s'" """ % key, capture=True)
            if len(res):
                lrun("""sqlite3 /home/pi/rpi/db.sqlite3 "update heizmanager_cryptkeys set aeskey = '%s' where id = 1" """ % key)
    except Exception as e:
        err.append("%s bei ck" % str(e))

    try:
        lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi btlehandler enoceanhandler")
    except Exception as e:
        err.append("error restarting uwsgi/btle: %s" % str(e))
    try:
        lrun("pkill celery")
    except:
        err.append("error killall celery")
    try:
        lrun("rm /home/pi/rpi/celerybeat.pid")
    except:
        pass
    try:
        uname = lrun("uname -a", capture=True)
        if '4.14.79' in uname:
            lrun("sudo killall celery")
        lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart celery celerybeat")
    except Exception as e:
        err.append("error restarting celery/celerybeat: %s" % str(e))

    s = lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf status", capture=True)
    if 'usbdev' not in s:
        try:
            lrun("sudo cp /home/pi/rpi/config/supervisor-usbdev.conf /etc/supervisor/conf.d/usbdev.conf")
            restartsupervisor = True
        except:
            err.append("usbdev konnte nicht kopiert werden")
    if 'btlehandler' not in s or 'btlehandler                      FATAL' in s:
        try:
            lrun("sudo cp /home/pi/rpi/config/supervisor-messaging.conf /etc/supervisor/conf.d/messaging.conf")
            lrun("sudo apt-get install -y libglib2.0-dev")
            lrun("sudo pip install bluepy==1.3.0")
            restartsupervisor = True
        except:
            err.append("messaging (fuer btle) konnte nicht kopiert werden")
    try:
        lrun("ls /home/pi/rpi/heizmanager/static/testlaeufe/")
    except:
        lrun("mkdir /home/pi/rpi/heizmanager/static/testlaeufe/")
    try:
        lrun("sudo pip install -U bluepy==1.3.0")
    except:
        pass
    try:
        lrun("sudo pip install xxtea==1.3.0")
    except:
        pass
    if 'enoceanhandler' not in s:
        try:
            lrun("sudo cp /home/pi/rpi/config/supervisor-messaging.conf /etc/supervisor/conf.d/messaging.conf")
            lrun("sudo pip install enocean==0.41")
            restartsupervisor = True
        except:
            err.append("messaging (enocean) konnte nicht kopiert werden")
    else:
        try:
            res = lrun("pip show enocean", capture=True)
            if not len(res):
                lrun("sudo pip install enocean==0.41")
                lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart enoceanhandler")
        except:
            lrun("sudo pip install enocean==0.41")
            lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart enoceanhandler")
    if 'telegrambot' not in s:
        try:
            lrun("sudo pip install telepot==12.1")
            lrun("sudo pip install apscheduler==3.3.1")
            lrun("sudo cp /home/pi/rpi/config/supervisor-messaging.conf /etc/supervisor/conf.d/messaging.conf")
            restartsupervisor = True
        except:
            err.append("telegram konnte nicht kopiert werden")
    if restartsupervisor:
        lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf reload")

    try:
        res = lrun("pip show matplotlib", capture=True)
        if not len(res):
            result = lrun("sudo pip install matplotlib==2.0.2")
    except:
        try:
            result = lrun("sudo pip install matplotlib==2.0.2")
        except:
            err.append("error installing matplotlib")

    try:
        r = lrun("dpkg -s openvpn", capture=True)
    except:
        lrun("sudo apt-get update")
        lrun("sudo apt-get install -y openvpn")
    try:
        lrun("ls /etc/openvpn/down.sh")
    except:
        lrun("sudo cp /home/pi/rpi/config/openvpn_down.sh /etc/openvpn/down.sh")
        lrun("sudo chmod +x /etc/openvpn/down.sh")
        lrun("sudo cp /home/pi/rpi/config/openvpn_up.sh /etc/openvpn/up.sh")
        lrun("sudo chmod +x /etc/openvpn/up.sh")
        lrun("sudo cp /home/pi/rpi/config/openvpn.conf /etc/openvpn/openvpn.conf")

    try:
        lrun("sudo grep 'mssfix' /etc/openvpn/openvpn.conf")
    except:
        lrun("sudo cp /home/pi/rpi/config/openvpn.conf /etc/openvpn/openvpn.conf")
        try:
            lrun("sudo systemctl restart openvpn@openvpn.service")
        except:
            pass
    try:
        lrun("sudo grep 'mtu-disc' /etc/openvpn/openvpn.conf")
        lrun("sudo cp /home/pi/rpi/config/openvpn.conf /etc/openvpn/openvpn.conf")
        lrun("sudo systemctl restart openvpn@openvpn.service")
    except:
        pass

    try:
        with settings(warn_only=True):
            a = lrun("sudo systemctl status alexa_interface.service", capture=True)
        if a.succeeded:
            pass
        elif "run.pid was not met" in a:
            pass
        else:
            raise Exception("not installed or not working")
    except:
        try:
            lrun("sudo cp /home/pi/rpi/alexa_interface/alexa_interface.service /lib/systemd/system/")
            lrun("sudo chmod +x /lib/systemd/system/alexa_interface.service")
            lrun("sudo systemctl daemon-reload")
            lrun("sudo systemctl enable alexa_interface.service")
            lrun("sudo systemctl start alexa_interface")
        except:
            pass

    try:
        lrun("sudo systemctl restart alexa_interface.service")
    except:
        pass

    try:
        with settings(warn_only=True):
            a = lrun("sudo systemctl status homebridge_configbuilder", capture=True)
            if a.succeeded:
                b = lrun("npm list -g | grep homebridge-controme", capture=True)
                if "2.1.1" not in b:
                    lrun("sudo npm install -g homebridge-controme")
                try:
                    a = lrun("ls /home/pi/rpi/homebridge/run.pid", capture=True)
                    if not len(a):
                        raise Exception("fabric version inconsistencies?")
                    lrun("sudo systemctl restart homebridge")
                    try:
                        # legacy: wenn homebridge laeuft, nicht abschiessen
                        mods = lrun("sqlite3 /home/pi/rpi/db.sqlite3 'select modules from heizmanager_hausprofil'", capture=True)
                        if 'homebridge' not in mods:
                            mods += ',homebridge'
                            lrun("""sqlite3 /home/pi/rpi/db.sqlite3 'update heizmanager_hausprofil set modules="%s"' """ % mods)
                            lrun("sudo cp /home/pi/rpi/homebridge/homebridge_configbuilder.service /lib/systemd/system/")  # run.pid fuer configbuilder entfernt
                            lrun("sudo systemctl daemon-reload")
                            lrun("sudo systemctl reload homebridge_configbuilder")
                    except:
                        pass
                except:
                    # keine run.pid
                    pass
                lrun("sudo systemctl restart homebridge_configbuilder")
            elif a.return_code > 2:
                lrun("sudo cp /home/pi/rpi/homebridge/homebridge_configbuilder.service /lib/systemd/system/")
                lrun("sudo chmod +x /lib/systemd/system/homebridge_configbuilder.service")
                lrun("sudo cp /home/pi/rpi/homebridge/homebridge.service /lib/systemd/system/")
                lrun("sudo chmod +x /lib/systemd/system/homebridge.service")
                raise Exception("not installed")
            else:
                b = lrun("npm list -g | grep homebridge-controme", capture=True)
                if "2.1.1" not in b:
                    lrun("sudo npm install -g homebridge-controme")
                lrun("sudo systemctl restart homebridge_configbuilder")  # falls sich das skript geaendert hat
    except:
        try:
            lrun("sudo apt-get update")
            lrun("curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -")
            lrun("sudo apt-get install -y nodejs")
            lrun("sudo apt-get install -y libavahi-compat-libdnssd-dev")
            lrun("sudo npm install -g --unsafe-perm homebridge")
            lrun("sudo npm install -g --unsafe-perm homebridge-controme")

            lrun("sudo systemctl daemon-reload")
            lrun("sudo systemctl enable homebridge.service")
            lrun("sudo systemctl start homebridge")
            lrun("sudo systemctl enable homebridge_configbuilder.service")
            lrun("sudo systemctl start homebridge_configbuilder")
            lrun("sudo systemctl restart homebridge")
        except:
            err.append("error installing nodejs")

    try:
        v = lrun("npm list -g | grep homebridge@", capture=True)
        v = v.split('@')[1]
        if v < "1.3.3":
            lrun("sudo npm update -g homebridge")
            lrun("sudo systemctl restart homebridge")
    except:
        pass

    dv = lrun("cat /etc/debian_version", capture=True)
    try:
        p = lrun("pip show pandas", capture=True)
        if "Version: 0.20.3" in p and dv.startswith("10."):
            try:
                lrun("sudo pip uninstall -y numpy pandas scikit-learn")
                lrun("sudo apt-get update")
                lrun("sudo apt-get install -y python-pandas python-numpy python-sklearn")
                lrun("sudo supervisorctl restart uwsgi celery")
            except:
                pass
        elif not len(p):
            raise Exception("")
    except:
        try:
            if dv.startswith("10."):
                lrun("sudo pip uninstall -y numpy pandas scikit-learn")
                lrun("sudo apt-get update")
                lrun("sudo apt-get install -y python-pandas python-numpy python-sklearn")
                lrun("sudo supervisorctl restart uwsgi celery")
            else:
                lrun("""sudo su -c 'echo "CONF_SWAPSIZE=1024" > /etc/dphys-swapfile'""")
                try:
                    lrun("sudo dphys-swapfile setup")
                except:
                    lrun("sudo pip install cython==0.29.15 pandas==0.20.3")  # proserver
                else:
                    lrun("sudo dphys-swapfile swapon")
                    lrun("sudo pip install cython==0.29.15")
                    lrun("nohup sh -c 'sudo pip install pandas==0.20.3 && sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi celery celerybeat' &")
        except:
            print "exc running pandas install"

    try:
        res = lrun("pip show scikit-learn", capture=True)
        if not len(res):
            lrun("sudo apt-get update")
            if dv.startswith("10."):
                lrun("sudo pip uninstall -y numpy pandas scikit-learn")
                lrun("sudo apt-get install -y python-pandas python-numpy python-sklearn")
                lrun("sudo supervisorctl restart uwsgi celery")
            else:
                lrun("sudo apt-get install -y gfortran libopenblas-dev liblapack-dev")
                lrun("nohup sh -c 'sudo pip install -U numpy==1.16.6 && sudo apt-get install -y python-scipy && sudo pip install scikit-learn==0.20.4 && sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi celery celerybeat' &")
    except:
        try:
            lrun("sudo dpkg --configure -a")
            lrun("sudo apt-get update")
            if dv.startswith("10."):
                lrun("sudo pip uninstall -y numpy pandas scikit-learn")
                lrun("sudo apt-get install -y python-pandas python-numpy python-sklearn")
                lrun("sudo supervisorctl restart uwsgi celery")
            else:
                lrun("sudo apt-get install -y gfortran libopenblas-dev liblapack-dev")
                lrun("nohup sh -c 'sudo pip install -U numpy==1.16.6 && sudo apt-get install -y python-scipy && sudo pip install scikit-learn==0.20.4 && sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi celery celerybeat' &")
        except:
            pass

    try:
        lrun("sudo apt-get clean")
        lrun("sudo rm /supervi*")
    except:
        pass

    if len(err):
        try:
            import json
            err = err.translate(None, "'").translate(None, '"')
            enc = lrun("""python -c "from rpi.aeshelper import aes_encrypt; import json; print aes_encrypt(json.dumps(%s))" """ % {"error": err}, capture=True)
            err = enc
            with open('/sys/class/net/eth0/address') as f:
                mac = f.read()
                macaddr = mac.strip().replace(':', '-').lower()
            requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % err)
        except:
            pass


def restart_services():
    #lrun("sudo service nginx reload")
    lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart messageserver zwavehandler")


def stop():
    # cuisine.sudo("service nginx stop")
    # cuisine.sudo("supervisorctl -c /etc/supervisor/supervisord.conf stop zwavehandler messageserver uwsgi celery celerybeat")
    pass
