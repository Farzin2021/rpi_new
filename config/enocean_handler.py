from enocean.consolelogger import init_logging
from enocean.communicators.serialcommunicator import SerialCommunicator
from enocean.protocol.constants import PACKET, RORG
from enocean.protocol.packet import Packet, ResponsePacket, RadioPacket, UTETeachIn
from enocean import utils
import sys
import serial
import logging
import zmq
import json
import requests
from fabric.api import local
import time
from datetime import datetime, timedelta


try:
    from zmq.core.error import ZMQError
except ImportError:
    from zmq import ZMQError
try:
    import queue
except ImportError:
    import Queue as queue

logging.basicConfig(level=logging.INFO, format='[%(asctime)s] - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger('core')


class EnoceanHandler():

    def __init__(self):
        self.com = None
        self.base_id = None
        ports = ['/dev/ttyUSB0', '/dev/ttyUSB1', '/dev/ttyACM0', '/dev/ttyACM1']
        while self.com is None:
            for port in ports:
                try:
                    self.com = SerialCommunicator(port=port)
                except serial.serialutil.SerialException:
                    continue
                self.com.start()
                logger.info("base_id is %s" % str(self.com.base_id))
                c = Packet(PACKET.COMMON_COMMAND, [0x03])
                self.com.send(c)
                try:
                    p = self.com.receive.get(block=True, timeout=1)
                except (queue.Empty, Exception):
                    logger.error("nothing/error on port %s" % port)
                else:
                    if self.com.base_id is not None:
                        logger.info("port %s it is" % port)
                        self.base_id = utils.to_hex_string(self.com.base_id)
                        break
            else:
                time.sleep(60)

        self.zmqcontext = zmq.Context()
        self.outsocket = self.zmqcontext.socket(zmq.DEALER)
        self.outsocket.connect("tcp://127.0.0.1:5557")
        self.insocket = self.zmqcontext.socket(zmq.SUB)
        self.insocket.connect("tcp://127.0.0.1:%s" % 5558)
        self.insocket.setsockopt(zmq.SUBSCRIBE, 'enocean')
        self.poller = zmq.Poller()
        self.poller.register(self.insocket, zmq.POLLIN)

        self.network_data_cnt = dict()
        self.last_ts = None

    def run(self):
        if self.com is None or self.base_id is None:
            logger.error("no communicator instantiated")
            return

        states = {}
        self.outsocket.send_multipart(['enocean', '', json.dumps({self.base_id: {}})])
        self.last_ts = datetime.utcnow()
        logger.info("starting loop...")
        while self.com.is_alive():
            
            try:
                p = self.com.receive.get(block=True, timeout=0.1)
            except queue.Empty:
                p = None
            except Exception as e:
                p = None
                logger.error("exception %s, stopping" % str(e))

            if p:
                try:
                    if p.sender_hex.lower() == self.base_id.lower():
                        raise Exception("discarding repeated packet")
                except Exception as e:
                    logger.exception("exception checking for sender")
                else:
                    if isinstance(p, UTETeachIn):
                        logger.info("new device taught in! the id is %s." % (p.sender_hex))
                        # antwort wird automatisch geschickt via serialcommunicator.run() -> packet.parse_msg()
                        self.outsocket.send_multipart(['enocean', '', json.dumps({self.base_id: {p.sender_hex: {'teach_in': True}}})])
                        self.last_ts = datetime.utcnow()
                        # kann natuerlich auch sein, dass dieses paket verloren geht. was machen wir dann in der oberflaeche?
                        # vielleicht gegenchecken? eine erfolgsmeldung zurueckschicken, wenn das schalten funktioniert hat? wir bekommen ja eine bestaetigung... geht aber natuerlich nur, wenn diese nicht kommt, wenn das geraet nicht eingelernt ist
                    elif isinstance(p, RadioPacket) and hasattr(p, 'sender'):
                        if p.rorg == 0xd5:
                            for k in p.parse_eep(0x00, 0x01):
                                logger.info("%s: %s" % (k, p.parsed[k]))
                                if p.parsed[k].get('description') == 'Contact':
                                    self.outsocket.send_multipart(['enocean', '', json.dumps({self.base_id: {p.sender_hex: p.parsed[k]}})])
                                    self.last_ts = datetime.utcnow()
                        elif p.rorg == 0xd2:
                            for k in p.parse_eep(0x01, 0x01):
                                logger.info("%s: %s" % (k, p.parsed[k]))
                                if k == 'OV':
                                    if states.get(p.sender_hex.lower()) != p.parsed[k]['raw_value']:
                                        self.outsocket.send_multipart(['enocean', '', json.dumps({self.base_id: {p.sender_hex: p.parsed[k]}})])
                                    else:
                                        ret = {p.sender_hex: p.parsed[k]}
                                        ret[p.sender_hex]['ack'] = True
                                        self.outsocket.send_multipart(['enocean', '', json.dumps({self.base_id: ret})])
                                    self.last_ts = datetime.utcnow()
                                    states[p.sender_hex.lower()] = p.parsed[k]['raw_value']
                        elif p.rorg == 0xa5:
                            if p.sender_hex.lower() in self.network_data_cnt and not p.learn:
                                if self.network_data_cnt[p.sender_hex.lower()]['rorg_func'] == 0x20:
                                    ret = {'description': 'HKT'}
                                    for k in p.parse_eep(self.network_data_cnt[p.sender_hex.lower()]['rorg_func'], self.network_data_cnt[p.sender_hex.lower()]['rorg_type']):
                                        if k == 'CV':
                                            ret['CV'] = p.parsed[k]['value']
                                        if k == 'TMP':
                                            ret['Temperature'] = p.parsed[k]['value']
                                        if k == 'SVC':
                                            ret['Battery'] = p.parsed[k]['raw_value'] / 2.5
                                        if k == 'ES':
                                            ret['sufficiently_charged'] = p.parsed[k]['value']
                                        if k == 'ACO':
                                            ret['actuator_obstructed'] = p.parsed[k]['value']
                                        if k == 'ENIE':
                                            ret['currently_charging'] = p.parsed[k]['value']    
                                    if 'SP' in self.network_data_cnt[p.sender_hex.lower()]:
                                        tmp = self.network_data_cnt[p.sender_hex.lower()].get('TMP')
                                        # tmp = 0x0 if not tmp else (255 - int(tmp/40.0*255))  # das ist falsch, da gehen die stellantriebe sofort zu
                                        # tmp = 0x0 if not tmp else (255-int(tmp))
                                        tmp = 0x0 if not tmp else 255-int((float(tmp)*255)/40)
                                        if int(self.network_data_cnt[p.sender_hex.lower()]['SP']) == -99:
                                            DB_3 = 0x0
                                            DB_2 = tmp
                                            DB_1 = 0b00000000  # DB1.2 ist SPS, DB1.3 ist SB
                                            DB_0 = 0b00001000  # DB0.3 ist LRN, ist bei Datentelegrammen gesetzt
                                        else:
                                            DB_3 = int((255*float(self.network_data_cnt[p.sender_hex.lower()]['SP']))/40)  # int(self.network_data_cnt[p.sender_hex.lower()]['SP'])
                                            DB_2 = tmp
                                            DB_1 = 0b00000100  # DB1.2 ist SPS, DB1.3 ist SB
                                            DB_0 = 0b00001000  # DB0.3 ist LRN, ist bei Datentelegrammen gesetzt
                                        data = [p.rorg] + [DB_3, DB_2, DB_1, DB_0] + self.com.base_id + [0x0]
                                        optional = [0x03] + p.sender + [0xff, 0x0]
                                        self.com.send(RadioPacket(PACKET.RADIO, data=data, optional=optional))
                                    else:
                                        logger.info("sp not in %s" % self.network_data_cnt[p.sender_hex.lower()])
                                    self.outsocket.send_multipart(['enocean', '', json.dumps({self.base_id: {p.sender_hex: ret}})])
                                    self.last_ts = datetime.utcnow()
                                else:
                                    ret = {'description': 'Sensor'}
                                    for k in p.parse_eep(self.network_data_cnt[p.sender_hex.lower()]['rorg_func'], self.network_data_cnt[p.sender_hex.lower()]['rorg_type']):
                                        if k == 'TMP':
                                            ret['Temperature'] = p.parsed[k]['value']
                                        if k == 'HUM':
                                            ret['Relative Humidity'] = p.parsed[k]['value']
                                        if k == 'PIR':
                                            ret['Alarm'] = p.parsed[k]['raw_value']
                                    self.outsocket.send_multipart(['enocean', '', json.dumps({self.base_id: {p.sender_hex: ret}})])
                                    self.last_ts = datetime.utcnow()
                            elif p.learn:
                                data = [p.rorg] + [0x80, 0x8, 0x49] + [0xf0] + self.com.base_id + [0x0]
                                optional = [0x03] + p.sender + [0xff, 0x0]
                                rp = self.com.send(
                                    RadioPacket(PACKET.RADIO, data=data, optional=optional)
                                )
                            else:
                                self.outsocket.send_multipart(['enocean', '', json.dumps({self.base_id: {p.sender_hex: {'rorg': 0xa5}}})])
                                self.last_ts = datetime.utcnow()

                        else:
                            logger.info("received other packet from %s: %s %s" % (p.sender_hex, hex(p.rorg), type(p)))
                    else:
                        logger.info("packet without sender: %s" % str(p.__dict__))

            if self.last_ts is None or self.last_ts + timedelta(seconds=300) < datetime.utcnow():
                self.outsocket.send_multipart(['enocean', '', json.dumps({self.base_id: {}})])
                self.last_ts = datetime.utcnow()

            # fuers erste in jedem durchlauf checken, egal ob grade was gekommen ist oder nicht
            socks = dict(self.poller.poll(10))
            if self.insocket in socks and socks[self.insocket] == zmq.POLLIN:
                try:
                    message = self.insocket.recv(zmq.NOBLOCK)
                    message = json.loads(message.split('enocean ')[1])
                except ZMQError, e:
                    self.update_data = {}
                    if e.errno == zmq.EAGAIN:
                        pass
                    else:
                        raise e
                except ValueError:
                    logging.exception("jsondecodevalueerror")
                    continue
                else:
                    logger.info("received message: %s" % str(message))

                for deviceid, data in message.get('data', {}).items():
                    if data['rorg'] == 0xd2 and data['rorg_func'] == 0x01:  # and data['rorg_type'] == 0x01:
                        out = 0 if data['val'] == 0 else 0x64
                        # if states.get(deviceid) == out:
                        #     continue
                        self.com.send(
                                RadioPacket.create(rorg=RORG.VLD, rorg_func=0x01, rorg_type=0x01, destination=[int(d, 16) for d in deviceid.split(':')], sender=self.com.base_id, command=1, OV=out)
                        )
                        try:
                            self.com.receive.get(block=True, timeout=0.5)
                        except queue.Empty:
                            p = None
                        except Exception as e:
                            p = None
                        else:
                            if p and p.packet_type == PACKET.RESPONSE and p.response == 0x0:  # resp ok
                                # das kommt scheinbar nicht immer in form eines responsepackets ... oder nicht schnell genug?
                                self.outsocket.send_multipart(['enocean', '', json.dumps({self.base_id: {deviceid: {'ack': True, 'raw_value': data['val']}}})])

                        logger.info("sent message to %s" % deviceid)
                    self.network_data_cnt[deviceid] = data

                for deviceid, data in message.get('devicedef', {}).items():
                    self.network_data_cnt[deviceid] = {'rorg_func': data.get('rorg_func'), 'rorg_type': data.get('rorg_type')}

    def stop(self):
        if self.com:
            self.com.stop()
        if not self.zmqcontext.closed:
            self.zmqcontext.destroy()


def main():
    try:
        enodaemon = EnoceanHandler()
        enodaemon.run()
    except KeyboardInterrupt:
        enodaemon.stop()
        sys.exit(0)
    except Exception as e:
        logging.exception("exception instantiating/running handler")
        with open('/sys/class/net/eth0/address') as f:
            macaddr = f.read().strip().replace(':', '-')
        try:
            import traceback
            exc_type, exc_obj, exc_tb = sys.exc_info()
            err = {
                'exc_type': exc_type.__name__,
                'exc_value': str(exc_obj).translate(None, "'").translate(None, '"').translate(None, "!"),
                'exc_trace': [(_a[0].translate(None, '"').translate(None, "'"), _a[1], _a[2].translate(None, '"').translate(None, "'"), _a[3].translate(None, '"').translate(None, "'")) for _a in traceback.extract_tb(exc_tb)]
            }
            enc = local("""python -c "from rpi.aeshelper import aes_encrypt; import json; print aes_encrypt(json.dumps(%s))" """ % err, capture=True)
            requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % enc, timeout=10)
        except:
            requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data={"error": "exception obtaining exception"}, timeout=10)
        enodaemon.stop()
    else:
        enodaemon.stop()

if __name__ == "__main__":
    init_logging()
    main()

