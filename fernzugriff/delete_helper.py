import requests
from fabric.api import local

def delete_keys():

    with open('/sys/class/net/eth0/address') as f:
        mac = f.read()
        mac = mac.strip().replace(':', '-').lower()

        try:
            local("sudo rm /etc/openvpn/ca.crt /etc/openvpn/client.crt /etc/openvpn/client.key /etc/openvpn/ta.key")
        except:
            pass
        mac = mac.lower().replace('-', '')[-6:]
        vac = local("cat /home/pi/rpi/rpi/verification_code.py", capture=True).strip().rsplit(' ', 1)[1].replace("'", "")
        ret = requests.get("https://fwd.controme.com/ctrl/keys/ms%s/?vac=%s&del=1" % (mac, vac))
if __name__ == "__main__":
    delete_keys()
  