import os
from datetime import datetime, timedelta
import pytz
from views import is_compatible_db
from heizmanager.models import Haus
from fabric.api import local
from celery import shared_task
from fabric.state import env


env.hosts = ["localhost"]
PATH = "/home/pi/rpi/backupcontrol/backups"


@shared_task
def periodic_backup():

    # 1. pruefen, ob backupcontrol ueberhaupt aktiviert ist
    try:
        haus = Haus.objects.all()[0]
    except Haus.DoesNotExist:
        return
    else:
        if 'backupcontrol' not in haus.get_modules():
            return

    # 2. pruefen, ob es tatsaechlich 3 uhr morgens ist (falls celery wild wird)
    berlin = pytz.timezone("Europe/Berlin")
    now = datetime.now(berlin)
    now_wotz = datetime.now()
    if now.hour != 3 and now.minute != 0:
        return

    try:
        backups = os.listdir(PATH)
    except OSError:
        local("mkdir %s" % PATH)
        backups = []

    # 3. alle existierenden backups durchgehen und ...
    for backup in backups:
        # 3.1. ...schauen ob aelter als vier wochen (-> loeschen)
        if datetime.fromtimestamp(os.stat("%s/%s" % (PATH, backup)).st_mtime) + timedelta(weeks=4) < now_wotz \
                and backup.endswith("automatic_backup.db"):
            local("rm '%s/%s'" % (PATH, backup), shell="/bin/bash")

        # 3.2. ...kompatibel zum aktuellen schema (-> loeschen)
        if not is_compatible_db("%s/%s" % (PATH, backup)):
            local("rm '%s/%s'" % (PATH, backup), shell="/bin/bash")

    # 4. backup erstellen
    params = haus.get_module_parameters().get('backupcontrol', {'active': False})
    if params['active']:
        local("cp /home/pi/rpi/db.sqlite3 %s/%s_automatic_backup.db" % (PATH, now.strftime("%Y%m%d%H%M%S")))

    return
