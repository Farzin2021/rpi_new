from django.conf.urls import include, url
from heizmanager.mobile import m_error

handler500 = m_error.error500
handler404 = m_error.error404

urlpatterns = [ 
    url(r'^', include('heizmanager.urls')),
    url(r'^', include('users.urls')),
    url(r'^', include('rf.urls')),
    url(r'^', include('jsonapi.urls')),
    url(r'^', include('vorlauftemperaturregelung.urls')),
    url(r'^', include('differenzregelung.urls')),
    url(r'^', include('gatewaymonitor.urls')),
    url(r'^', include('geolocation.urls')),
    url(r'^', include('pumpenlogik.urls')),
    url(r'^', include('fps.urls')),
    url(r'^', include('zweipunktregelung.urls')),
    url(r'^', include('nullregelung.urls')),
    url(r'^', include('ruecklaufregelung.urls')),
    url(r'^', include('telegrambot.urls')),
    url(r'^', include('quickui.urls')),
    url(r'^', include('raumregelung_smart.urls')),
    url(r'^', include('temperaturszenen.urls')),
    url(r'^', include('wetter_pro.urls')),
    url(r'^', include('heizflaechenoptimierung.urls')),
    url(r'^', include('alexa_interface.urls')),
    url(r'^', include('logger.urls')),
    url(r'^', include('kaminofen.urls')),
    url(r'^', include('betriebsarten.urls')),
    url(r'^', include('ki.urls')),
    url(r'^', include('offsets.urls')),
    url(r'^', include('knx.urls')),
    url(r'^', include('fenster_offen.urls')),
    url(r'^', include('api_query.urls')),
    ]
