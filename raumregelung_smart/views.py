
# -*- coding: utf-8 -*-

import pytz
from datetime import datetime, timedelta
from benutzerverwaltung.decorators import multizone_raumregelung_smart_access, has_access_permanent_tab
from heizmanager.mobile.m_temp import _get_haus_for_user, get_offsets_icon
from heizmanager.models import Raum, Haus
import heizmanager.cache_helper as ch
from heizmanager.render import render_response
import logging
from heizmanager.abstracts.base_mode import BaseMode
from quickui.views import QuickuiMode
from quickui.helpers import get_nearest_time
from django.views.decorators.csrf import csrf_exempt
import json
from django.http import HttpResponse

logger = logging.getLogger("logmonitor")


def get_raum_sensor_temperature(raum):
    ms = raum.get_mainsensor()
    temp = None
    if ms:
        last = ms.get_wert()
        if last is not None:
            temp = last[0]
    if temp is None:
        temp = '-'
    else:
        temp = '%.1f' % temp
    return temp


def get_raum_info(raum, mode=None):
    # get offset according to mode
    if mode is None:
        offsets, module_icon = get_offsets_icon(raum)
    else:
        if mode.get('use_offsets', False):
            offsets, module_icon = get_offsets_icon(raum)
        else:
            offsets = 0.0

    temp = get_raum_sensor_temperature(raum)
    soll = '%.1f' % raum.solltemp or 21.0
    ziel = '%.1f' % (float(soll) + float(offsets))
    return temp, soll, ziel, mode


@csrf_exempt
@multizone_raumregelung_smart_access
def raumregelung_smart_view(request):
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)
    haus = _get_haus_for_user(request.user)
    # temperaturmod
    hausmods = haus.get_modules()
    hparams = haus.get_module_parameters()
    tempmode = False
    tempmode_params = hparams.get('temperaturmodi', {})
    if 'temperaturszenen' in hausmods:
        tempmode = True

    if request.method == 'GET':
        ch.set_usage_val("raumliste_quick", 1)
        eundr = []
        if haus:
            for etage in haus.etagen.all():
                e = {etage: []}
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    try:
                        mode = BaseMode.get_active_mode_raum(raum)
                        raum.refresh_from_db()
                    except Exception as e:
                        logging.exception("rlr: exception getting mode for raum %s" % raum.id)
                        mode = None
                    temp, ziel, soll, mode = get_raum_info(raum, mode=mode)
                    e[etage].append((raum, temp, ziel, soll, mode))
                if len(e[etage]):
                    eundr.append(e)

            if len(eundr) == 0:
                eundr = None

        default_time = hparams.get('quickui', {}).get('default_duration', 180)
        time_all = (now + timedelta(minutes=default_time))
        time_all = get_nearest_time(time_all).strftime("%H:%M")
        return render_response(request, "raumregelung_smart_index.html",
                               {'haus': haus, 'eundr': eundr, 'time_all': time_all,
                                'tempmode': tempmode, 'tempmodes': tempmode_params.items(),
                                'default_time': default_time,
                                'permanent_tab': has_access_permanent_tab(request)
                                })

    if request.method == 'POST':
        if 'set_temperature' in request.POST:
            raum_id = request.POST.get("raum_id")
            temperature = request.POST.get("temperature")
            raum = Raum.objects.get(id=raum_id)
            mode = BaseMode.get_active_mode_raum(raum)
            if mode is not None:
                mode['activated_mode'].set_mode_temperature_raum(raum, temperature)
                # make it serializable to return
                del mode['activated_mode']
            else:
                raum.solltemp = temperature
                raum.save()
            ret = get_raum_info(raum, mode)

            ret = {
                "temp": ret[0],
                "soll": ret[1],
                "ziel": ret[2],
                "mode": mode
            }
            return HttpResponse(json.dumps(ret), content_type="application/json")

        if 'del_hand_mode' in request.POST:
            ret = dict(rooms=dict())
            raum = request.POST.get("raum_id")
            try:
                raum = Raum.objects.get(id=raum)
            except Raum.DoesNotExist:
                return HttpResponse('error', status=400)

            QuickuiMode.expire_hand_mode(raum)
            raum.refresh_from_db()
            rooms_details = get_raum_info(raum)
            mode = rooms_details[3]
            rooms_details = {
                "temp": rooms_details[0],
                "soll": rooms_details[1],
                "ziel": rooms_details[2],
                "icon": mode.get('icon', '') if mode else '',
                "mode_text": mode.get('activated_mode_text', '') if mode else ''
            }
            ret['rooms'][raum.id] = rooms_details
            return HttpResponse(json.dumps(ret), content_type="application/json")

        if 'get_enabled_mode_room' in request.POST:
            raum = request.POST.get("raum_id")
            mode_id_lst = json.loads(request.POST.get("mode_id_lst", "{}"))

            try:
                raum = Raum.objects.get(id=raum)
            except Raum.DoesNotExist:
                return HttpResponse('error', status=400)

            tz_room_parameters = raum.get_spec_module_params('temperaturmodi')
            disabled_mode_lst = list()
            for mode_id in mode_id_lst:
                if not tz_room_parameters.get(int(mode_id), dict()).get('enabled', True):
                    disabled_mode_lst.append(mode_id)
            return HttpResponse(json.dumps(disabled_mode_lst), content_type="application/json")
