# coding=utf-8
from datetime import datetime, timedelta
from heizmanager.render import render_response, render_redirect
from pytz.tzinfo import StaticTzInfo
from heizmanager.models import Haus, Raum, Regelung
import pytz
import logging
import time
import copy
import heizmanager.cache_helper as ch
from django.http import HttpResponse
from django.http import JsonResponse
from abc import abstractmethod
from heizmanager.abstracts.base_time_task import TimeTask
from heizprogramm.views import HeizprogrammTask
from temperaturszenen.views import TemperaturszenenTask

logger = logging.getLogger("logmonitor")


class JahreskalenderTask(TimeTask):
    MOD = 'jahreskalender'
    TYPE = ['temperaturszenen', 'heizprogramm']

    @staticmethod
    def get_module_tasks(haus):
        tasks_list = list()
        entries = get_module_entries(haus, JahreskalenderTask.TYPE[1])
        for task in entries:
            if 'next_start' in task:
                task['begin'] = task['next_start'][:-3]
            else:
                task['begin'] = task['begin'].replace("T", " ")
            tasks_list.append(HeizprogrammTask(JahreskalenderTask.TYPE[1], 'activate-heizprogramm',
                                               JahreskalenderTask.MOD, task['begin'],
                                               haus=haus.id, p=TimeTask.HIGH,
                                               heizp_id=task['showing_value']))

        entries = get_module_entries(haus, JahreskalenderTask.TYPE[0])
        for task in entries:
            if 'next_start' in task:
                task['begin'] = task['next_start'][:-3]
                task['end'] = task['next_end'][:-3]
            else:
                task['begin'] = task['begin'].replace("T", " ")
                task['end'] = task['end'].replace("T", " ")

            tasks_list.append(TemperaturszenenTask(JahreskalenderTask.TYPE[0], 'set-mode',
                                                   JahreskalenderTask.MOD, task['begin'], task['end'], TimeTask.LOW,
                                                   hausid=haus.id, priority=1, mode_id=task['showing_value']))

            if task.get('next_mode_id', 0):
                tasks_list.append(TemperaturszenenTask(JahreskalenderTask.TYPE[0], 'set-mode',
                                                       JahreskalenderTask.MOD, task['end'], hausid=haus.id,
                                                       priority=1, mode_id=task['next_mode_id']))
        return tasks_list


def get_name():
    return u'Kalender'


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def is_hidden_offset():
    return False


def calculate_always_anew():
    return False


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/jahreskalender/'>Kalender</a>" % str(haus.id)


def get_global_description_link():
    desc = u"Integrierter, leistungsfähiger Kalender für zeitgesteuertes und geplantes Heizen."
    desc_link = "http://support.controme.com/auswahl-und-aktivierung-von-modulen/modul-jahreskalender/"
    return desc, desc_link


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    context = dict()
    params = haus.get_module_parameters()

    if request.method == "GET":
        context['haus'] = haus
        context['r_m_active'] = params.get('jahreskalender_right_menu', True)
        return render_response(request, "m_settings_jahreskalender.html", context)

    if request.method == "POST":
        if 'right_menu_ajax' in request.POST:
            params['jahreskalender_right_menu'] = bool(int(request.POST['right_menu_ajax']))
            haus.set_module_parameters(params)
            return JsonResponse({'message': 'done'})

        return render_redirect(request, '/m_setup/%s/jahreskalender/' % haus.id)


def get_global_settings_page_help(request, haus):
    pass


class KalenderBaseClass:

    def __init__(self, haus, typ):
        self._typ = typ
        self._haus = haus

    def add(self, index, prepared_dict):
        params = self._haus.get_module_parameters()
        self._haus.set_module_parameters(params)
        params.setdefault('jahreskalender', {})
        if self._typ not in params['jahreskalender']:
            params['jahreskalender'][self._typ] = []
        params['jahreskalender'][self._typ].insert(index, prepared_dict)
        self._haus.set_module_parameters(params)
        self.invalidate_cache(params, index)
        return True

    def delete(self, index):
        params = self._haus.get_module_parameters()
        if 'jahreskalender' in params:
            if isinstance(params['jahreskalender'], dict) and self._typ in params['jahreskalender']:
                try:
                    self.invalidate_cache(params, index)
                    del params['jahreskalender'][self._typ][index]
                    self._haus.set_module_parameters(params)
                    return True
                except IndexError:
                    return False
        return False

    def preparing_dict(self, request, created=None):
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
        week_name = ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']

        prepared_dict = {}

        if 'repeat' in request.POST:
            repeat = request.POST.getlist('repeat')
            prepared_dict['repeat'] = [int(item.encode('utf-8')) for item in repeat]
            prepared_dict['repeat_name'] = [week_name[int(item)] for item in repeat]
        else:
            prepared_dict['repeat'] = []

        if 'begin' in request.POST:
            prepared_dict['begin'] = request.POST['begin']

            begin_int = time.mktime(berlin.localize(
                datetime.strptime(request.POST['begin'], '%Y-%m-%dT%H:%M')
            ).timetuple())

            prepared_dict['begin_int'] = begin_int

        if 'end' in request.POST:
            prepared_dict['end'] = request.POST['end']
            end_int = time.mktime(berlin.localize(
                datetime.strptime(request.POST['end'], '%Y-%m-%dT%H:%M')
            ).timetuple())
            prepared_dict['end_int'] = end_int
        else:
            # those entries that trigger only with start time
            prepared_dict['end'] = None

        if 'end_repeat' in request.POST:
            prepared_dict['end_repeat'] = request.POST['end_repeat']

        if not created:
            created = now.strftime("%Y-%m-%d %H:%M")

        prepared_dict['created'] = created
        prepared_dict['created_int'] = time.mktime(berlin.localize(
            datetime.strptime(created, '%Y-%m-%d %H:%M')
        ).timetuple())

        prepared_dict['edited'] = now.strftime("%Y-%m-%d %H:%M:%S")
        prepared_dict['edited_int'] = time.mktime(now.timetuple())

        prepared_dict['name'] = request.POST.get('name', '')
        prepared_dict['typ'] = self._typ

        return prepared_dict

    def get_list(self, raum=None):
        params = self._haus.get_module_parameters()
        params = adapt_with_new_structure(self._haus, copy.deepcopy(params))

        lst = params.get('jahreskalender', dict())
        lst = lst.get(self._typ, [])
        if raum is not None:
            raum_list = []
            for item in lst:
                if str(raum) in item['rooms']:
                    raum_list.append(item)
            return raum_list
        return lst

    def get_tasks_list(self):
        lst = self.get_list()
        _lst, bad_items = next_event(lst)
        return _lst

    def invalidate_cache(self, params, index):
        rooms = params['jahreskalender'][self._typ][index].get('rooms', [])
        diffs = params['jahreskalender'][self._typ][index].get('diffs', [])
        vors = params['jahreskalender'][self._typ][index].get('vors', [])

        for room_id in rooms:
            ch.delete_cache_upcoming_events(self._haus.id, 'timer', entity_id=room_id, entity_type='Raum')
            ch.set_module_offset_raum("jahreskalender", self._haus.id, room_id, None)
            ch.set("%s_roffsets_dict" % room_id, None)

        for diff_id in diffs:
            ch.delete_cache_upcoming_events(self._haus.id, 'timer', entity_id=diff_id, entity_type='differenzregelung')
            ch.set_module_offset_regelung("jahreskalender", self._haus.id, 'differenzregelung', str(diff_id), None)

        for vor_id in vors:
            ch.delete_cache_upcoming_events(self._haus.id, 'timer', entity_id=vor_id, entity_type='vorlauftemperaturregelung')
            ch.set_module_offset_regelung("jahreskalender", self._haus.id, 'vorlauftemperaturregelung', str(vor_id), None)

        return True

    @abstractmethod
    def add_entry(self, request, index=0, created=None):
        pass

    @abstractmethod
    def get_list_api(self):
        pass


class KalenderHausRaum(KalenderBaseClass):

    def __init__(self, haus):
        self._haus = haus
        KalenderBaseClass.__init__(self, haus, 'hausraum')

    def add_entry(self, request, index=0, created=None):
        if created:
            preparing_dict = self.preparing_dict(request, created)
        else:
            preparing_dict = self.preparing_dict(request)

        params = self._haus.get_module_parameters()
        rooms = request.POST.getlist('rooms')
        diffs = request.POST.getlist('diffs')
        vors = request.POST.getlist('vors')
        fps = request.POST.get('fps')
        _rooms = []
        rooms = [item.encode('utf-8') for item in rooms]
        diffs = [item.encode('utf-8') for item in diffs]
        vors = [item.encode('utf-8') for item in vors]
        fps = [fps.encode('utf-8')] if fps else []
        rooms_name = []
        diffs_name = []
        vors_name = []
        reg_params = params.get('differenzregelung', {})
        for item in diffs:
            if item in reg_params:
                diffs_name.append(reg_params[item]['name'])
        reg_params = params.get('vorlauftemperaturregelung', {})
        for item in vors:
            if item in reg_params:
                vors_name.append(reg_params[item]['name'])
        for e in self._haus.etagen.all():
            for raum in Raum.objects.filter_for_user(request.user, etage_id=e.id):
                if str(raum.id) in rooms:
                    rooms_name.append(raum.name)
                    _rooms.append(str(raum.id))
                    ch.set("%s_roffsets_dict" % raum.id, None)
                    activate(raum)

        preparing_dict['rooms'] = _rooms
        preparing_dict['diffs'] = diffs
        preparing_dict['vors'] = vors
        preparing_dict['fps'] = fps
        preparing_dict['rooms_name'] = rooms_name
        preparing_dict['diffs_name'] = diffs_name
        preparing_dict['vors_name'] = vors_name
        preparing_dict['offset'] = request.POST.get('offset', 0.0)
        preparing_dict['showing_text'] = preparing_dict['offset']
        preparing_dict['showing_value'] = preparing_dict['offset']
        self.add(index, preparing_dict)
        return True

    def get_list_api(self):
        lst = self.get_list()
        _lst, bad_items = next_event(lst)
        ret = []
        for index, params in enumerate(_lst):
            if params.get('next_start', None):
                ret.append({'id': index, "name": params.get('name', ''),
                            'next_start_time': params.get('next_start', ''),
                            'next_end_time': params.get('next_end', ''),
                            'raeume': params.get('rooms_name', []),
                            'offset': params.get('offset', None)
                            })
        return ret


class KalenderTemperaturszenen(KalenderBaseClass):

    def __init__(self, haus):
        self._haus = haus
        KalenderBaseClass.__init__(self, haus, 'temperaturszenen')

    def add_entry(self, request, index=0, created=None):
        if created:
            preparing_dict = self.preparing_dict(request, created)
        else:
            preparing_dict = self.preparing_dict(request)

        params = self._haus.get_module_parameters()

        mode_id = int(request.POST['select-mode'])
        preparing_dict['mode_id'] = mode_id
        preparing_dict['mode_name'] = params.get('temperaturmodi', {}).get(mode_id, {}).get('name', '')
        preparing_dict['showing_text'] = preparing_dict['mode_name']
        preparing_dict['showing_value'] = preparing_dict['mode_id']
        preparing_dict['next_mode_id'] = int(request.POST.get('next-select-mode', 0))
        self.add(index, preparing_dict)
        return True

    def get_list_api(self):
        lst = self.get_list()
        _lst, bad_items = next_event(lst)
        ret = []
        for index, params in enumerate(_lst):
            if params.get('next_start', None):
                ret.append({'id': index, "name": params.get('name', ''),
                            'next_start_time': params.get('next_start', ''),
                            'next_end_time': params.get('next_end', ''),
                            'temperaturszene': params.get('showing_text', []),
                            'offset': None
                            })
        return ret


class KalenderHeizProgramm(KalenderBaseClass):

    def __init__(self, haus):
        self._haus = haus
        KalenderBaseClass.__init__(self, haus, 'heizprogramm')

    def add_entry(self, request, index=0, created=None):
        if created:
            preparing_dict = self.preparing_dict(request, created)
        else:
            preparing_dict = self.preparing_dict(request)

        params = self._haus.get_module_parameters()

        heizp_id = int(request.POST['select-heizpid'])
        preparing_dict['heizp_id'] = heizp_id
        heizp_list = get_heizprogramm_list(self._haus, params)
        heizp = (item for item in heizp_list if item["heizp_id"] == heizp_id).next()
        preparing_dict['heizp_name'] = heizp.get('heizp_name', '')
        preparing_dict['showing_text'] = preparing_dict['heizp_name']
        preparing_dict['showing_value'] = preparing_dict['heizp_id']
        self.add(index, preparing_dict)
        return True

    def get_list_api(self):
        lst = self.get_list()
        _lst, bad_items = next_event(lst)
        ret = []
        for index, params in enumerate(_lst):
            if params.get('next_start', None):
                ret.append({'id': index, "name": params.get('name', ''),
                            'next_start_time': params.get('next_start', ''),
                            'next_end_time': None,
                            'heizprogramm': params.get('showing_text', []),
                            'offset': None
                            })
        return ret


kalender_factory = {
    'hausraum': KalenderHausRaum,
    'temperaturszenen': KalenderTemperaturszenen,
    'heizprogramm': KalenderHeizProgramm
}


def get_heizprogramm_list(haus, hparams=None):

    if hparams == None:
        hparams = haus.get_module_parameters()
    if 'heizprogramms_list' not in hparams:
        heizp_list = [{'heizp_id': 0, 'heizp_name': 'Standard'}]
    else:
        heizp_list = hparams.get('heizprogramms_list', [])
    return heizp_list


def get_list_regelung(params, module_name=None, objid=None):
    lst = copy.deepcopy(params.get('jahreskalender', list()))
    lst = lst.get("hausraum", [])
    if objid is not None:
        _list = []
        for item in lst:
            if module_name == 'differenzregelung':
                if 'diffs' in item:
                    if objid in item['diffs']:
                        _list.append(item)
            elif module_name == 'vorlauftemperaturregelung':
                if 'vors' in item:
                    if objid in item['vors']:
                        _list.append(item)
        return _list
    return lst


def convert_date(lst):
    output = []
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)
    for index, item in enumerate(lst):

        try:
            # simple input 2015-11-11T23:11
            # end_repeat = berlin.localize(
            end_repeat = datetime.strptime(item['end_repeat'], '%Y-%m-%dT%H:%M')
            # )

            # begin = berlin.localize(
            begin = datetime.strptime(item['begin'], '%Y-%m-%dT%H:%M')
            # )

            # end = berlin.localize(
            if 'end' in item and item['end']:
                end = datetime.strptime(item['end'], '%Y-%m-%dT%H:%M')
                # )
            else:
                # those entries that trigger only with start time
                end = begin + timedelta(minutes=5)
        except:
            logging.exception("problem with date conversion")
            continue
        # add begin and end
        to_append = copy.deepcopy(item)
        to_append.update({
            'begin': berlin.localize(begin),
            'end': berlin.localize(end),
            'index': index,
        })
        output.append(to_append)
        repeat_delta = (berlin.localize(end_repeat) - berlin.localize(begin)).days + 1

        # start from which day ?
        start = 0
        # limit the loop to two weeks from past week to next week if end and start are very long
        if repeat_delta > 14:
            if berlin.localize(begin) > now:
                start = (berlin.localize(begin) - (now - timedelta(days=7))).days
            else:
                start = ((now - timedelta(days=7)) - berlin.localize(begin)).days
            repeat_delta = ((now + timedelta(days=7)) - berlin.localize(begin)).days
        end_delta = end - begin

        for i in range(start, repeat_delta):
            repeat_day = berlin.localize(begin + timedelta(days=i))
            repeat_end = repeat_day + end_delta
            # skip for overlay
            if berlin.localize(begin) <= repeat_day <= berlin.localize(end) or berlin.localize(begin) <= repeat_end <= berlin.localize(end):
                continue

            if int(repeat_day.strftime('%w')) in item['repeat']:
                to_append = copy.deepcopy(item)
                to_append.update(
                    {
                        'begin': repeat_day,
                        'end': repeat_end,
                        'index': index
                    }
                )
                output.append(to_append)
    return output


def next_event(lst):

    list_data = convert_date(lst)
    bad_items = []
    ret = []

    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)

    for item in list_data:
        index = item['index']
        try:
            # convert the date for view
            if lst[index]['end']:
                end = berlin.localize(
                    datetime.strptime(lst[index]['end'], '%Y-%m-%dT%H:%M')
                ).strftime("%Y-%m-%d %H:%M:%S")
            else:
                # those entries that trigger only with start time
                end = (datetime.strptime(lst[index]['begin'], '%Y-%m-%dT%H:%M') + timedelta(minutes=5)).strftime('%Y-%m-%dT%H:%M')

            end_repeat = berlin.localize(
                datetime.strptime(lst[index]['end_repeat'], '%Y-%m-%dT%H:%M')
            ).strftime("%Y-%m-%d %H:%M:%S")

            begin = berlin.localize(
                datetime.strptime(lst[index]['begin'], '%Y-%m-%dT%H:%M')
            ).strftime("%Y-%m-%d %H:%M:%S")

            lst[index]['begin_str'] = begin
            lst[index]['end_str'] = end
            lst[index]['end_repeat_str'] = end_repeat

            if item['begin'] < now < item['end'] and 'next_start' not in lst[index]:
                lst[index]['active'] = True

            if item['begin'] > now and 'next_start' not in lst[index]:
                lst[index]['next_start'] = item['begin'].strftime("%Y-%m-%d %H:%M:%S")
                lst[index]['next_end'] = item['end'].strftime("%Y-%m-%d %H:%M:%S")
                # continue

            elif item['begin'] < now < item['end']:
                lst[index]['next_start'] = item['begin'].strftime("%Y-%m-%d %H:%M:%S")
                lst[index]['next_end'] = item['end'].strftime("%Y-%m-%d %H:%M:%S")
                # continue

            if len(ret) > index:
                ret[index] = copy.deepcopy(lst[index])
            else:
                ret.append(copy.deepcopy(lst[index]))
        except:
            logging.exception("bad_item with index %s" % index)
            bad_items.append(index)

    return ret, bad_items


def get_offset(haus, raum=None):
    hr = KalenderHausRaum(haus)
    lst = convert_date(hr.get_list(str(raum.id)))

    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)

    index = []
    offset = 0.0
    min_end = None
    for item in lst:
        if item['begin'] < now < item['end'] and item['index'] not in index:
            index.append(item['index'])
            offset += float(item['showing_value'])
            if min_end is None or item['end'] < min_end:
                min_end = item['end']

    if len(index) and min_end is not None:
        for item in lst:
            if item['index'] not in index and item['begin'] < min_end:
                min_end = item['begin']

    ttl = (min_end - now).total_seconds() if min_end is not None else 300
    ch.set_module_offset_raum("jahreskalender", haus.id, raum.id, offset, ttl=ttl)

    return offset


def get_offset_regelung(haus, module=None, objid=None):
    params = haus.get_module_parameters()
    params = adapt_with_new_structure(haus, params)
    lst = convert_date(get_list_regelung(params, module_name=module, objid=objid))
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)

    index = []
    offset = 0.0
    min_end = None
    for item in lst:
        if item['begin'] < now < item['end'] and item['index'] not in index:
            index.append(item['index'])
            offset += float(item['showing_value'])
            if min_end is None or item['end'] < min_end:
                min_end = item['end']
    
    if len(index) and min_end is not None:
        for item in lst:
            if item['index'] not in index and item['begin'] < min_end:
                min_end = item['begin']

    ttl = (min_end - now).total_seconds() if min_end is not None else 300
    ch.set_module_offset_regelung("jahreskalender", haus.id, module, str(objid), offset, ttl=ttl)

    return offset


def upcoming_event_type(typ):
    types = {
        'start': 'starting-offset',
        'end': 'ending-offset',
        'end_list': 'ending-list',
    }
    return types[typ]


def entity_list_factory(haus, entity, entity_id):
    params = haus.get_module_parameters()
    hr = KalenderHausRaum(haus)
    lst = []
    # get list
    if isinstance(entity, Regelung):
        entity_str = unicode(entity)
        lst = get_list_regelung(params, entity_str, entity_id)
    elif isinstance(entity, Raum):
        lst = hr.get_list(entity_id)

    # convert list
    lst = convert_date(lst)
    return lst


def get_module_upcoming_events(haus, entity, entity_id, lookahead=12):
    """
    :param haus:
    :param entity: gives an entity object like (room, haus, or regelung)
    :param entity_id: gives object Id
    :param lookahead: gives a value that shows how many hours should function calculate with
    :return: returns changing offset within given lookahead and show with
                 a list of tuples ('offset', 'time', 'type') type here is offset,
    """

    # get model name
    entity_str = ''
    if isinstance(entity, Regelung):
        entity_str = unicode(entity)
    else:
        entity_str = entity.__class__.__name__
    #

    lst = entity_list_factory(haus, entity, entity_id)
    berlin_timezone = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin_timezone)
    time_at_lookahead_h = datetime.now(berlin_timezone) + timedelta(hours=lookahead)
    ret = []
    index = []
    end_of_list_time = time_at_lookahead_h

    #from bisect import bisect_left
    for item in lst:
        # check if item is overlap with now to lookahead
        if (item['begin'] <= time_at_lookahead_h) and (item['end'] >= now) and item['index'] not in index:
            end_of_list_time = max(time_at_lookahead_h, item['end'])
            index.append(item['index'])
            offset = float(item['showing_value'])
            ret.append((item['begin'], offset, item['end']))

    # initial list with first record
    ret_list = []
    if len(ret):
        first_entry = min(ret, key=lambda t: t[0])
        # move minimum of starting entry to start of list
        ret.remove(first_entry)
        ret.insert(0, first_entry)
        ret_list = [
            (first_entry[0], first_entry[1], upcoming_event_type('start')),
            (first_entry[2], 0, upcoming_event_type('end'))
        ]

        for i, tupi in enumerate(ret[1: len(ret)]):
            entry_offset = float(tupi[1])
            start_offset = entry_offset
            end_offset = entry_offset
            for j, tupj in enumerate(ret):
                if i + 1 == j:
                    continue

                if tupj[0] <= tupi[0] <= tupj[2]:
                    start_offset += float(tupj[1])

                if tupj[0] <= tupi[2] <= tupj[2]:
                    end_offset += float(tupj[1])

            ret_list.append((tupi[0], start_offset, upcoming_event_type('start')))
            ret_list.append((tupi[2], end_offset - entry_offset, upcoming_event_type('end')))
            # # insert begin to a sorted list
            # idx = bisect_left(ret_list, (tupi[0], None, None))
            # ret_list.insert(idx, (tupi[0], offset, upcoming_event_type('start')))
            #
            # # insert end to a sorted list
            # idx = bisect_left(ret_list,  (tupi[2], None, None))
            # ret_list.insert(idx, (tupi[2], offset - entry_offset, upcoming_event_type('end')))
    ret_list = sorted(ret_list, key=lambda t: t[0])

    ret_list.append((end_of_list_time, None, upcoming_event_type('end_list')))

    ch.set_cache_upcoming_events('jahreskalender', haus.id, entity_id, entity_str, ret_list, ttl=lookahead * 60 * 60)
    return ret_list


def get_local_settings_page_haus(request, haus):

    return _display_jahreskalender(request, haus, None)


def get_local_settings_link(request, raum):
    haus = raum.etage.haus
    offset = get_offset(haus, raum)
    if offset != 0.0:
        return "Kalender", "%.2f" % offset, 75, "/m_raum/%s/jahreskalender/" % raum.id
    else:
        return "Kalender", "deaktiviert", 75, "/m_raum/%s/jahreskalender/" % raum.id


def get_local_settings_link_regelung(haus, modulename, objid):
    offset = get_offset_regelung(haus, modulename, objid)
    return get_name(), "Kalender", offset, 75, "/m_%s/%s/show/%s/jahreskalender/" % (modulename, haus.id, objid)


def get_local_settings_page(request, raum):

    return _display_jahreskalender(request, None, raum)


def adapt_with_new_structure(haus, params):
    # adapt before entries to new
    if 'jahreskalender' in params:
        jahr_params = copy.deepcopy(params['jahreskalender'])
        if isinstance(jahr_params, list):
            for index, item in enumerate(jahr_params):
                if 'offset' in item:
                    jahr_params[index]['showing_value'] = item['offset']
                    jahr_params[index]['showing_text'] = item['offset']
            params['jahreskalender'] = {'hausraum': jahr_params}
            haus.set_module_parameters(params)

    if 'new_jahreskalender' in params:
        params['jahreskalender'] = params['new_jahreskalender']
        del params['new_jahreskalender']
        haus.set_module_parameters(params)

    return params


def _display_jahreskalender(request, haus=None, raum=None, regelung=None):

    # vorsicht: immer auf raum is None pruefen fuer urls etc
    object = "raum"
    objid = None
    if regelung:
        objid = regelung[1]
        object = "regelung"
        regelung = regelung[0]

    if haus is None:
        haus = raum.etage.haus

    params = haus.get_module_parameters()
    lst_dict = {}
    for item in kalender_factory.keys():
        lst_dict[item] = kalender_factory[item](haus).get_list()

    if request.method == "GET":

        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin).strftime('%Y-%m-%dT%H:%M')
        next_hour = (datetime.now(berlin) + timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M')

        # get diff and vorlauf params
        reg_hparams = params.get('differenzregelung', {})
        regs = Regelung.objects.filter_for_user(request.user).filter(regelung='differenzregelung')
        diff_params = {}
        for d_reg in regs:
            dreg_id = d_reg.get_parameters()['dreg']
            diff_params[dreg_id] = reg_hparams[dreg_id]

        reg_hparams = params.get('vorlauftemperaturregelung', {})
        regs = Regelung.objects.filter_for_user(request.user).filter(regelung='vorlauftemperaturregelung')
        vor_params = {}
        for d_reg in regs:
            mid = d_reg.get_parameters()['mid']
            vor_params[mid] = reg_hparams[mid]

        search_name = None
        if raum:
            search_name = raum.name
        if regelung == "differenzregelung":
            search_name = diff_params[objid].get('name', None)

        if regelung == "vorlauftemperaturregelung":
            search_name = vor_params[objid].get('name', None)

        if 'edit' in request.GET:
            index = int(request.GET['edit'])
            typ = request.GET['typ']
            if typ == "hausraum":
                selected_rooms = [int(rid) for rid in lst_dict['hausraum'][int(request.GET['edit'])]['rooms']]
                eundr = []
                for etage in haus.etagen.all():
                    e = {etage: []}
                    for _raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                        e[etage].append(_raum)
                    eundr.append(e)

                list_diff = lst_dict['hausraum'][index].get('diffs', [])
                list_vor = lst_dict['hausraum'][index].get('vors', [])
                list_fps = lst_dict['hausraum'][index].get('fps', [])
                if 'fps' in haus.get_modules():
                    has_fps = True
                else:
                    has_fps = False
                return render_response(request, "m_%s_jahreskalender_add_edit.html" % object,
                                       {"raum": raum, "haus": haus, "data": lst_dict[typ][index], "index": index,
                                        "now": now, "next_hour": next_hour, "selected_rooms": selected_rooms,
                                        "eundr": eundr, 'diff_params': sorted(diff_params.iteritems()),
                                        'list_diff': list_diff, 'list_fps': list_fps, 'has_fps': has_fps,
                                        'list_vor': list_vor, 'vor_params': sorted(vor_params.iteritems()),
                                        "objid": objid, "regelung": regelung, "typ": typ})

            elif typ == "temperaturszenen":
                temp_modes = params.get("temperaturmodi", {})
                return render_response(request, "m_%s_jahreskalender_add_edit.html" % object,
                                       {"raum": raum, "haus": haus, "data": lst_dict[typ][index], "index": index,
                                        "now": now, "next_hour": next_hour, "typ": typ, "select_modes": temp_modes.iteritems})

            elif typ == "heizprogramm":
                select_heizps = get_heizprogramm_list(haus, params)
                return render_response(request, "m_%s_jahreskalender_add_edit.html" % object,
                                       {"raum": raum, "haus": haus, "data": lst_dict[typ][index], "index": index,
                                        "now": now, "next_hour": next_hour, "typ": typ, "select_heizps": select_heizps})

        elif 'add' in request.GET:
            eundr = []
            for etage in haus.etagen.all():
                e = {etage: []}
                for _raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    e[etage].append(_raum)
                eundr.append(e)
            access_list_diff = []
            access_list_vor = []

            list_diff = []
            list_vor = []
            if regelung == "differenzregelung":
                list_diff = [objid]
            if regelung == "vorlauftemperaturregelung":
                list_vor = [objid]

            if 'fps' in haus.get_modules():
                has_fps = True
            else:
                has_fps = False

            temp_modes = params.get("temperaturmodi", {})
            select_heizps = get_heizprogramm_list(haus, params)
            return render_response(request, "m_%s_jahreskalender_add_edit.html" % object,
                                   {"raum": raum, "haus": haus, "now": now, "next_hour": next_hour, "eundr": eundr,
                                    'diff_params': sorted(diff_params.iteritems()), 'access_list_diff': access_list_diff,
                                    'access_list_vor': access_list_vor, 'vor_params': sorted(vor_params.iteritems()),
                                    'list_vor': list_vor, 'list_diff': list_diff, "objid": objid, "regelung": regelung,
                                    "select_modes": temp_modes.iteritems, 'select_heizps': select_heizps, 'has_fps': has_fps})

        elif 'delete' in request.GET:
            ch.delete_module_tasks(haus.id, 'quickui')
            ch.delete_module_tasks(haus.id, 'jahreskalender')
            index = int(request.GET['delete'])
            if 'typ' in request.GET:
                typ = request.GET['typ']
                kalender = kalender_factory[typ](haus)
                if not kalender.delete(index):
                    return HttpResponse("id does not exist")
            else:
                return HttpResponse("please define type")

            return render_redirect(request, '/m_jahreskalender/%s/' % haus.id)

        else:
            now_r = datetime.now(berlin).strftime('%Y-%m-%d %H:%M:00')

            for k, v in lst_dict.items():
                lst_dict[k], bad_items = next_event(v)
                for bi in bad_items:
                    try:
                        del params['jahreskalender'][k][bi]
                    except IndexError:
                        pass

            if len(bad_items):
                haus.set_module_parameters(params)

            if request.user.userprofile.get().role != 'A':
                raeume = {r[0] for r in Raum.objects.filter_for_user(request.user, etage__haus_id=haus.id).values_list('id')}
                _lst = []
                for item in lst_dict['hausraum']:
                    if len(set([int(r) for r in item['rooms']]) - raeume):
                        item['hide'] = True
                    else:
                        item['hide'] = False
                    _lst.append(item)

                lst_dict['hausraum'] = _lst
            else:
                for k, item in lst_dict.items():
                    [item.update({'hide': False}) for item in lst_dict[k]]
            return render_response(request, "m_%s_jahreskalender.html" % object,
                                   {"raum": raum, "haus": haus, "list": lst_dict, "now": now_r,
                                    'search_name': search_name, "objid": objid, "regelung": regelung})

    elif request.method == "POST":
        ch.delete_module_tasks(haus.id, 'jahreskalender')
        typ = request.POST['typ']
        kalender = kalender_factory[typ](haus)
        lst = kalender.get_list()
        if 'index' in request.POST:
            index = int(request.POST['index'])
            created = lst[index]['created']
            if kalender.delete(index):
                kalender.add_entry(request, index, created)
            else:
                return HttpResponse("there is something wrong with index, maybe the index does not exist")
        else:

            kalender.add_entry(request, len(lst))

        return render_redirect(request, '/m_jahreskalender/%s/' % haus.id)


def get_local_settings_page_regelung(request, haus, modulename, objid, params):
    return _display_jahreskalender(request, haus=haus, regelung=(modulename, objid))


class OffsetTime(StaticTzInfo):
    def __init__(self, offset):
        """A dumb timezone based on offset such as +0530, -0600, etc."""
        hours = int(offset[:3])
        minutes = int(offset[0] + offset[3:])
        self._utcoffset = timedelta(hours=hours, minutes=minutes)


def activate(hausoderraum):
    if 'jahreskalender' not in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['jahreskalender'])


def get_module_entries(haus, typ):
    kalender = kalender_factory[typ](haus) if typ in kalender_factory else []
    return kalender.get_tasks_list()


def get_module_variables(haus):
    variables_list = []
    lst_dict = {}
    for item in kalender_factory.keys():
        lst_dict[item] = kalender_factory[item](haus).get_list()
    list_fps = []
    for idx, item in enumerate(lst_dict['hausraum']):
        if item.get('fps') == ['all']:
            lst, _ = next_event([item])
            offset = 0
            if len(lst):
                berlin = pytz.timezone('Europe/Berlin')
                now = datetime.now(berlin)
                if 'next_start' in lst[0] and 'next_end' in lst[0] and lst[0]['next_start'] < now.strftime("%Y-%m-%d %H:%M:%S") < lst[0]['next_end']:
                    offset = float(lst[0]['showing_value'])
            list_fps.append((idx, item.get('name', 'Unbenannt'), offset))

    variables_list.append({'module_name': 'jahreskalender', 'variables': list_fps, 'verbose_module_name': get_name()})

    return variables_list


def get_jsonapi(haus, usr, entityid=None):

    ret_lst = []
    for item in kalender_factory.keys():
        ret_lst += kalender_factory[item](haus).get_list_api()

    return ret_lst
