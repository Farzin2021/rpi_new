from heizmanager.tests import BaseModeTest
from .views import JahreskalenderTask
from datetime import timedelta


class JahreskalenderTest(BaseModeTest):

    def setUp(self):
        super(JahreskalenderTest, self).setUp('jahreskalender/fixtures/fixture.json')

    def test_FixATemperaturszeneInARoomWithNoOtherActiveHeizprogrammTemperaturszeneUI(self):
        # set a fixed mode
        self.set_jahreskalender_entry_ui('temperaturszenen', 2)
        # mode must not get changed after running switching mode
        self.assert_temperatureszene_is_activated_in_room(3, 1)
        self.run_auto_switch()
        self.assert_temperatureszene_is_activated_in_room(3, 2)

    def test_FixATemperaturszeneInARoomWithNoOtherActiveHeizprogrammTemperaturszeneDB(self):
        # set a fixed mode
        self.set_jahreskalender_entry_db('temperaturszenen', 2)
        # mode must not get changed after running switching mode
        self.assert_temperatureszene_is_activated_in_room(3, 1)
        self.run_auto_switch()
        self.assert_temperatureszene_is_activated_in_room(3, 2)

    def test_FixAHeizprogrammInARoomWithNoOtherActiveHeizprogrammUI(self):
        self.set_jahreskalender_entry_ui('heizprogramm', 1)
        self.run_auto_switch()
        self.assert_heizprogramm_activated(1)

    def test_FixAHeizprogrammInARoomWithNoOtherActiveHeizprogrammDB(self):
        self.set_jahreskalender_entry_db('heizprogramm', 1)
        self.assert_heizprogramm_activated(0)
        self.run_auto_switch()
        self.assert_heizprogramm_activated(1)

    def test_FixATemperaturszeneInARoomWithAnotherActivHeizprogrammTemperaturszene(self):
        self.set_jahreskalender_entry_ui('temperaturszenen', 1)
        self.set_jahreskalender_entry_ui('heizprogramm', 1)
        self.set_heizprogramm_entry_ui(mode_id=2, heizp_id=1)
        self.assert_heizprogramm_activated(0)
        self.assert_temperatureszene_is_activated_in_room(room_id=3, mode_id=1)
        self.run_auto_switch()
        self.assert_heizprogramm_activated(1)
        self.assert_temperatureszene_is_activated_in_room(room_id=3, mode_id=1)

    def test_FixAHeizprogrammInARoomWithAnotherActiveHeizprogrammWithDifferentSchedule(self):
        self.set_heizprogramm_entry_ui(mode_id=2, heizp_id=0)
        self.assert_heizprogramm_activated(0)
        self.assert_temperatureszene_is_activated_in_room(room_id=3, mode_id=1)
        self.set_jahreskalender_entry_ui('heizprogramm', 1)
        self.set_heizprogramm_entry_ui(mode_id=1, heizp_id=1)
        self.run_auto_switch()
        self.assert_heizprogramm_activated(1)
        self.assert_temperatureszene_is_activated_in_room(room_id=3, mode_id=1)

    def test_GetModuleTasksFunctionality(self):
        haus = self.get_haus_by_id(1)
        self.set_jahreskalender_entry_db('temperaturszenen', 2)
        module_tasks = JahreskalenderTask.get_module_tasks(haus)
        self.assertEqual(len(module_tasks), 2)
        self.run_modules_timer()
        self.assert_temperatureszene_is_activated_in_room(3, 2)

    def test_ActivateHeizprogrammByTimeTaskFunctionality(self):
        haus = self.get_haus_by_id(1)
        self.set_jahreskalender_entry_ui('heizprogramm', 1)
        module_tasks = JahreskalenderTask.get_module_tasks(haus)
        self.assertEqual(len(module_tasks), 1)
        self.run_modules_timer()
        self.assert_heizprogramm_activated(1)

    def test_ActivateHeizprogrammInPastTimeButRepeatingDays(self):
        haus = self.get_haus_by_id(1)
        begin = self.berlin_now - timedelta(days=7)
        today_wd = self.berlin_now.isoweekday()
        self.set_jahreskalender_entry_ui('heizprogramm', 1, begin, repeat=today_wd)
        module_tasks = JahreskalenderTask.get_module_tasks(haus)
        self.assertEqual(len(module_tasks), 1)
        self.run_modules_timer()
        self.assert_heizprogramm_activated(1)

    def test_ActivateHeizprogrammInPastTimeButRepeatingDays(self):
        begin = self.berlin_now - timedelta(days=7)
        end = self.berlin_now - timedelta(days=6)
        today_wd = self.berlin_now.isoweekday()
        self.set_jahreskalender_entry_ui('temperaturszenen', 2, begin, end, repeat=today_wd)
        self.assert_temperatureszene_is_activated_in_room(3, 1)
        self.run_auto_switch()
        self.assert_temperatureszene_is_activated_in_room(3, 2)
