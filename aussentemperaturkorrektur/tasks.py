from celery import shared_task
from fabric.state import env


env.hosts = ["localhost"]


@shared_task
def calc_offset():

    from heizmanager.models import Haus
    from views import get_offset

    for haus in Haus.objects.all():
        if 'aussentemperaturkorrektur' not in haus.get_modules():
            continue
        get_offset(haus)
