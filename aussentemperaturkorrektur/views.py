# coding=utf-8

from datetime import datetime, timedelta
from heizmanager.render import render_response, render_redirect
from heizmanager.models import Haus, Raum, Regelung
import pytz
from django.http import HttpResponse
from django.http import JsonResponse
import ast
import logging
import heizmanager.cache_helper as ch
from heizmanager.mobile.m_setup import set_regelung_entity, get_regelung_entity


try:
    from wetter.views import get_10dayforecast
except ImportError:
    def _gf():
        logging.warning("asdf")
        return None
    get_10dayforecast = _gf


def get_name():
    return 'Außentemperaturkorrektur'


def get_cache_ttl():
    return 3600


def is_togglable():
    return True


def get_forecast_average(haus, begin, end, forecast=None):
    end = min(end, 72)
    if forecast is None:
        forecast = get_10dayforecast(haus, int(end))
    if forecast is None:
        return None, None
    if int(end) == int(begin):
        end = int(end) + 1
    return forecast[int(begin):int(end)], sum([int(f[1]) for f in forecast[int(begin):int(end)] if isinstance(f, tuple)]) / float((int(end)-int(begin)))


def _calculate_offset(haus, _forecast, params):

    rforecast, avg = get_forecast_average(haus, params.get('start_time', 0), params.get('end_time', 5), forecast=_forecast)
    end1 = float(params.get('heating_element_end_1', 0))
    end2 = float(params.get('heating_element_end_2', 0))

    a = (end1, 20)
    b = (end2, -20)
    c = (-10, avg)
    d = (10, avg)

    line1 = (a, b)
    line2 = (c, d)

    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(_a, _b):
        return _a[0] * _b[1] - _a[1] * _b[0]

    div = (det(xdiff, ydiff))
    if div == 0:
        raise Exception('lines do not intersect')

    _d = (det(*line1), det(*line2))
    x = float(det(_d, xdiff)) / float(div)
    # y = float(det(d, ydiff)) / float(div)
    return x


def get_offset(haus, raum=None):

    if raum is None:
        return 0.0

    else:
        offset = ch.get_module_offset_raum("aussentemperaturkorrektur", haus.id, raum.id)
        if offset is not None:
            return offset
        activate(raum)
        atkparams = raum.get_atkorrektur_params()
        try:
            forecast = get_10dayforecast(haus, min(atkparams.get('end_time', 5), 72))
            offset = _calculate_offset(haus, forecast, atkparams)
        except ValueError:
            offset = 0.0
            logging.error("atk raum %s: setting offset to 0.0 because valueerror" % raum.id)
        except TypeError:
            offset = 0.0
            logging.error("atk raum %s: setting offset to 0.0 because typerror (probably no forecast)" % raum.id)
        ch.set_module_offset_raum(
            "aussentemperaturkorrektur", haus.id, raum.id, offset, ttl=(3600-datetime.now().minute*60)
        )

        return offset


def is_hidden_offset():
    return False


def calculate_always_anew():
    return False


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/aussentemperaturkorrektur/'>Außentemperaturkorrektur</a>" % str(haus.id)


def get_global_description_link():
    desc = u"Kompensiert die Abhängigkeit der Wohlfühltemperatur von der Außentemperatur."
    desc_link = "http://support.controme.com/aussentemperaturkorrektur/"
    return desc, desc_link


def has_ms_and_rls(raum):
    if raum.get_mainsensor() and raum.regelung.regelung == "ruecklaufregelung":
        regparams = raum.regelung.get_parameters()
        if len(regparams.get('rlsensorssn', dict())):
            return True

    return False


def get_local_settings_page_haus(request, haus):

    if request.method == "GET":

        eundr = []
        for etage in haus.etagen.all():
            e = {etage: []}
            for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                e[etage].append((raum, raum.get_atkorrektur_params()))
            eundr.append(e)

        params = haus.get_module_parameters()

        reg_hparams = params.get('differenzregelung', {})
        regs = Regelung.objects.filter_for_user(request.user).filter(regelung='differenzregelung')
        diff_params = []
        for d_reg in regs:
            dreg_id = d_reg.get_parameters().values()[0]
            atk_params = d_reg.get_atkorrektur_params(haus)
            diff_params.append((d_reg.id, dreg_id, reg_hparams[dreg_id], atk_params))

        reg_hparams = params.get('vorlauftemperaturregelung', {})
        regs = Regelung.objects.filter_for_user(request.user).filter(regelung='vorlauftemperaturregelung')
        vor_params = []
        for d_reg in regs:
            mid = d_reg.get_parameters().values()[0]
            atk_params = d_reg.get_atkorrektur_params(haus)
            vor_params.append((d_reg.id, mid, reg_hparams[mid], atk_params))

        diff_params.sort(key=lambda tup: tup[1])
        vor_params.sort(key=lambda tup: tup[1])
        return render_response(request, "m_aussentemperaturkorrektur.html", {"haus": haus, "eundr": eundr, 'diff_params': diff_params,
                                                                             'vor_params': vor_params})

    elif request.method == "POST":

        if request.POST.get('singleroom', False):
            name = request.POST['name']
            obj_id = long(request.POST.get('id', 0))
            if name.startswith("reg"):
                object = "regelung"
                name = name.replace("reg-", '')
            else:
                object = "raum"

            start = -1
            end = -1
            end1 = -99
            end2 = -99
            if name == "time-start":
                start = request.POST['number']
            if name == "time-end":
                end = request.POST['number']
            if name == "heating-element-end_1":
                end1 = request.POST['number']
            if name == "heating-element-end_2":
                end2 = request.POST['number']

            if object == "regelung":
                params = {"start_time": start, 'end_time': end,
                          'heating_element_end_1': end1, 'heating_element_end_2': end2}
                reg_obj = Regelung.objects.get(id=obj_id)
                set_params_regelung(haus, reg_obj, params)
            else:
                raum = Raum.objects.get(id=obj_id)
                set_params(raum, start, end, end1, end2)
            return HttpResponse()

        else:
            start = request.POST.get("time-start", -1)
            end = request.POST.get("time-end", -1)
            if "mitt-zeit-active-atk" not in request.POST:
                start = end = -1
            end1 = request.POST.get("heating-element-end_1", -99)
            if "heating_element_end_1_checkbox" not in request.POST:
                end1 = -99
            end2 = request.POST.get("heating-element-end_2", -99)
            if "heating_element_end_2_checkbox" not in request.POST:
                end2 = -99

            raeume = request.POST.getlist('rooms')
            for raum_id in raeume:
                raum = Raum.objects.get(id=long(raum_id))
                set_params(raum, start, end, end1, end2)
            # save diff / vor params

            params = {
                'start_time': start,
                'end_time': min(72, end),
                'heating_element_end_1': end1,
                'heating_element_end_2': end2
            }
            regs = request.POST.getlist('regs')
            for reg_id in regs:
                reg_obj = Regelung.objects.get(id=reg_id)
                set_params_regelung(haus, reg_obj, params)

            return render_redirect(request, "/m_aussentemperaturkorrektur/%s/" % haus.id)


def get_local_settings_link(request, raum):
    #if has_ms_and_rls(raum):
    haus = raum.etage.haus
    offset = get_offset(haus, raum)

    if offset != 0.0:
        return u"Außentemperaturkorrektur", "aussentemperaturkorrektur", "%.2f" % offset, 75, "/m_raum/%s/aussentemperaturkorrektur/" % raum.id
    else:
        return u"Außentemperaturkorrektur", "aussentemperaturkorrektur" ,"deaktiviert", 75, "/m_raum/%s/aussentemperaturkorrektur/" % raum.id


def get_offset_regelung(haus, modulename, objid):
    param = get_regelung_entity(haus, objid, modulename)
    if 'module_params' not in param or 'aussentemperaturkorrektur' not in param['module_params']:
        return 0.0
    param = param['module_params']['aussentemperaturkorrektur']
    forecast = get_10dayforecast(haus, 72)  # maximale laenge aus atkparams
    if forecast is None:
        return 0.0

    offset = ch.get_module_offset_regelung('aussentemperaturkorrektur', haus.id, modulename, objid)

    if offset is not None:
        return offset

    offset = _calculate_offset(haus, forecast, param)
    ch.set_module_offset_regelung(
        "aussentemperaturkorrektur", haus.id, modulename, objid, offset, ttl=(3600 - datetime.now().minute * 60)
    )
    return offset


def get_local_settings_page_regelung(request, haus, modulename, objid, params):
    if request.method == "GET":
        hparams = haus.get_module_parameters()
        r_param = hparams.get(modulename, dict()).get(objid, dict())
        page_param = {}
        berlin = pytz.timezone("Europe/Berlin")
        now = datetime.now(berlin)
        last_check = now
        last_check = last_check.replace(minute=0)
        last_check = last_check.strftime("%d.%m.%Y %H:%M")
        next_check = now + timedelta(hours=1)
        next_check = next_check.replace(minute=0)
        next_check = next_check.strftime("%d.%m.%Y %H:%M")

        params = {'start_time': params.get('start_time', 0), 'end_time': min(72, params.get('end_time', 5)),
                  'heating_element_end_1': params.get('heating_element_end_1', 0),
                  'heating_element_end_2': params.get('heating_element_end_2', 0)}

        rforecast, avg = get_forecast_average(haus, params['start_time'], params['end_time'])

        page_param['heating_element_temp'] = get_offset_regelung(haus, modulename, objid)
        page_param['next_check'] = next_check
        page_param['last_check'] = last_check
        page_param['forecast'] = rforecast
        page_param['average'] = avg
        page_param['module_params'] = params
        page_param['reg_param'] = r_param
        page_param['objid'] = objid
        page_param['regelung'] = modulename
        page_param['haus'] = haus
        return render_response(request, "m_regelung_aussentemperaturkorrektur.html", page_param)

    if request.method == "POST":
        heating_element_end_1 = float(request.POST['heating-element-end_1'])
        heating_element_end_2 = float(request.POST['heating-element-end_2'])
        start_time = float(request.POST['time-start'])
        end_time = float(request.POST['time-end'])
        params = {"start_time": start_time, "end_time": min(72, end_time),
                  "heating_element_end_1": heating_element_end_1, "heating_element_end_2": heating_element_end_2}
        set_regelung_entity(haus, modulename, objid, params, 'aussentemperaturkorrektur')
        return render_redirect(request, '/m_%s/%s/show/%s/aussentemperaturkorrektur' % (modulename, haus.id, objid))


def get_local_settings_link_regelung(haus, modulename, objid):
    offset = get_offset_regelung(haus, modulename, objid)
    return get_name(), 'aussentemperaturkorrektur', offset, 75, '/m_%s/%s/show/%s/aussentemperaturkorrektur/' % (modulename, haus.id, objid)


def get_local_settings_page(request, raum):

    haus = raum.etage.haus

    if request.method == "GET":

        # TODO time*start, time*end, heating*element*end*1, heating*element*end*2 in sprechende namen umbenennen!

        atkparams = raum.get_atkorrektur_params()

        start_time = atkparams.get('start_time', 0)
        end_time = atkparams.get('end_time', 5)
        heating_element_end_1 = atkparams.get('heating_element_end_1', 0)
        heating_element_end_2 = atkparams.get('heating_element_end_2', 0)

        offset = get_offset(haus, raum)

        berlin = pytz.timezone("Europe/Berlin")
        now = datetime.now(berlin)
        last_check = now
        last_check = last_check.replace(minute=0)
        last_check = last_check.strftime("%d.%m.%Y %H:%M")
        next_check = now + timedelta(hours=1)
        next_check = next_check.replace(minute=0)
        next_check = next_check.strftime("%d.%m.%Y %H:%M")

        rforecast, avg = get_forecast_average(haus, atkparams.get('start_time', 0), atkparams.get('end_time', 5))

        page_param = {"next_check": next_check, "last_check": last_check, "start_time": start_time,
                      "end_time": min(72, end_time), "heating_element_end_2": heating_element_end_2,
                      "heating_element_end_1": heating_element_end_1, 'raum': raum, 'haus': haus, "forecast": rforecast,
                      "average": avg, "heating_element_temp": offset}

        return render_response(request, "m_raum_aussentemperaturkorrektur.html", page_param)

    if request.method == "POST":

        if request.POST.get('singleroom', False):
            name = request.POST['name']
            obj_id = long(request.POST.get('id', 0))

            start = -1
            end = -1
            end1 = -99
            end2 = -99
            if name == "time-start":
                start = request.POST['number']
            if name == "time-end":
                end = request.POST['number']
            if name == "heating-element-end_1":
                end1 = request.POST['number']
            if name == "heating-element-end_2":
                end2 = request.POST['number']

            raum = Raum.objects.get(id=obj_id)
            set_params(raum, start, end, end1, end2)
            haus = raum.etage.haus

            # offset neu berechnen
            ch.delete('%s_offset_%s_%s' % ("aussentemperaturkorrektur", haus.id, raum.id))
            get_offset(haus, raum)

        return HttpResponse()


def activate(hausoderraum):
    if 'aussentemperaturkorrektur' not in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['aussentemperaturkorrektur'])


def set_params(raum, start, end, end1, end2):
    atkparams = raum.get_atkorrektur_params()
    try:
        if start > -1:
            atkparams['start_time'] = int(start)
        if end > -1:
            atkparams['end_time'] = int(end)
        if end1 > -99:
            atkparams['heating_element_end_1'] = float(end1)
        if end2 > -99:
            atkparams['heating_element_end_2'] = float(end2)
    except (ValueError, TypeError):
        pass
    else:
        params = raum.get_module_parameters()
        params['aussentemperaturkorrektur'] = atkparams
        raum.set_module_parameters(params)


def set_params_regelung(haus, reg_obj, params):
    objid = reg_obj.get_parameters().values()[0]
    reg_params = reg_obj.get_atkorrektur_params(haus)
    if params['start_time'] > -1:
        reg_params['start_time'] = int(params['start_time'])

    if params['end_time'] > -1:
        reg_params['end_time'] = int(params['end_time'])

    if params['heating_element_end_1'] > -99:
        reg_params['heating_element_end_1'] = float(params['heating_element_end_1'])

    if params['heating_element_end_2'] > -99:
        reg_params['heating_element_end_2'] = float(params['heating_element_end_2'])

    set_regelung_entity(haus, reg_obj.regelung, objid, reg_params, 'aussentemperaturkorrektur')


def utc_to_local(utc_dt):
    berlin = pytz.timezone('Europe/Berlin')
    local_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(berlin)
    return berlin.normalize(local_dt)  # normalize might be unnecessary


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    context = dict()
    params = haus.get_module_parameters()

    if request.method == "GET":
        context['haus'] = haus
        context['r_m_active'] = params.get('aussentemperaturkorrektur_right_menu', True)
        return render_response(request, "m_settings_aussentemperaturkorrektur.html", context)

    if request.method == "POST":
        if 'right_menu_ajax' in request.POST:
            params['aussentemperaturkorrektur_right_menu'] = bool(int(request.POST['right_menu_ajax']))
            haus.set_module_parameters(params)
            return JsonResponse({'message': 'done'})

        return render_redirect(request, '/m_setup/%s/aussentemperaturkorrektur/' % haus.id)
