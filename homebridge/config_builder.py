"""
Script file: config_builder.py
Created on: Apr 23, 2019
Last modified on: Apr 19, 2021

Comments:
    Python script to fetch the data from API
    Sample url: https://test.fwd.controme.com/get/json/v1/1/rooms/
"""

import os
import json
import requests

import io
import sys
import time
import sqlite3
import subprocess
import logging


def get_mac(interface='eth0'):
    """
    Return the MAC address of the specified interface
    :param interface: Ethernet or Wi-Fi (eth0, wlan0)
    :return: mac address
    """
    try:
        mac_address = open('/sys/class/net/%s/address' % interface).read()
    except IOError:
        mac_address = "00:00:00:00:00:00"
    return mac_address[0:17].upper()


def generate_pincode(string):
    """
    This function retrieves numbers from a string
    and generate the pin code based on it
    :rule: [3 digits chosen from right to left]-11-111
    :param string: mac address
    :return: pin code
    """
    digits = ''
    for i in string:
        if i in '1234567890':
            digits += i
    # generate the pin code
    if digits == '':
        digits = '0'
    pin_code = '%03d-11-111' % int(digits[-3:])
    return pin_code


def read_config(out_path):
    with io.open(out_path, 'r', encoding="utf8") as cfile:
        text = cfile.read()
        return json.loads(text)


def write_config(data, out_path):
    """
    Save json body into .json file
    :param data: json body, unicode enabled
    :param out_path: output file path
    """
    subprocess.Popen("touch %s" % out_path, shell=True)
    with io.open(out_path, 'w', encoding="utf8") as outfile:
        json.dump(data, outfile, indent=4, sort_keys=False, ensure_ascii=False)


def build_config(api_url, file_path, server, home_id):
    """
    The room names can be fetched via following GET request
    Build config.json file and save it to the specified path
    :param api_url: api route url
    :param file_path: path of homebridge config.json file
    :param server: server address
    :home_id: home id
    :return: none
    """
    accessories = []
    response = requests.get(api_url).json()
    mac_address = get_mac()
    serial = mac_address.replace(':', '-')

    for body in response:
        floor_name = body["etagenname"]
        rooms = body["raeume"]
        for room_info in rooms:
            accessory = {
                "accessory": "ContromeThermostat",
                "id": room_info["id"],
                "name": "%s (%s)" % (room_info["name"], floor_name),
                "homeId": home_id,
                "server": server,
                "serial": serial.lower(),
            }
            accessories.append(accessory)

    # to make a bridge unique
    name = "Controme Miniserver {}".format(serial)
    bridge = {
        "name": name,
        "username": mac_address,
        "port": 51826,
        "pin": generate_pincode(mac_address)
    }
    description = "Controme Miniserver Homebridge"
    platforms = []

    # save config information
    saved_json = {
        "bridge": bridge,
        "description": description,
        "accessories": accessories,
        "platforms": platforms
    }

    try:
        old_conf = read_config(file_path)
        if old_conf != saved_json:
            write_config(saved_json, file_path)
            subprocess.Popen("sudo systemctl restart homebridge.service", shell=True)
    except Exception:
        logging.exception("error writing config")
        write_config(saved_json, file_path)
        try:
            subprocess.Popen("sudo systemctl restart homebridge.service", shell=True)
        except Exception:
            logging.exception("error restarting homebridge service")


def main():
    server = 'http://127.0.0.1'
    file_path = '/home/pi/.homebridge/config.json'

    # get information from SQLiteDB
    conn = sqlite3.connect("/home/pi/rpi/db.sqlite3")
    cursor = conn.cursor()

    # get home id
    cursor.execute("select id from heizmanager_haus")
    ids = cursor.fetchone()
    if ids is None:
        return
    home_id = ids[0]

    # get homebridge module
    cursor.execute("select modules from heizmanager_hausprofil")
    modules = cursor.fetchone()

    # get email, password
    # not required inside a local raspberry pi server

    # check homebridge service status
    process = subprocess.Popen("sudo systemctl status homebridge", stdout=subprocess.PIPE, shell=True)
    sysctlout = process.communicate()
    if 'homebridge' in modules[0]:
        api_url = '%s/get/json/v1/%s/rooms/' % (server, home_id)
        try:
            build_config(api_url, file_path, server, home_id)
        except Exception as e:
            # upon first start, ~/.homebridge doesnt exist, need to start homebridge first:
            logging.exception("exc")

        subprocess.Popen("touch /home/pi/rpi/homebridge/run.pid", shell=True)
        if b'Active: active (running)' not in sysctlout[0]:
            subprocess.Popen("sudo systemctl start homebridge", shell=True)
            subprocess.Popen("sudo systemctl start avahi-daemon.service", shell=True)
            subprocess.Popen("sudo systemctl start avahi-daemon.socket", shell=True)
    else:
        if b'Active: active (running)' in sysctlout[0] or b'Active: inactive (dead)' in sysctlout[0]:
            subprocess.Popen("rm /home/pi/rpi/homebridge/run.pid", shell=True)
            subprocess.Popen("sudo systemctl stop homebridge", shell=True)
            subprocess.Popen("sudo systemctl stop avahi-daemon.service", shell=True)
            subprocess.Popen("sudo systemctl stop avahi-daemon.socket", shell=True)


if __name__ == "__main__":
    while True:
        main()
        time.sleep(30)