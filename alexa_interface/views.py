
# -*- coding: utf-8 -*-

import logging
from heizmanager.render import render_response, render_redirect
from heizmanager.models import Raum, Haus, Regelung
import heizmanager.cache_helper as ch
import pytz
from datetime import timedelta, datetime
from django.http import HttpResponse
from heizmanager import network_helper
from fabric.api import local
import json


def get_name():
    return u'Amazon Alexa'


def is_togglable():
    return True


def get_cache_ttl():
    return 3600


def calculate_always_anew():
    return False


def is_hidden_offset():
    return False


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/alexa_interface/'>Amazon Alexa</a>" % str(haus.id)


def get_global_description_link():
    desc = u"Bedienung über Sprache und die Alexa App."
    desc_link = "https://support.controme.com/amazon-alexa-integration/"
    return desc, desc_link


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        try:
            haus = Haus.objects.get(pk=long(haus))
        except Haus.DoesNotExist:
            return HttpResponse("Error", status=400)

    if request.method == "GET":
        params = haus.get_module_parameters()
        if action == 'changeid':
            alexa_id = params.get('alexa_interface', {}).get('alexa_id')
            # default for alexa id is the mac-address
            if alexa_id is None:
                from heizmanager.network_helper import get_mac
                alexa_id = get_mac()
            context = dict()
            context['alexa_id'] = alexa_id
            context['haus'] = haus
            return render_response(request, "m_settings_alexa_interface_changeid.html", context)

        is_active = params.get('alexa_interface', {}).get('is_active', False)
        context = {'haus': haus, 'is_active': is_active}

        return render_response(request, "m_settings_alexa_interface.html", context)

    if request.method == 'POST':

        if action == 'changeid':
            params = haus.get_module_parameters()
            alexa_id = request.POST.get('alexa_id')
            alexa_params = params.get('alexa_interface', {})
            alexa_params['alexa_id'] = alexa_id
            params['alexa_interface'] = alexa_params
            haus.set_module_parameters(params)
            return render_redirect(request, "/m_setup/%s/alexa_interface/changeid/?success=true" % haus.id)

        return render_redirect(request, '/config/')


def get_global_settings_page_help(request, haus):
    pass


def get_local_settings_page_haus(request, haus):
    pass


def get_local_settings_link(request, raum):
    pass


def get_local_settings_page(request, raum):
    pass


def get_offset(haus, raum=None):
    offset = 0
    return offset


def deactivate(hausoderraum):
    mods = hausoderraum.get_modules()
    try:
        mods.remove('alexa_interface')
        hausoderraum.set_modules(mods)
    except ValueError:  # nicht in modules
        pass


def activate(hausoderraum):
    if not 'alexa_interface' in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['alexa_interface'])


def start_service(haus):
    params = haus.get_module_parameters()
    params.setdefault('alexa_interface', dict())
    params['alexa_interface']['is_active'] = True
    haus.set_module_parameters(params)

    try:
        local("touch /home/pi/rpi/alexa_interface/run.pid")
    except:
        pass
    
    try:
        local("sudo systemctl start alexa_interface.service")
    except:
        pass


def stop_service(haus):
    params = haus.get_module_parameters()
    params.setdefault('alexa_interface', dict())
    params['alexa_interface']['is_active'] = False
    haus.set_module_parameters(params)

    try:
        local("rm /home/pi/rpi/alexa_interface/run.pid")
    except:
        pass
    
    try:
        local("sudo systemctl stop alexa_interface.service")
    except:
        pass


def get_jsonapi(haus, usr, entityid=None):
    params = haus.get_module_parameters()
    is_active = params.get('alexa_interface', {}).get('is_active', False)
    ret = {'is_active': is_active}
    return ret
