from django.conf.urls import url
from . import views
urlpatterns = [ 
    url(r'^set/zwave/(?P<rpimac>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/(?P<controllerid>([0-9A-Za-z_]*))/$', views.set_zwave),
    url(r'^get/zwave/(?P<controllerid>([0-9A-Za-z_]*))/$', views.get_zwave),
    url(r'^set/btle/(?P<rpimac>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/(?P<controllerid>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/$', views.set_btle),
    url(r'^get/btle/(?P<controllerid>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/$', views.get_btle),
    url(r'^set/enocean/(?P<rpimac>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/(?P<controllerid>([0-9A-Fa-f]{2}[:]){3}([0-9A-Fa-f]{2}))/$', views.set_enocean),
    url(r'^get/enocean/(?P<controllerid>([0-9A-Fa-f]{2}[:]){3}([0-9A-Fa-f]{2}))/$', views.get_enocean),
    url(r'^set/rc/(?P<rcid>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/$', views.set_rc),
    url(r'^get/rc/(?P<rcid>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/$', views.get_rc),
    ]
