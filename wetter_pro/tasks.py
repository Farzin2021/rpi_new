import os
from datetime import datetime, timedelta
import pytz
from heizmanager.models import Haus, sig_new_temp_value
from celery import shared_task
import requests
import heizmanager.cache_helper as ch
from views import get_list_of_assign_values
import hashlib
import base64
import urllib


def get_forecast(haus):
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)
    nowUTC = now.utcnow()
    dateApiFormat = nowUTC.strftime('%Y-%m-%d-%H')
    # API Version number
    apiVersion = 1
    # basic path for geo-API request 
    # https://api.wetteronline.de/geo?lat=<latitude>&lon=<longitude>&v=<versionnumber>
    basepath_geo = "https://api.wetteronline.de/geo?"
    # basic path for Weather-API request 
    basepath_weather = "https://api.wetteronline.de/weather?"
    # generate from a API url with latitude and longitude
    geoId = None
    # individual UserID
    uid = "Y29udHJvbWU="
    # individual secret key for the account of "UserID"
    secret = "Ym11YmxwdnJtcnJmeGxr"
    # required package (available current, hourly72, day6parts, daily8)
    package = "hourly72"

    params = haus.get_module_parameters()
    lat, lng, forecast = None, None, None
    is_active = params.get("wetter_pro", {}).get('is_active', False)
    if not is_active: # module is deactive
        return forecast

    if not lat:
        lat = params.get('wetter', {}).get('lat', None)
        lng = params.get('wetter', {}).get('lng', None)

    geoId = ch.get('weatherpro_geoId_%s_%s_%s' % (haus.id, lat, lng))
    if not geoId:
        forecast = requests.get(basepath_geo+"lat=%s&lon=%s&v=%s" % (lat, lng, apiVersion), timeout=20).json()
        geoId = forecast[0]['gid']
        ch.set('weatherpro_geoId_%s_%s_%s' % (haus.id, lat, lng), geoId)

    if geoId:
        data_array = [dateApiFormat, geoId, package, uid, str(apiVersion), secret]
        checksum = hashlib.md5('|'.join(data_array)).digest().encode('base64').strip()
        query_dict = {'package':package, 'gid':geoId, 'v': str(apiVersion), 'uid':uid, 'date': dateApiFormat, 'checksum': checksum}
        # print basepath_weather+"%s" % urllib.urlencode(query_dict)
        forecast = requests.get(basepath_weather+"%s" % urllib.urlencode(query_dict), timeout=20).json()
        forecast_local_time = datetime.strptime(forecast[geoId]['meta']['local_date'], '%Y-%m-%d %H:%M:%S')
        current_hour = int(forecast_local_time.strftime('%H'))
        forecast_today = []
        for d in forecast[geoId]['data']:
            f_date = d['date']
            if f_date == forecast_local_time.strftime('%Y-%m-%d'):
                hours = d['periods'][current_hour:]
            else:
                hours = d['periods']
            for v in hours:
                if not 'date' in v:
                    v.update(date = f_date )
            forecast_today.extend(hours)
        forecast = forecast_today
        
    return forecast


# This task MUST execute every 60 minutes
@shared_task
def periodic_calculation(request=None):
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)

    for haus in Haus.objects.all():
        if 'wetter_pro' not in haus.get_modules():
            continue

        params = haus.get_module_parameters()
        is_active = params.get("wetter_pro", {}).get('is_active', False)
        if not is_active: # module is deactive
            continue

        # forecasts list
        f_list = ch.get('weatherpro_updating_forecast_%s' % haus.id)

        # check if cache has old version of data
        if f_list and any('FCTTIME' in v for v in enumerate(f_list)):
            f_list = None
            ch.set('weatherpro_assign_values_%s' % haus.id, None, time=3601)
            ch.set('weatherpro_updating_forecast_%s' % haus.id, None, time=36000)

        if f_list and len(f_list) > 24 and f_list[24]['date'] + f_list[24]['periodname'] < now.strftime("%Y-%m-%d%H:00"):
            del f_list[0:1]
            ch.set('weatherpro_updating_forecast_%s' % haus.id, f_list, time=36000)
            ch.set('weatherpro_lasttime_list_updated_%s' % haus.id, now, time=36000)

        # len(f_list) < 50 >>>> -24h + current time + +24h === 49h
        # last condition: for any reason (like deactivate and activate after 2h) if 24th of f_list that should be current time, is not equal with current time
        if not f_list or len(f_list) < 50 or (f_list[24]['date'] + f_list[24]['periodname'] < now.strftime("%Y-%m-%d%H:00")):
            f_list_api = get_forecast(haus)
            if not f_list or len(f_list) < 24:
                f_list = [None] * 55
            f_list[24:] = f_list_api[:31] # the next 24h + extra 6h
            ch.set('weatherpro_updating_forecast_%s' % haus.id, f_list, time=36000)
            ch.set('weatherpro_lasttime_list_updated_%s' % haus.id, now, time=36000)

        # calculate every 60 minutes
        wetters_assign_values = get_list_of_assign_values(haus)
        ch.set('weatherpro_assign_values_%s' % haus.id, wetters_assign_values, time=3601)

        # Log the actual degree of coverage
        percent = '0'
        coverage = f_list[24]['wm'][0:2]
        if coverage == 'bd' or coverage == 'md':  # Overcast sky
            percent = '100'
        elif coverage == 'bw' or coverage == 'mw':  # Cloudy
            percent = '75'
        elif coverage == 'wb' or coverage == 'mb':  # Various clouds
            percent = '50'
        elif coverage == 'ms' or coverage == 'mm':  # Mostly sunny
            percent = '25'
        sig_new_temp_value.send(sender="wetter_pro", name='coverage_degree', value=int(percent), modules=haus.get_modules(), timestamp=now)
