try:
    import telepot
except ImportError:
    from fabric.api import local
    local("sudo pip install telepot==12.1")
    local("sudo pip install apscheduler==3.3.1")
    local("sudo cp /home/pi/rpi/config/supervisor-messaging.conf /etc/supervisor/conf.d/messaging.conf")
    local("sudo supervisorctl -c /etc/supervisor/supervisord.conf reload")
finally:
    import telepot
import sys

bot = telepot.Bot(sys.argv[1])
bot.getMe()

try:
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
except ImportError:
    from fabric.api import local
    local("sudo pip install matplotlib==2.0.2")