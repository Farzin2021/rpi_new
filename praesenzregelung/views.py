# -*- coding: utf-8 -*-

import heizmanager.cache_helper as ch
import logging
from heizmanager.render import render_response, render_redirect
from rf.models import RFSensor
from itertools import chain


def get_name():
    return u"Präsenzregelung"


def is_togglable():
    return True


def is_hidden_offset():
    return False


def get_cache_ttl():
    return 1800


def calculate_always_anew():
    return True


def get_offset(haus, raum=None):

    try:
        if 'praesenzregelung' not in raum.get_modules():
            ret = 0.0

        else:

            for sensor in list(chain(RFSensor.objects.filter(raum=raum, type="multisensor"), RFSensor.objects.filter(raum=raum, type__startswith="pir"))):
                last = sensor.get_wert(val='Alarm')

                logging.warning("pr last: %s" % str(last))

                if last is None or not last[0]:  # entweder kein Wert oder kein 'Alarm'
                    continue

                else:
                    if last[0]:
                        ret = 0.0
                        break

            else:
                params = raum.get_module_parameters()
                ret = params.get('praesenzregelung', dict()).get('offset', -0.1)

    except (AttributeError, TypeError):
        ret = 0.0

    prev = ch.get_module_offset_raum('praesenzregelung', haus.id, raum.id)

    if prev is not None:
        if prev == 0.0 and ret == 0.0:
            duration = 1800  # um 30min verlaengern
        elif prev == 0.0 and ret != 0.0:
            return prev  # vorherige 30min Spanne weiterverwenden
        elif prev != 0.0 and ret == 0.0:
            duration = 1800  # neue 30min Spanne starten
        else:
            duration = 60  # absenken

    else:
        if ret == 0.0:
            duration = 1800
        else:
            duration = 60

    ch.set_module_offset_raum('praesenzregelung', haus.id, raum.id, ret, ttl=duration)

    return ret


def get_outputs():
    # kann in Zukunft auch Ausgaenge schalten (z.B. InfrarotHK an/aus ueber Wallplug)
    pass


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/"+str(haus.id)+"/praesenzregelung/'>Präsenzregelung</a>"


def get_global_description_link():
    desc = u"Anwesenheitserkennung mit Präsenzmelder nutzen, um Heizkosten zu sparen."
    desc_link = "http://support.controme.com/auswahl-und-aktivierung-von-modulen/modul-praesenzregelung/"
    return desc, desc_link


def get_global_settings_page(request, haus):

    if request.method == "GET":

        ret = []
        raeume = set()
        sensoren = RFSensor.objects.filter(haus=haus, type__in=['multisensor', 'pira50703'])
        for sensor in sensoren:

            if not sensor.raum_id or sensor.raum_id in raeume:
                continue
            raeume.add(sensor.raum_id)

            params = sensor.raum.get_module_parameters().get('praesenzregelung', dict())
            offset = params.get('offset', -0.1)
            active = True if 'praesenzregelung' in sensor.raum.get_modules() else False
            ret.append((sensor, active, offset))

        return render_response(request, "m_settings_praesenzregelung.html", {'haus': haus, 'sensoren': ret})

    elif request.method == "POST":
        logging.warning(request.POST)
        sensoren = RFSensor.objects.filter(haus=haus, type__in=['multisensor', 'pira50703'])
        for sensor in sensoren:

            # weil mehrere Sensoren in einem Raum sein koennen und wir nur einen zeigen
            if str(sensor.id) not in request.POST:
                continue

            if "%sprraktiv" % sensor.id in request.POST:
                offset = float(request.POST.get('%s_offset' % sensor.id, -0.1))

                params = sensor.raum.get_module_parameters()
                params.setdefault('praesenzregelung', dict())
                params['praesenzregelung']['offset'] = offset
                sensor.raum.set_module_parameters(params)

                activate(sensor.raum)

            else:
                deactivate(sensor.raum)

        return render_redirect(request, "/m_setup/%s/praesenzregelung/" % haus.id)


def get_global_settings_page_help(request, haus):
    return render_response(request, "m_help_praesenzregelung.html", {'haus': haus})


def get_local_settings_link(request, raum):
    sensoren = RFSensor.objects.filter(haus=raum.etage.haus, type__in=['multisensor', 'pira50703'])
    if not len(sensoren):
        logging.warning("pr: kein multisensor")
        return ""

    if 'praesenzregelung' not in raum.get_modules():
        return "Pr&auml;senzregelung", "deaktiviert", 75, "/m_raum/%s/praesenzregelung/" % raum.id

    offset = ch.get_module_offset_raum('praesenzregelung', raum.etage.haus_id, raum.id)
    logging.warning("pr cache: %s" % offset)
    if offset is None:
        offset = get_offset(raum.etage.haus, raum)
        logging.warning("pr neu: %s" % offset)
    return "Pr&auml;senzregelung", "%.2f" % offset, 75, "/m_raum/%s/praesenzregelung/" % raum.id


def get_local_settings_page(request, raum):

    if request.method == "GET":
        active = False
        if 'praesenzregelung' in raum.get_modules():
            active = True
        params = raum.get_module_parameters()
        offset = params.get('praesenzregelung', dict()).get('offset', -0.1)
        return render_response(request, "m_raum_praesenzregelung.html",
                               {'raum': raum, 'active': active, 'offset': offset})

    elif request.method == "POST":

        if 'prraktiv' in request.POST:
            offset = float(request.POST.get('offset', -0.1))

            params = raum.get_module_parameters()
            params.setdefault('praesenzregelung', dict())
            params['praesenzregelung']['offset'] = offset
            raum.set_module_parameters(params)

            activate(raum)

        else:
            deactivate(raum)

        return render_redirect(request, "/m_raum/%s/" % raum.id)


def deactivate(hausoderraum):
    mods = hausoderraum.get_modules()
    try:
        mods.remove('praesenzregelung')
        hausoderraum.set_modules(mods)
    except ValueError:  # nicht in modules
        pass


def activate(hausoderraum):
    if 'praesenzregelung' not in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['praesenzregelung'])