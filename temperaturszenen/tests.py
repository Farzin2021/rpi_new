from heizmanager.tests import BaseModeTest
from .views import TemperaturszenenMode
import heizmanager.helpers as helpers


class TemperaturszenenTest(BaseModeTest):

    def setUp(self):
        super(TemperaturszenenTest, self).setUp('temperaturszenen/fixtures/fixture.json')

    def test_SetModeFunctionality(self):
        mode = TemperaturszenenMode(mode_id=2)
        raum = self.get_raum_by_id(3)
        ret = mode.set_mode_raum(raum)
        self.assertTrue(ret)
        self.assert_temperatureszene_is_activated_in_room(raum.id, 2)

    def test_GetModeFunctionality(self):
        mode = TemperaturszenenMode(mode_id=2)
        raum = self.get_raum_by_id(3)
        mode.set_mode_raum(raum)
        ret = mode.get_mode_raum(raum)
        name = helpers.get_temperatureszene_mode_name(raum.etage.haus, 2)
        temp = helpers.get_temperatureszene_temperature(raum, 2)
        self.assertIsInstance(ret, dict)
        self.assertIn('soll', ret)
        self.assertEqual(ret['soll'], temp)
        self.assertEqual(ret['activated_mode_text'], name)
        self.assertEqual(ret['activated_mode_id'], 2)
