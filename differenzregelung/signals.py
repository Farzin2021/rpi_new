from heizmanager.models import sig_get_outputs_from_regelung, sig_get_possible_outputs_from_regelung, sig_create_regelung, sig_create_output_regelung, Raum
from django.dispatch import receiver
import differenzregelung.views as dr
import heizmanager.cache_helper as ch
import logging


@receiver(sig_get_outputs_from_regelung)
def dr_get_outputs(sender, **kwargs):
    haus = kwargs['haus']
    raum = kwargs['raum']
    regelung = kwargs['regelung']
    return dr.get_outputs(haus, raum, regelung)


@receiver(sig_get_possible_outputs_from_regelung)
def dr_get_possible_outputs(sender, **kwargs):
    haus = kwargs['haus']
    return dr.get_possible_outputs(haus)


@receiver(sig_create_regelung)
def dr_create_regelung(sender, **kwargs):
    # called when a sensor is added or deleted and requires changes to a regelung
    # currently not necessary here
    pass


@receiver(sig_create_output_regelung)
def dr_create_output(sender, **kwargs):
    # currently not necessary
    pass

