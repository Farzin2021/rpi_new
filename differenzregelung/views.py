# -*- coding: utf-8 -*-

from heizmanager.models import Haus, Sensor, Gateway, GatewayAusgang, AbstractSensor, Regelung
from rf.models import RFSensor, RFAktor
from heizmanager.render import render_response, render_redirect
import logging
from django.http import HttpResponse
import json
import pytz
from datetime import datetime, timedelta
import heizmanager.cache_helper as ch
from heizmanager.models import sig_new_output_value, sig_new_temp_value
from django.views.decorators.csrf import csrf_exempt
from heizmanager.mobile.m_temp import get_module_offsets_regelung, get_module_line_list_regelung
from benutzerverwaltung.decorators import has_regelung_access
import operator
from heizmanager.models import sig_get_outputs_from_output, sig_get_possible_outputs_from_output, sig_get_ctrl_devices
from itertools import chain


logger = logging.getLogger('logmonitor')


def get_name():
    return u"Differenzregelung"


def managesoutputs():
    return False


def do_ausgang(regelung, ausgangno):

    hparams = Haus.objects.all()[0].get_module_parameters()
    regparams = regelung.get_parameters()
    params = hparams.get('differenzregelung', dict()).get(regparams['dreg'])
    if params is None:
        logging.warning("dr: params is None")
        return HttpResponse("<>")
    status = {'control': regparams['dreg'], 'description': params['name']}

    s1s = params['s1']
    s1 = AbstractSensor.get_sensor(s1s)
    if s1 is not None:
        s1 = s1.get_wert()
        if s1 is not None:
            s1 = s1[0]
            sig_new_temp_value.send(sender="differenzregelung",
                                    name="dreg_%s_s1" % regparams['dreg'],
                                    value=s1,
                                    modules=[],
                                    timestamp=datetime.utcnow())

    s2 = params['s2']
    s2 = AbstractSensor.get_sensor(s2)
    if s2 is not None:
        s2 = s2.get_wert()
        if s2 is not None:
            s2 = s2[0]
            sig_new_temp_value.send(sender="differenzregelung",
                                    name="dreg_%s_s2" % regparams['dreg'],
                                    value=s2,
                                    modules=[],
                                    timestamp=datetime.utcnow())

    s3s = params['s3']
    s3 = AbstractSensor.get_sensor(s3s)
    if s3 is not None:
        s3 = s3.get_wert()
        if s3 is not None:
            s3 = s3[0]
            sig_new_temp_value.send(sender="differenzregelung",
                                    name="dreg_%s_s3" % regparams['dreg'],
                                    value=s3,
                                    modules=[],
                                    timestamp=datetime.utcnow())

    if (s1s != "0" and s1 is None) or (s1 == -51.0 or s1 == 151.0 or (params['s1'] != 'pt1000' and s1 == 85.0)) or s2 is None or (s3s != "0" and s3 is None):
        status['error'] = True
        ret = 0

    else:
        diff_s2 = float(params['diff_s2'])
        min_s1 = float(params['min_s1'])
        max_s1 = float(params.get('max_s1', 90))
        max_s2 = float(params['max_s2']) + get_module_offsets_regelung(Haus.objects.all()[0], regparams['dreg'], 'differenzregelung')
        try:
            max_s3 = float(params['max_s3'])
        except (ValueError, TypeError):
            logging.exception("max_s3 exception, setting to 85")
            max_s3 = 85
        try:
            einvzg = int(params.get('einvzg', 0))
        except (ValueError, TypeError):
            logging.exception("einvzg exception, setting to 0")
            einvzg = 0
        try:
            ausvzg = int(params.get('ausvzg', 0))
        except (ValueError, TypeError):
            logging.exception("ausvzg exception, setting to 0")
            ausvzg = 0
        hysteresis = float(params['hysteresis'])

        ret = s2 < max_s2
        if s1 is not None:
            ret &= (s1 > (s2 + diff_s2)) & (max_s1 > s1 > min_s1)
        if s3 is not None:
            ret &= (s3 < max_s3)
        ret = 100 if ret else 0  # sonst ist die berechnung ein boolean

        invertout = params.get('invertout', False)

        ls = ch.get("%s%sstatus" % ('differenzregelung', regelung.id))
        do_delay = False
        if ls and ls.get('position') is not None:

            last_position = ls['position'] if not invertout else 100 if ls['position'] == 0 else 0

            if s1 is None and s3 is None:
                if ret == 100 and last_position == 0 and s2 < (max_s2 - hysteresis):
                    do_delay = True
                elif ret == 0 and last_position == 100 and s2 > (max_s2 + hysteresis):
                    do_delay = True
                else:
                    ret = last_position
                    status['applied_hysteresis'] = True

            elif s3 is None:
                if max_s1 > s1 > min_s1 and s2 < max_s2:
                    if ret == 100 and last_position == 0 and s1 > (s2 + diff_s2 + hysteresis):
                        do_delay = True
                    elif ret == 0 and last_position == 100 and s1 < (s2 + diff_s2 - hysteresis):
                        do_delay = True
                    else:
                        ret = last_position
                        status['applied_hysteresis'] = True

            else:
                if max_s1 > s1 > min_s1 and s2 < max_s2 and s3 < max_s3:
                    if ret == 100 and last_position == 0 and s1 > (s2 + diff_s2 + hysteresis):
                        do_delay = True
                    elif ret == 0 and last_position == 100 and s1 < (s2 + diff_s2 - hysteresis):
                        do_delay = True
                    else:
                        ret = last_position
                        status['applied_hysteresis'] = True

        else:
            if (s1 is None and s3 is None) or (s3 is None and max_s1 > s1 > min_s1 and s2 < max_s2) or (max_s1 > s1 > min_s1 and s2 < max_s2 and s3 < max_s3):
                # zwar kein vorheriger wert, aber auch keine sicherheitsbedingung aktiv:
                do_delay = True

        if invertout:
            ret = 100 if ret == 0 else 0

        if do_delay and (einvzg or ausvzg):
            last = ch.get("cclast_%s" % regelung.id)
            if ret:
                # ret = 100
                if last:

                    if last[1] > 0:
                        pass

                    else:
                        if datetime.now() > (last[0] + timedelta(seconds=ausvzg)):
                            last = (datetime.now(), ret)
                            ch.set("cclast_%s" % regelung.id, last)

                    if datetime.now() > (last[0] + timedelta(seconds=einvzg)):
                        pass

                    else:
                        ret = 0

                else:
                    ch.set("cclast_%s" % regelung.id, (datetime.now(), ret))
                    ret = 0

            else:
                # ret = 0
                if last:

                    if last[1] == 0:
                        pass

                    else:
                        if datetime.now() > (last[0] + timedelta(seconds=einvzg)):
                            last = (datetime.now(), ret)
                            ch.set("cclast_%s" % regelung.id, last)

                    if datetime.now() > (last[0] + timedelta(seconds=ausvzg)):
                        pass

                    else:
                        ret = 100

                else:
                    ch.set("cclast_%s" % regelung.id, (datetime.now(), 0))
                    ret = 100

    sig_new_output_value.send(sender="differenzregelung",
                              name="dregout_%s" % regparams['dreg'],
                              value=ret,
                              ziel=ret,
                              parameters=str({}),  # todo
                              timestamp=datetime.utcnow(),
                              modules=[])

    status['s1'] = s1
    status['s2'] = s2
    status['s3'] = s3
    status['position'] = ret  # 0-100, not runtime-adjusted!
    ch.set('%s%sstatus' % ('differenzregelung', regelung.id), status)

    logger.warning(
        "%s|%s" % (regparams['dreg'],
                   u"%s%s%s%s" %
                   (
                       (u"S1: %.2f°. " % s1) if s1 is not None else "",
                       (u"S2: %.2f°. " % s2) if s2 is not None else "",
                       (u"S3: %.2f°. " % s3) if s3 is not None else "",
                       (u"Position: %s%%." % ret),
                   )
                   )
    )

    return HttpResponse("<%s>" % min(1, ret))  # cast auf 0/1


def do_rfausgang(regelung, controllerid):
    ret = do_ausgang(regelung, 0).content
    return int(ret[1]) if ret[1] in ["0", "1"] else 0


def get_outputs(haus, raum, regelung):

    belegte_ausgaenge = dict()
    hparams = haus.get_module_parameters()
    diffregs = hparams.get('differenzregelung', dict())

    ret = sig_get_outputs_from_output.send(sender='differenzregelung', raum=raum, regelung=regelung)
    for cbfunc, outputs in ret:
        for gw, raum, regelung, ausgang, namen in outputs:
            if regelung.regelung != "differenzregelung":
                continue
            belegte_ausgaenge.setdefault(gw, list())
            regparams = regelung.get_parameters()
            for name in namen:
                try:
                    n = int(name.strip())
                except:
                    n = name
                drid = regparams.values()[0]
                belegte_ausgaenge[gw].append((n, ausgang, ("diffreg_%s" % drid, u"Differenzregelung %s: %s" % (drid, diffregs[drid]['name'])), True))

    return belegte_ausgaenge


def get_possible_outputs(haus):

    ctrldevices = sig_get_ctrl_devices.send(sender='fps', haus=haus)
    ctrldevices = [d for cbf, devs in ctrldevices for d in devs]
    regler = dict((cd, list()) for cd in ctrldevices)
    params = haus.get_module_parameters()
    for drid, values in params.get('differenzregelung', dict()).items():
        for cd in regler.keys():
            regler[cd].append(["diffreg_%s" % drid, u"Differenzregelung %s: %s" % (drid, values['name'])])

    return regler


def get_config(request, macaddr):
    if request.method == 'GET':
        try:
            gw = Gateway.objects.get(name=macaddr.lower())
        except Gateway.DoesNotExist:
            return HttpResponse(status=405)
        ret = {}
        haus = gw.haus
        params = haus.get_module_parameters()
        for dreg, values in params.get('differenzregelung', dict()).items():
            try:
                ret.setdefault(dreg, dict())

                ret[dreg]['outputs'] = []
                ausgaenge = GatewayAusgang.objects.filter(gateway=gw, regelung__regelung="differenzregelung")
                if not len(ausgaenge):
                    del ret[dreg]
                    continue

                for ausgang in ausgaenge:
                    regparams = ausgang.regelung.get_parameters()
                    if regparams['dreg'] == dreg:
                        ret[dreg]['outputs'] = [int(a.strip()) for a in ausgang.ausgang.split(',')]
                        break
                else:
                    # wenn diese bedingung faellt, dann aufpassen, wenn ein kunde mehr als ein hrgw hat
                    # -> eine dr muss vorerst immer einen ausgang haben
                    del ret[dreg]
                    continue

                ret[dreg]['control'] = 'singlecycledifferentialcontrol'
                ret[dreg].update(values)
                ret[dreg]['hrgw'] = gw.name  # for backwards compatibility with the actual hrgw
                s1 = AbstractSensor.get_sensor(values['s1'])
                if s1 is not None:
                    ret[dreg]['s1'] = 'pt1000' if 'pt1000' in s1.name else s1.name
                    if s1.gateway != gw:
                        val = s1.get_wert() or (None,)
                        ret[dreg]['s1_val'] = val[0]
                else:
                    ret[dreg]['s1'] = None
                s2 = AbstractSensor.get_sensor(values['s2'])  # .name
                ret[dreg]['s2'] = 'pt1000' if 'pt1000' in s2.name else s2.name
                if s2.gateway != gw:
                    val = s2.get_wert() or (None,)
                    ret[dreg]['s2_val'] = val[0]
                s3 = AbstractSensor.get_sensor(values['s3'])
                if s3 is not None:
                    ret[dreg]['s3'] = 'pt1000' if 'pt1000' in s3.name else s3.name
                    if s3.gateway != gw:
                        val = s3.get_wert() or (None,)
                        ret[dreg]['s3_val'] = val[0]
                else:
                    ret[dreg]['s3'] = None
                ret[dreg]['diff_s2'] = values['diff_s2']
                ret[dreg]['min_s1'] = values['min_s1']
                ret[dreg]['max_s1'] = values.get('max_s1', 90)

                regelung_offsets = get_module_offsets_regelung(haus, dreg, 'differenzregelung')
                ret[dreg]['max_s2'] = int(values['max_s2']) + regelung_offsets

                ret[dreg]['max_s3'] = values['max_s3']
                ret[dreg]['min_slider_val'] = values['min_slider_val']
                ret[dreg]['max_slider_val'] = values['max_slider_val']
                ret[dreg]['einvzg'] = values.get('einvzg', 0)
                ret[dreg]['ausvzg'] = values.get('ausvzg', 0)
                ret[dreg]['hysteresis'] = values['hysteresis']
                ret[dreg]['invertout'] = values.get('invertout', False)

            except:
                if dreg in ret:
                    del ret[dreg]

        return HttpResponse(json.dumps(ret))

    else:
        return HttpResponse(405)


@csrf_exempt
def set_status(request, macaddr):

    if request.method == "POST":
        ch.set_gw_ping(macaddr.lower())

        data = json.loads(request.body)
        data['gateway_ip'] = request.META['REMOTE_ADDR']

        _data = ch.get("cc_%s" % macaddr)
        if _data is None or not isinstance(_data, dict):
           _data = data
        else:
            try:
                _data.update(data)
            except AttributeError:
                pass
        ch.set("cc_%s" % macaddr, _data)

        for dregid, values in data.items():
            if type(values) != dict: continue
            elif dregid == "error":
                try:
                    gw = Gateway.objects.get(name=macaddr.lower())
                except Gateway.DoesNotExist:
                    pass
                else:
                    ausgaenge = GatewayAusgang.objects.filter(gateway=gw)
                    for ausgang in ausgaenge:
                        if ausgang.regelung and ausgang.regelung.regelung == "differenzregelung":
                            logger.warning("%s|%s" % (ausgang.regelung.id, data['error']))
                continue

            if values.get('position') is not None:
                sig_new_output_value.send(sender="differenzregelung",
                                          name="dregout_%s" % dregid,
                                          value=values['position'],
                                          ziel=values['position'],
                                          parameters=str({}),  # todo
                                          timestamp=datetime.strptime(values['last_drive'], "%Y-%m-%d %H:%M"),
                                          modules=[])
            if values.get('s1') is not None:
                sig_new_temp_value.send(sender="differenzregelung",
                                        name="dreg_%s_s1" % dregid,
                                        value=values['s1'],
                                        modules=[],
                                        timestamp=datetime.strptime(values['last_drive'], "%Y-%m-%d %H:%M"))
            if values.get('s2') is not None:
                sig_new_temp_value.send(sender="differenzregelung",
                                        name="dreg_%s_s2" % dregid,
                                        value=values['s2'],
                                        modules=[],
                                        timestamp=datetime.strptime(values['last_drive'], "%Y-%m-%d %H:%M"))
            if values.get('s3') is not None:
                sig_new_temp_value.send(sender="differenzregelung",
                                        name="dreg_%s_s3" % dregid,
                                        value=values['s3'],
                                        modules=[],
                                        timestamp=datetime.strptime(values['last_drive'], "%Y-%m-%d %H:%M"))

            logger.warning(
                "%s|%s" % (dregid,
                           u"%s%s%s%s" %
                           (
                               (u"S1: %.2f°. " % values.get("s1")) if values.get("s1") else "",
                               (u"S2: %.2f°. " % values.get("s2")) if values.get("s2") else "",
                               (u"S3: %.2f°. " % values.get("s3")) if values.get("s3") else "",
                               (u"Position: %s%%." % int(values.get("position"))) if values.get("position") is not None else "",
                           )
                           )
            )

        return HttpResponse()

    else:
        return HttpResponse(status=405)


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/differenzregelung/'>Differenzregelung</a>" % haus.id


def get_global_description_link():
    desc = u"Flexible Erstellung von 2-Punkt- und Differenz-Regelungen."
    desc_link = "http://support.controme.com/differenzregelung-und-solaranlage/"
    return desc, desc_link


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    if request.method == "GET":

        if action == "edit":
            ret = dict(haus=haus)

            ret['sensoren'] = list(Sensor.objects.filter(haus=haus).order_by("gateway_id", "name")) + \
                list(RFSensor.objects.filter(haus=haus).order_by("controller_id", "name")) + \
                list(RFAktor.objects.filter(haus=haus, type__in=["wt", 'hktControme']).order_by("controller_id", "name"))

            ret['dregid'] = entityid

            ret.update({
                "name": "",
                "diff_s2": 7,
                "min_s1": 25,
                "max_s1": 90,
                "max_s2": 75,
                "min_slider_val": 10,
                "max_slider_val": 60,
                "max_s3": 85,
                "hysteresis": 1.0,
                'einvzg': 0,
                'ausvzg': 0,
                'min_slider_val': 10,
                'max_slider_val': 60,
                'invertout': False
            })  # defaultwerte, werden ggf gleich ueberschrieben

            if entityid:
                params = haus.get_module_parameters()
                ret.update(params.get('differenzregelung', dict()).get(entityid, dict()))

            ret['icons'] = [
                '3-sensors_blue', 'buffer_yellow', 'floor-valve_blue', 'heater_blue','heating-valve_yellow',
                'pumpe_pink', 'thermometer_blue', '3-sensors_green', 'cloud', 'floor-valve_green', 'heater_green',
                'mischer_blue', 'pumpe_yellow', 'thermometer_green', '3-sensors_pink', 'floor-heating_blue',
                'floor-valve_pink', 'heater_pink', 'mischer_green', 'solar_blue', 'thermometer_pink',
                '3-sensors_yellow', 'floor-heating_green', 'heat-generator_blue', 'heater_yellow', 'mischer_pink',
                'solar_green', 'thermometer_yellow', 'buffer_blue', 'floor-heating_pink', 'heat-generator_green',
                'heating-valve_blue', 'mischer_yellow', 'solar_pink', 'buffer_green', 'floor-heating_yellow',
                'heat-generator_pink', 'heating-valve_green', 'pumpe_blue', 'solar_yellow', 'buffer_pink',
                'floor-valce_yellow', 'heat-generator_yellow', 'heating-valve_pink', 'pumpe_green', 'sun'
            ]

            return render_response(request, "m_settings_differenzregelung_edit.html", ret)

        elif action == "delete":
            regs = Regelung.objects.filter(regelung='differenzregelung')
            for reg in regs:
                regparams = reg.get_parameters()
                if regparams.get('dreg', '0') == entityid:
                    try:
                        reg.gatewayausgang.delete()
                    except:
                        pass
                    reg.delete()

            params = haus.get_module_parameters()
            del params['differenzregelung'][entityid]
            haus.set_module_parameters(params)

            # clearing cache for upcoming offset
            from heizmanager.modules.module import planning_modules
            for planned_mod in planning_modules:
                ch.delete_cache_upcoming_events(haus.id, planned_mod,
                                                 entityid, 'differenzregelung')
            ###

            return render_redirect(request, "/m_setup/%s/differenzregelung/" % haus.id)

        else:
            params = haus.get_module_parameters()
            return render_response(request, "m_settings_differenzregelung.html",
                                   {"haus": haus, "dreg": sorted(params.get("differenzregelung", dict()).iteritems())})

    elif request.method == "POST":

        dreg_id = request.POST.get('dreg', '0')
        s1 = request.POST.get('s1')
        s2 = request.POST.get('s2')
        s3 = request.POST.get('s3')
        name = request.POST.get('name')
        diff_s2 = request.POST.get('diff_s2', 7)
        min_s1 = request.POST.get('min_s1', 25)
        max_s1 = request.POST.get('max_s1', 90)
        max_s2 = request.POST.get('max_s2', 75)
        show_slider = True if request.POST.get('show_slider_ch') else False
        min_slider_val = request.POST.get("min_slider_val", 10)
        max_slider_val = request.POST.get("max_slider_val", 60)
        max_s3 = request.POST.get('max_s3', 85)
        einvzg = request.POST.get('einvzg', 0)
        ausvzg = request.POST.get('ausvzg', 0)
        benutzer_text_0 = request.POST.get('benutzer_text_0', '')
        benutzer_text_1 = request.POST.get('benutzer_text_1', '')
        try:
            invertout = bool(request.POST.get('invertout', 0))
        except:
            invertout = False
        try:
            show_in_menu = bool(request.POST.get('show_in_menu', 0))
        except:
            show_in_menu = False
        hysteresis = request.POST.get('hysteresis', 1.0)
        icon = request.POST.get('icon', "")
        lst = []
        for key in request.POST:
            if key.startswith('c_'):
                k = key.split('_')
                lst.append(k[1])

        params = haus.get_module_parameters()
        dregs = params.get('differenzregelung', dict())
        if dreg_id == '0':
            ids = [0] + [int(k) for k in dregs.keys()]
            dreg_id = str(max(ids)+1)

        err = u''
        if len({s1, s2, s3}) < 3 and not (s1 == "0" and s3 == "0"):
            err += u'Sensoren für Differenzregelung müssen unterschiedlich sein.\n<br/>'

        try:
            if int(diff_s2) < 0:
                err += u'Differenztemperatur muss positiv sein.\n<br/>'
        except (ValueError, TypeError):
            err += u"Bitte Differenztemperatur angeben.\n<br/>"

        try:
            if int(min_s1) > int(max_s1):
                err += u"Minimalwert für S1 muss kleiner sein als Maximalwert für S1.\n<br/>"
        except (ValueError, TypeError):
            err += u"Bitte Minimal-/Maximalwert für S1 angeben.\n<br/>"

        try:
            _ = int(max_s2)
        except (ValueError, TypeError):
            err += u"Bitte Maximalwert für S2 angeben.\n<br/>"

        try:
            _ = int(max_s3)
        except (ValueError, TypeError):
            err += u"Bitte Maximalwert für S3 angeben.\n<br/>"

        try:
            if int(min_slider_val) > int(max_slider_val):
                err += u'Minimaler Sliderwert muss kleiner sein als der Maximalwert.\n<br/>'
        except (ValueError, TypeError):
            err += u"Bitte Zahlenwerte für minimalen und maximalen Sliderwert angeben.\n<br/>"

        _s1 = AbstractSensor.get_sensor(s1)
        if s1 != "0" and _s1 is None:
            err += u'Bitte S1 auswählen.\n<br/>'

        _s2 = AbstractSensor.get_sensor(s2)
        if _s2 is None:
            err += u'Bitte S2 auswählen.\n<br/>'

        _s3 = AbstractSensor.get_sensor(s3)
        if s3 != "0" and _s3 is None:
            err += u'Bitte S3 auswählen.\n<br/>'

        if not len(name):
            err += 'bitte Namen eingeben.\n<br/>'

        if len(err):
            ret = dict(haus=haus, error=err)

            ret['sensoren'] = Sensor.objects.filter(haus=haus).order_by("gateway_id", "name")

            ret['dregid'] = None
            if dreg_id != '0':
                ret['dregid'] = dreg_id
                params = haus.get_module_parameters()
                ret.update(params.get('differenzregelung', dict()).get(dreg_id, dict()))
            else:
                ret.update({
                    "name": "",
                    "diff_s2": 7,
                    "min_s1": 25,
                    "max_s1": 90,
                    "max_s2": 75,
                    "max_s3": 85,
                    "hysteresis": 1.0,
                    'hrgw': 0,
                    "einvzg": 0,
                    "ausvzg": 0
                })
            return render_response(request, "m_settings_differenzregelung_edit.html", ret)

        dregs.setdefault(dreg_id, dict())
        dregs[dreg_id]['s1'] = s1
        dregs[dreg_id]['s2'] = s2
        dregs[dreg_id]['s3'] = s3
        dregs[dreg_id]['name'] = name
        dregs[dreg_id]['diff_s2'] = diff_s2
        dregs[dreg_id]['min_s1'] = min_s1
        dregs[dreg_id]['max_s1'] = max_s1
        dregs[dreg_id]['max_s2'] = max_s2
        dregs[dreg_id]['show_slider'] = show_slider
        dregs[dreg_id]['min_slider_val'] = min_slider_val
        dregs[dreg_id]['max_slider_val'] = max_slider_val
        dregs[dreg_id]['max_s3'] = max_s3
        dregs[dreg_id]['einvzg'] = einvzg
        dregs[dreg_id]['ausvzg'] = ausvzg
        dregs[dreg_id]['hysteresis'] = hysteresis
        dregs[dreg_id]['icon'] = icon
        dregs[dreg_id]['modules_list'] = lst
        dregs[dreg_id]['invertout'] = invertout
        dregs[dreg_id]['show_in_menu'] = show_in_menu
        dregs[dreg_id]['benutzer_text_0'] = benutzer_text_0
        dregs[dreg_id]['benutzer_text_1'] = benutzer_text_1

        # maintain backwards compatibility if possible
        hrgws = [gw for gw in Gateway.objects.all() if gw.is_heizraumgw()]
        if len(hrgws) == 1:
            dregs[dreg_id]['hrgw'] = hrgws[0].pk

        params['differenzregelung'] = dregs
        haus.set_module_parameters(params)
        for reg in Regelung.objects.filter(regelung='differenzregelung'):
            regparams = reg.get_parameters()
            if regparams['dreg'] == dreg_id:
                break
        else:
            reg = Regelung(regelung='differenzregelung', parameter="{'dreg': '%s'}" % dreg_id)
            reg.save()

        return render_redirect(request, "/m_setup/%s/differenzregelung/" % haus.id)


@has_regelung_access
@csrf_exempt
def get_local_settings_page_haus(request, haus, action=None, objid=None):

    if request.method == "GET":

        if action == "show":
            context = {}
            context['logging'] = 'logger' in haus.get_modules()
            hparams = haus.get_module_parameters()
            config = hparams.get('differenzregelung', dict()).get(objid)
            if config is None:
                err = u'Ungültige Differenzregelung.'
                return render_response(request, "m_differenzregelung_show.html", {'haus': haus, 'error': err, 'logging': context['logging']})

            context['haus'] = haus
            context['config'] = config
            context['objid'] = objid
            context['dregid'] = objid

            berlin = pytz.timezone('Europe/Berlin')
            now = datetime.now(berlin)
            yd = (now-timedelta(days=1)).strftime("%Y-%m-%dT00:00")
            now = now.strftime("%Y-%m-%dT%H:%M")
            context['von'] = yd
            context['bis'] = now

            regelung_offsets = get_module_offsets_regelung(haus, objid, 'differenzregelung', as_dict=True)
            regelung_offset = sum(regelung_offsets.values())
            module_lines = get_module_line_list_regelung(haus, objid, 'differenzregelung')
            context['mods'] = module_lines

            for s in ['s1', 's2', 's3']:
                context[s] = None
                sensor = AbstractSensor.get_sensor(config[s])
                if sensor is not None:
                    context[s] = {}
                    context[s]['sensor'] = sensor
                    val = sensor.get_wert()
                    if val:
                        if len(val):
                            context[s]['val'] = val[0]
                            context[s]['converted'] = (100.0*val[0]) / 120
                            if val[0] > 120:
                                context[s]['converted'] = 100
                            if val[0] < 0:
                                context[s]['converted'] = 0

            offset_converted = (100*abs(regelung_offset))/120
            context['offset_converted'] = abs(offset_converted)
            context['offset'] = regelung_offset
            try:
                max_s2 = float(config['max_s2'])
            except:
                max_s2 = 75
            ziel = max_s2 + regelung_offset
            context['ziel_s2'] = ziel

            try:
                hysteresis = float(config.get('hysteresis', 1))
            except ValueError:
                hysteresis = 1

            context['ziel_center'] = hysteresis / 2

            for var, default in [('min_s1', 25), ('max_s1', 90), ('diff_s2', 7), ('max_s2', 75), ('ziel_s2', ziel), ('max_s3', 85)]:
                try:
                    v = float(config[var])
                except:
                    v = default
                convert = min((100 * v) / 120, 100)
                context['vbar_%s' % var] = convert

            config['min_slider_val'] = config.get('min_slider_val', 10)
            config['max_slider_val'] = config.get('max_slider_val', 60)

            # macaddr = hrgw.name
            regs = Regelung.objects.filter(regelung="differenzregelung", parameter="{'dreg': '%s'}" % objid)
            if len(regs) > 1:
                for r in regs[1:]:
                    r.delete()
            reg = regs[0]
            status = ch.get("cclast_%s" % reg.id)
            if status:
                status = status[1]
            else:
                for ausgang in GatewayAusgang.objects.filter(regelung=reg):
                    if ausgang.gateway.name.startswith(("b8", "dc")):
                        status = ch.get("cc_%s" % ausgang.gateway.name)
                        break
                else:
                    status = None

            context['beam'] = False
            # result
            result = ''
            result_text = 'kein Ergebnis'
            if isinstance(status, dict):  # ausgang an hrgw
                for dregid, values in status.items():
                    if dregid == objid:
                        if values.get('position'):
                            context['beam'] = True
                            result = 1
                            result_text = config.get('benutzer_text_1', '')
                        else:
                            context['beam'] = False
                            result = 0
                            result_text = config.get('benutzer_text_0', '')
            else:  # ausgang an anderem gw
                context['beam'] = status

            context['result'] = result
            context['result_text'] = result_text

            if regelung_offsets:
                offsets_items = dict((mod, abs(offset)) for mod, offset in regelung_offsets.items())
                max_module = max(offsets_items.iteritems(), key=operator.itemgetter(1))
            else:
                max_module = 0
            context['max_icon'] = max_module
            config['hysteresis'] = float(config['hysteresis'])

            return render_response(request, "m_differenzregelung_show.html", context)

        if action == "timer":
            return render_redirect(request, '/get_regelung_module/%s/timer/%s/%s/' % (haus.id, objid, 'differenzregelung'))

    elif request.method == "POST":
        logging.warning("asdf")

        try:
            slider_number = int(request.POST['slidernumber'])
        except (ValueError, TypeError, AttributeError):
            return HttpResponse("error")
        haus = Haus.objects.get(id=haus.id)
        params = haus.get_module_parameters()
        dregs = params.get('differenzregelung', dict())
        dregs[objid]['max_s2'] = slider_number
        params['differenzregelung'] = dregs
        haus.set_module_parameters(params)
        return HttpResponse("done")
