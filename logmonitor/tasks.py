# -*- coding: cp1252 -*-
from celery import shared_task
from heizmanager.models import Haus
from heizmanager.mobile.m_temp import get_module_offsets
from heizmanager.models import Gateway, AbstractSensor, Raum, Haus, Sensor
from itertools import chain
import heizmanager.cache_helper as ch
import pytz
from datetime import datetime
import logging
from heizmanager.mobile.m_error import CeleryErrorLoggingTask
import math

logger = logging.getLogger('servicemonitorlog')


@shared_task(base=CeleryErrorLoggingTask)
def update_service_monitor():

    for haus in Haus.objects.all():

        active_mods = haus.get_modules()

        # todo after a while we should remove these lines
        if 'servicemonitor' in active_mods:
            active_mods.remove('servicemonitor')
            haus.set_modules(active_mods)

        if 'logmonitor' not in active_mods:
            return

    try:
        checks = {
            'gws': 'Gateway',
            'sens': 'Sensor'
        }
    
        currentCache = ch.get('ServiceMonitor')
        
        if currentCache is None:
            currentCache = dict()
        
        gws = Gateway.objects.all()
        
        for g in gws:
            last = ch.get_gw_ping(g.name)
            berlin = pytz.timezone('Europe/Berlin')
            now = datetime.now(berlin)               
            if last is not None: #if gw send anything yet
                if len(last) and (now-last[0]).total_seconds() > 750: # last ping is old but was there before
                    if currentCache.get("gw" + g.name) is None: # if monitor has no entry yet, create one
                        logger.warning("%s|%s|Gateway offline seit %1.0f Minuten" %(checks['gws'], g.name, math.ceil((now-last[0]).total_seconds() / 60.0)))
                        currentCache["gw" + g.name] = "Set"
                    else:
                        pass #logger.warning("gw offline but was before %s, %s, %s, %1.0f" % (g.name,last[0], now,(now-last[0]).total_seconds()))
                else:
                    if currentCache.get("gw" + g.name) is not None: # if monitor has entry , delte key and write good in monitor
                        logger.warning("%s|%s|Gateway wieder online" %(checks['gws'], g.name))
                        del currentCache["gw" + g.name]
                    else:
                        pass #logger.warning("gw online but was not before %s, %s, %s, %%1.0f" % (g.name,last[0], now,(now-last[0]).total_seconds()))
            else:
                #gw did not send anything yet
                pass
                
        for haus in Haus.objects.all():
        
            gwdelay = haus.profil.get().get_gateway_delay()
            if int(gwdelay) == 1:
                gwdelay = 90

            liniendict = {
                1: "Klemme 32",
                2: "Klemme 33",
                3: "Klemme 29",
                4: "Klemme 30",
                5: "Klemme 31"
            }

            liniendict_hrgw = {
                -1: "-",
                0: "-", 1: "1", 2: "-", 3: "-", 4: "2", 5: "3", 6: "4", 7: "5"
            }

            berlin = pytz.timezone('Europe/Berlin')
            now = datetime.now(berlin)

            gwdict = dict((gw.id, gw) for gw in gws)
            gwdict[None] = "ohne Gateway"

            liniensort = ["unzugeordnet", "Klemme 31", "Klemme 30", "Klemme 29", "Klemme 33", "Klemme 32"]
            liniensort_hrgw = ["unzugeordnet", "-", "1", "2", "3", "4", "5"]
            sensoren = [{"%s - %s" % (gw, i): []} for gw in gws for i in liniensort if not gw.is_heizraumgw()]
            sensoren += [{"%s - %s" % (gw, i): []} for gw in gws for i in liniensort_hrgw if gw.is_heizraumgw()]
            sensoren.append({"ohne Gateway": []})
            l2idx = dict((d.keys()[0], i) for i, d in enumerate(sensoren))

            for sensor in list(chain(haus.sensor_related.values_list("id", "name", "haus_id", "linie", "raum_id", "description", "gateway_id", "mainsensor"),
                                     haus.luftfeuchtigkeitssensor_related.values_list("id", "name", "haus_id", "linie", "raum_id", "description", "gateway_id", "mainsensor"))):
                if sensor[6] is not None and gwdict[sensor[6]].is_heizraumgw():
                    last, isOffline = checkLast(sensor[1], liniendict_hrgw[sensor[3]] if sensor[3] else "",  gwdelay)
                else:
                    last, isOffline = checkLast(sensor[1], liniendict[sensor[3]] if sensor[3] and 0 < sensor[3] < 6 else "",  gwdelay)
                if 'pt1000' in sensor[1]:
                    sname = 'PT1000 an %s' % sensor[1].split('#')[0]
                elif 'ui10' in sensor[1]:
                    sname = 'Digitaleingang 1 an %s' % sensor[1].split('#')[0]
                elif 'ui20' in sensor[1]:
                    sname = 'Digitaleingang 2 an %s' % sensor[1].split('#')[0]
                else:
                    sname = sensor[1]

                if sensor[4]:
                    sd = {
                        sname: [
                            sensor[0],
                            sensor[4],
                            "TBD",
                            last,
                            isOffline,
                            gwdict[sensor[6]].name if sensor[6] is not None else "unzugeordnet"
                        ]
                    }
                elif sensor[5]:
                    sd = {
                        sname: [
                            sensor[0],
                            None,
                            u"- / - - '%s'" % sensor[5],
                            last,
                            isOffline,
                            gwdict[sensor[6]].name if sensor[6] is not None else "unzugeordnet"
                        ]
                    }
                else:
                    sd = {
                        sname: [
                            sensor[0] if 'pt1000' not in sensor[1] else '',
                            None,
                            "- / -",
                            last,
                            isOffline,
                            gwdict[sensor[6]].name if sensor[6] is not None else "unzugeordnet"
                        ]
                    }

              
                if sensor[3]:
                    if sensor[6] is None or sensor[6] not in gwdict:
                        linienname = "ohne Gateway"
                    else:
                        if sensor[6] is not None and gwdict[sensor[6]].is_heizraumgw():
                            linienname = liniendict_hrgw[sensor[3]] if sensor[3] else "-"
                        else:
                            linienname = liniendict[sensor[3]] if sensor[3] and 0 < sensor[3] < 6 else ""
                        linienname = "%s - %s" % (gwdict[sensor[6]], linienname)
                else:
                    if sensor[6] is None or sensor[6] not in gwdict:
                        linienname = "ohne Gateway"
                    else:
                        linienname = "%s - unzugeordnet" % gwdict[sensor[6]]
                sensoren[l2idx[linienname]][linienname].append(sd)
            for i, rdata in enumerate(sensoren): 
                for rstr, sensors in rdata.items(): # rstr = klemme xy, rdata sensorid: data
                    for ii in sensors:
                        for j in ii:
                            if ii[j][4] == False and (j in currentCache and currentCache[j] == True): # if online but was marked as offline before
                                logger.warning("%s|%s|Sensor wieder online. (Linie=%s / %s)" %(checks['sens'], j,ii[j][5], rstr))
                                currentCache[j] = False
                            elif ii[j][4] == True and (j not in currentCache or currentCache[j] == False): # if offline and was not offline or known before
                                logger.warning("%s|%s|Sensor offline. (Linie=%s / %s)" %(checks['sens'], j,ii[j][5],rstr))
                                currentCache[j] = True
                            elif ii[j][4] == True: # offline but was offline before aka still offline
                                currentCache[j] = True                            
                            else:
                                currentCache[j] = False
                            
        ch.set('ServiceMonitor', currentCache)
    except Exception as e:
        logger.exception("Service Monitor exception")
        
        
def checkLast(ssn, linie, gwdelay):
    last = ch.get_last_temp(ssn)
    isOffline =  True
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)               

    if last:
        if len(last) >= 2 and last[1] and (now-last[1]).total_seconds() < 605:
            isOffline = False           
        else:
            isOffline = True
        if len(last) >= 2:
            temp = last[0]
            last = last[1].strftime("am %d.%m. um %H:%M")
            if ssn.startswith("26_"):
                last += " mit %s%%" % temp
                if linie:
                    last += " auf Linie %s" % linie
            elif "ui" in ssn:
                last += " mit %d" % temp
            else:
                if temp == 85 or temp is None:
                    last = u"Fehler! Bitte Leitungen pruefen!"
                else:
                    last += " mit %.2f&deg;" % temp
                    if linie:
                        last += " auf Linie %s" % linie
        else:
            isOffline = False # if no cache is there, sensor was never seen since system start. This does not count as offline
    else: # if not in cache it was never seen since system start -> do nothing
        return "", False
    return last, isOffline

