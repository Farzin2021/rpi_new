from django.apps import AppConfig


class PumpenlogikConfig(AppConfig):
    name = 'pumpenlogik'
    verbose_name = 'Raumanforderung'

    def ready(self):
        from . import signals
