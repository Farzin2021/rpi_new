# -*- coding: utf-8 -*-
from heizmanager.render import render_response, render_redirect
import heizmanager.cache_helper as ch
import logging
import heizmanager.pytz as pytz
from datetime import datetime, timedelta
import wetter.views as wetter
import math
from heizmanager.models import Raum


def get_name():
    return u"Hochtarifoptimierung"


def is_hidden_offset():
    return False


def is_togglable():
    return True


def get_cache_ttl():
    return 57600  # sec von 6 bis 22 Uhr. HT Zeit hernehmen?


def get_offset(haus, raum=None):
    offset = 0.0
    ttl = 1  # 0 wuerde nie expiren
    if raum and 'hochtarifoptimierung' in raum.get_modules() and 'hochtarifoptimierung' in haus.get_modules():
        hparams = haus.get_module_parameters() 
        rparams = raum.get_module_parameters() 
        
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
        wochentag = now.weekday()

        if 'hochtarifoptimierung' not in hparams \
                or 'beginm' not in hparams['hochtarifoptimierung']\
                or 'endm' not in hparams['hochtarifoptimierung']\
                or 'begins' not in hparams['hochtarifoptimierung']\
                or 'ends' not in hparams['hochtarifoptimierung']:
            return offset
        
        if wochentag < 5:  # mo-fr
            begin = hparams['hochtarifoptimierung']['beginm']
            end = hparams['hochtarifoptimierung']['endm']
        elif wochentag == 5:  # samstag
            begin = hparams['hochtarifoptimierung']['begins']
            end = hparams['hochtarifoptimierung']['ends']
        else:  # sonntag
            begin = 0
            end = 0
        
        if begin <= now.hour < end:  # HT
            max_temp_drop = rparams['hochtarifoptimierung'].get('max_drop', 0.0)
            soll = raum.solltemp
            ist = raum.solltemp
            ms = raum.get_mainsensor()
            if ms:
                ist = raum.get_mainsensor().get_wert()
            if (rparams['hochtarifoptimierung']['active'] or rparams['hochtarifoptimierung'].get('alwayson', False)) \
                    and not rparams['hochtarifoptimierung']['currently_off'] \
                    and (max_temp_drop == 0.0 or (ist > soll-max_temp_drop)):
                offset = -max_temp_drop
                logging.warning("ht -> offset = -%s" % max_temp_drop)
            else:
                offset = 0.0
                logging.warning("ht -> offset = 0")

            ttl = -1
            if ttl < 0:
                ttl = 600

        else:  # NT
            # nur wenn wir < 2h vor Beginn HT sind, berechnen und cache_flag setzen
            if begin > now.hour >= begin-2:
                
                if rparams.get('hochtarifoptimierung', dict()).get('alwayson', False):
                    rparams['hochtarifoptimierung']['active'] = True
                
                else:
                    forecast = wetter.get_forecast(haus, (end-begin)+(begin-now.hour))

                    if forecast is None:
                        logging.warning("hto -> kein forecast")
                        pass
                    else:
                        s = rparams['hochtarifoptimierung']['schaerfe']
                        i = rparams['hochtarifoptimierung']['isolierung']
                        temps = []
                        for hour, temp in forecast:
                            if begin <= hour <= end:
                                temps.append(float(temp))

                        temps = sorted(temps)
                        if not len(temps):
                            summe = 0
                            div = 1
                        elif i == 0:
                            summe = sum(temps[0:4])
                            div = len(temps[0:4])
                        elif i == 1:
                            summe = sum(temps[0:8])
                            div = len(temps[0:8])
                        elif i == 2:
                            summe = sum(temps)
                            div = len(temps)
                        elif i == 3:
                            summe = sum(temps[-8:])
                            div = len(temps[-8:])
                        elif i == 4:
                            summe = sum(temps[-4:])
                            div = len(temps[-4:])

                        avg = float(summe)/div  # oder runden mit round()?
                        logging.warning("temps: %s || summe: %s || div: %s || avg: %s || s: %s" %
                                        (temps, summe, div, avg, s))

                        if avg > s:
                            rparams['hochtarifoptimierung']['active'] = True
                            logging.warning('ht -> aktiv')
                        else:
                            rparams['hochtarifoptimierung']['active'] = False
                            logging.warning('ht -> nicht aktiv')
                
                rparams['hochtarifoptimierung']['currently_off'] = False
                raum.set_module_parameters(rparams)
                #ttl = timedelta(hours=begin-now.hour).seconds-tdm.seconds-now.minute*60
                #if ttl < 0: ttl = 5*60
                ttl = 900  # nicht rechnen, einfach setzen, so dass es bald wieder verschwindet.

            elif now.hour >= end or now.hour < (begin-2):  # damits keine komischen ueberlappungen gibt.
                ttl = 1800  # dann sollten wir das cache_flag in den 2h irgendwann verlieren
                offset = 0.0
                rparams['hochtarifoptimierung']['currently_off'] = False
                rparams['hochtarifoptimierung']['active'] = False
                raum.set_module_parameters(rparams)
                ch.delete_module_offset_raum("hochtarifoptimierung", haus.id, raum.id)
                logging.warning("hto -> falsche NT zeit: %s" % str(now.hour))
            else:
                logging.warning("hto -> else mit zeit: %s" % str(now.hour))

        logging.warning("htoraumcaching%s: %s for %s secs" % (str(raum.id), str(offset), str(ttl)))
        if ttl == 0:
            ttl = 600
        ch.set_module_offset_raum('hochtarifoptimierung', haus.id, raum.id, offset, ttl=ttl)

    ch.set_module_offset_haus('hochtarifoptimierung', haus.id, 0.0, ttl=get_cache_ttl())
    return offset
    
    
def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/hochtarifoptimierung/'>Hochtarifoptimierung</a>" % haus.id


def get_global_description_link():
    desc = u"Für Wärmepumpen mit Doppeltarifzähler. Dann heizen, wenn Strom günstig ist."
    desc_link = "http://support.controme.com/auswahl-und-aktivierung-von-modulen/modul-hochtarifoptimierung/"
    return desc, desc_link


def get_global_settings_page(request, haus):
    
    if request.method == "GET":
        active = False
        if 'hochtarifoptimierung' in haus.get_modules():
            active = True
        zeit = _get_zeit_select()
        params = haus.get_module_parameters()
        s = 5
        i = 2
        if 'hochtarifoptimierung' in params:
            s = params['hochtarifoptimierung'].get('schaerfe', 5)  # falls beim ersten Mal nicht gesetzt
            i = params['hochtarifoptimierung'].get('isolierung', 2)  # falls beim ersten Mal nicht gesetzt
            beginm = params['hochtarifoptimierung']['beginm']
            endm = params['hochtarifoptimierung']['endm']
            begins = params['hochtarifoptimierung']['begins']
            ends = params['hochtarifoptimierung']['ends']
            max_drop = params['hochtarifoptimierung'].get('max_drop', 0.0)
        else:
            beginm = 6
            endm = 22
            begins = 6
            ends = 13
            max_drop = 0.0

        return render_response(request, 'm_settings_hochtarifoptimierung.html',
                               {"haus": haus, "active": active, "zeit": zeit, "s": s, "i": i, "beginm": beginm,
                                "endm": endm, "begins": begins, "ends": ends, 'max_drop': max_drop})
    
    elif request.method == "POST" and request.user.userprofile.get().role != 'K':

        if 'alwayson' in request.POST and request.POST['alwayson'] == '1':
            for etage in haus.etagen.all():
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    rparams = raum.get_module_parameters()
                    rparams.setdefault('hochtarifoptimierung', dict())
                    rparams['hochtarifoptimierung']['alwayson'] = True

                    # setzen wir, damit das naechste /get/ den Raum ausschaltet
                    if not 'active' in rparams['hochtarifoptimierung']:
                        #rparams['hochtarifoptimierung']['active'] = True
                        rparams['hochtarifoptimierung']['currently_off'] = False
                    # damit das naechste /get/ was zu tun hat
                    ch.set_module_offset_raum('hochtarifoptimierung', haus.id, raum.id, -100.0, ttl=600)
                    ch.delete("%s_roffsets_dict" % raum.id)

                    raum.set_module_parameters(rparams)
        elif 'alloff' in request.POST and request.POST['alloff'] == '1':
            for etage in haus.etagen.all():
                for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    rparams = raum.get_module_parameters()
                    rparams.setdefault('hochtarifoptimierung', dict())
                    rparams['hochtarifoptimierung']['alwayson'] = False
                    ch.delete_module_offset_raum('hochtarifoptimierung', haus.id, raum.id)
                    ch.delete("%s_roffsets_dict" % raum.id)

                    raum.set_module_parameters(rparams)
        else:
            params = haus.get_module_parameters()
            # failsafe. aktiviert, aber keine default params. beim raum ist das wichtiger.
            params.setdefault('hochtarifoptimierung', dict())

            if 'p_set' in request.POST and request.POST['p_set'] == '1':
                s = int(request.POST['s'], 10)
                i = int(request.POST['i'], 10)
                try:
                    drop = math.fabs(float(request.POST['max_drop'].replace(',', '.')))
                except ValueError:
                    drop = 0.0
                params['hochtarifoptimierung']['schaerfe'] = s
                params['hochtarifoptimierung']['isolierung'] = i
                params['hochtarifoptimierung']['max_drop'] = drop
                activateall = False
                if 'activateall' in request.POST and request.POST['activateall'] == 'on':
                    activateall = True
                for etage in haus.etagen.all():
                    for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                        rparams = raum.get_module_parameters()
                        rparams.setdefault('hochtarifoptimierung', dict())
                        rparams['hochtarifoptimierung']['schaerfe'] = s
                        rparams['hochtarifoptimierung']['isolierung'] = i
                        rparams['hochtarifoptimierung']['max_drop'] = drop

                        if not 'active' in rparams['hochtarifoptimierung']:
                            rparams['hochtarifoptimierung']['active'] = False

                        raum.set_module_parameters(rparams)

                        if activateall:
                            activate(raum)

            beginm = int(request.POST['beginm'], 10)
            endm = int(request.POST['endm'], 10)
            begins = int(request.POST['begins'], 10)
            ends = int(request.POST['ends'], 10)
            params['hochtarifoptimierung']['beginm'] = beginm
            params['hochtarifoptimierung']['endm'] = endm
            params['hochtarifoptimierung']['begins'] = begins
            params['hochtarifoptimierung']['ends'] = ends

            haus.set_module_parameters(params)

        return render_redirect(request, '/m_setup/%s/hochtarifoptimierung/' % haus.id)


def get_global_settings_page_help(request, haus):
    return render_response(request, "m_help_hochtarifoptimierung.html", {'haus': haus})


def get_local_settings_link(request, raum):
    if not 'hochtarifoptimierung' in raum.etage.haus.get_modules():
        return ""
    a = 'deaktiviert'
    if 'hochtarifoptimierung' in raum.get_modules():
        a = 'nicht aktiv'
    
        params = raum.get_module_parameters()
        if (params.get('hochtarifoptimierung', dict()).get('active', False)
                or params.get('hochtarifoptimierung', dict()).get('alwayson', False)) \
                and not params.get('hochtarifoptimierung', dict()).get('currently_off', True):
            a = 'aktiv'

    return "Hochtarifoptimierung", a, 75, "/m_raum/%s/hochtarifoptimierung/" % raum.id


def get_local_settings_page(request, raum):
    if request.method == "GET":
        
        if 'htdel' in request.GET and request.user.userprofile.get().role != 'K':
            params = raum.get_module_parameters()
            params['hochtarifoptimierung']['currently_off'] = True
            raum.set_module_parameters(params)
            
            # wir koennen nur deaktivieren, wenns grade aktiv ist.
            # dementsprechend ist das folgende cache flag dann eigtl ttl=*delta bis ende der aktuellen ht periode*
            #ch.set_module_offset_raum('hochtarifoptimierung', raum.etage.haus.id, raum.id, 0.0, ttl=600)
            ch.delete_module_offset_raum('hochtarifoptimierung', raum.etage.haus.id, raum.id)
            
            return render_redirect(request, '/m_raum/%s/' % raum.id)
        
        elif 'htadd' in request.GET and request.user.userprofile.get().role != 'K':
            params = raum.get_module_parameters()
            params['hochtarifoptimierung']['active'] = True
            params['hochtarifoptimierung']['currently_off'] = False
            raum.set_module_parameters(params)
            
            berlin = pytz.timezone('Europe/Berlin')
            now = datetime.now(berlin)
            hparams = raum.etage.haus.get_module_parameters()
            if now.weekday() == 5:  # samstag
                end = hparams.get('hochtarifoptimierung', dict()).get('ends', 13)  #  ['hochtarifoptimierung']['ends']
            else:  # mo-fr, so kanns gar nicht sein
                end = hparams.get('hochtarifoptimierung', dict()).get('endm', 22)  # ['hochtarifoptimierung']['endm']
            tdm = timedelta(minutes=15)
            ttl = timedelta(hours=end-now.hour).seconds-tdm.seconds-now.minute*60
            # ttl = 600  # wegen max_drop

            if ttl < 0:
                pass
            elif 0 < ttl < 600:
                ch.set_module_offset_raum('hochtarifoptimierung', raum.etage.haus.id, raum.id, -100.0, ttl=600)
            else:
                ch.set_module_offset_raum('hochtarifoptimierung', raum.etage.haus.id, raum.id, -100.0, ttl=ttl)
            
            return render_redirect(request, '/m_raum/%s/' % raum.id)
            
        else:
            params = raum.get_module_parameters()
            
            active = False
            if (params.get('hochtarifoptimierung', dict()).get('active', False) or
                    params.get('hochtarifoptimierung', dict()).get('alwayson', False)) \
                    and not params.get('hochtarifoptimierung', dict()).get('currently_off', False):
                active = True
            
            activated = False
            if 'hochtarifoptimierung' in raum.get_modules():
                activated = True
            
            gactive = False
            if 'hochtarifoptimierung' in raum.etage.haus.get_modules():
                gactive = True
            
            alwayson = False
            if params.get('hochtarifoptimierung', dict()).get('alwayson', False):
                alwayson = True
            
            s = params.get('hochtarifoptimierung', dict()).get('schaerfe', 15)
            i = params.get('hochtarifoptimierung', dict()).get('isolierung', 2)
            max_drop = params.get('hochtarifoptimierung', dict()).get('max_drop', 0.0)
            
            return render_response(request, 'm_raum_hochtarifoptimierung.html',
                                   {"raum": raum, 'active': active, 'activated': activated,
                                    'gactive': gactive, 'alwayson': alwayson, 's': s, 'i': i, 'max_drop': max_drop})
    
    elif request.method == "POST" and request.user.userprofile.get().role != 'K':
        
        if 'htaktiv' in request.POST and request.POST['htaktiv'] == '1':
            params = raum.get_module_parameters()
            s = int(request.POST['s'], 10)
            i = int(request.POST['i'], 10)
            try:
                drop = math.fabs(float(request.POST['max_drop'].replace(',', '.')))
            except ValueError:
                drop = 0.0
            
            params.setdefault('hochtarifoptimierung', dict())
            params['hochtarifoptimierung']['schaerfe'] = s
            params['hochtarifoptimierung']['isolierung'] = i
            params['hochtarifoptimierung']['max_drop'] = drop
            if not 'active' in params['hochtarifoptimierung']:
                params['hochtarifoptimierung']['active'] = False
            
            if 'alwayson' in request.POST and request.POST['alwayson'] == '1':
                if 'alwayson' in params['hochtarifoptimierung'] and params['hochtarifoptimierung']['alwayson'] is True:
                    pass  # keine Aenderung
                else:
                    params['hochtarifoptimierung']['alwayson'] = True
                    params['hochtarifoptimierung']['currently_off'] = False
                    params['hochtarifoptimierung']['active'] = True
                    ch.delete_module_offset_raum('hochtarifoptimierung', raum.etage.haus.id, raum.id)
            else:
                if params['hochtarifoptimierung'].get('alwayson', False):
                    params['hochtarifoptimierung']['alwayson'] = False
                    ch.delete_module_offset_raum('hochtarifoptimierung', raum.etage.haus.id, raum.id)
                else:
                    pass  # keine Aenderung
            
            raum.set_module_parameters(params)
            activate(raum)
        
        else:
            ch.delete_module_offset_raum('hochtarifoptimierung', raum.etage.haus.id, raum.id)
            deactivate(raum)
        
        return render_redirect(request, '/m_raum/%s/' % raum.id)


def activate(hausoderraum):
    if not 'hochtarifoptimierung' in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['hochtarifoptimierung'])


def deactivate(hausoderraum):
    modules = hausoderraum.get_modules()
    try:
        modules.remove('hochtarifoptimierung')
        hausoderraum.set_modules(modules)
    except ValueError:  # nicht in modules
        pass


def _get_zeit_select():
    ret = []
    for i in range(0,24):
        ret.append((i, str(i).zfill(2)+":00"))
    return ret
