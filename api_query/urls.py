from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^m_setup/(?P<haus>\d+)/api_query/(?P<action>[a-z_]+)/$', views.get_global_settings_page),
    url(r'^m_setup/(?P<haus>\d+)/api_query/(?P<action>[a-z_]+)/(?P<entityid>\d+)/$', views.get_global_settings_page),
]
