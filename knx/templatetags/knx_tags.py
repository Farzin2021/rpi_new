from django import template
import heizmanager.cache_helper as ch
register = template.Library()


@register.simple_tag
def get_wert_and_ts(dev):
    last_vals = ch.get_last_vals("%s_%s_val" % (dev.gateway.address.replace('.', '_'), dev.get_url_ga()))
    if last_vals is None or not isinstance(last_vals, tuple) or len(last_vals) != 4:
        return "-"

    if dev.datapoint_type == "9.001":
        temp = "%0.2f" % last_vals[0]
    else:
        temp = last_vals[0]
    last = last_vals[1].strftime("am %d.%m. um %H:%M")
    return "%s mit %s" % (last, temp)
