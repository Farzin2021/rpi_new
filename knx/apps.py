from django.apps import AppConfig


class KNXConfig(AppConfig):
    name = 'knx'
    verbose_name = 'KNX'

    def ready(self):
        from heizmanager.models import sig_solltemp_change
        from signals import knx_solltemp_change
        sig_solltemp_change.connect(knx_solltemp_change)