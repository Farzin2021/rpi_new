from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^set/knx/(?P<rpimac>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/(?P<controllerid>([0-9A-Za-z.]*))/$', views.set_knx),
    url(r'^get/knx/(?P<controllerid>([0-9A-Za-z.]*))/$', views.get_knx),
    url(r'^m_setup/(?P<haus>\d+)/knx/(?P<action>[a-z]+)/$', views.get_global_settings_page),
    url(r'^m_setup/(?P<haus>\d+)/knx/(?P<action>[a-z]+)/(?P<entityid>\d+_\d+_\d+)/$', views.get_global_settings_page),
    ]
