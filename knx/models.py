# -*- coding: utf-8 -*-

from django.db import models
from caching.base import CachingManager, CachingMixin
from heizmanager.models import AbstractDevice, AbstractSensor, AbstractOutput, Raum, Haus
import ast
from heizmanager.fields import ListField
from django.db.models.signals import post_init
import django.dispatch as dispatch
import heizmanager.cache_helper as ch
from datetime import datetime
import pytz
from sqlite3 import OperationalError


class KNXGateway(CachingMixin, models.Model):

    address = models.CharField(max_length=12)
    parameters = models.TextField()
    haus = models.ForeignKey(Haus)

    objects = CachingManager()

    def __init__(self, *args, **kwargs):
        self._sensoren = list()
        super(KNXGateway, self).__init__(*args, **kwargs)

    def get_parameters(self):
        try:
            return ast.literal_eval(self.parameters)
        except SyntaxError:
            return {}

    def set_parameters(self, params):
        self.parameters = str(params)
        self.save()

    def get_type(self):
        return type(self).__name__

    @property
    def name(self):
        return self.address  # fuer hfo

    @property
    def description(self):
        return "KNX"  # fuer systemcheck

    def __unicode__(self):
        return "KNX"

    def get_sensoren(self):
        if not len(self._sensoren):
            _set_knxgateway_sensoren(sender='Gateway', instance=self)
        return self._sensoren

    def get_marker(self):
        last = ch.get_gw_ping(self.address)
        if last is None or not isinstance(last, tuple) or len(last) != 2:
            return 'red'
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
        if (now-last[0]).total_seconds() > 400:
            return 'red'
        else:
            return 'green'

    class Meta:
        app_label = 'knx'


class KNXDevice(AbstractDevice):

    # das kann ein physisches gerät sein, muss aber nicht. entsprechend hier keine verknüpfung zu knxgateway.
    # brauchen wir aber für xknx, damit wir zb Climate() vernünftig instantiieren können (geht aber auch ohne)

    parameters = models.TextField()

    objects = CachingManager()

    def get_parameters(self):
        try:
            return ast.literal_eval(self.parameters)
        except SyntaxError:
            return {}

    def set_parameters(self, params):
        self.parameters = str(params)
        self.save()

    class Meta:
        app_label = 'knx'


class KNXGroupObject(AbstractDevice):  # dt. Kommunikationsobjekt

    DPT_CHOICES = (('1.001', '1.001'),  # switch 0/1
                   ('5.001', '5.001'),  # light brightness / valve 0-100
                   ('9.001', '9.001'),  # temperatur
                   )

    group_address = models.CharField(max_length=12)
    rec_group_addresses = ListField(models.CharField(), default=[])  # von diesen adressen kann empfangen werden. optional.
    datapoint_type = models.CharField(choices=DPT_CHOICES, blank=False, max_length=8)
    parameters = models.TextField()

    gateway = models.ForeignKey(KNXGateway, on_delete=models.CASCADE)
    device = models.ForeignKey(KNXDevice, blank=True, null=True, on_delete=models.SET_NULL)  # m2m waere besser, aber wurscht, weil das ja eh nur eine abstraktion ist

    objects = CachingManager()

    def get_parameters(self):
        try:
            return ast.literal_eval(self.parameters)
        except SyntaxError:
            return {}

    def set_parameters(self, params):
        self.parameters = str(params)
        self.save()

    def get_identifier(self):
        return "%s*%s" % (type(self).__name__, self.id)

    def get_url_ga(self):
        return self.group_address.replace('/', '_')

    def get_marker(self):
        last_vals = ch.get_last_vals("%s_%s_val" % (self.gateway.address.replace('.', '_'), self.get_url_ga()))
        if last_vals is None or not isinstance(last_vals, tuple) or len(last_vals) != 4:
            return 'red'
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
        if (now-last_vals[1]).total_seconds() > 1800:
            return 'red'
        else:
            return 'green'

    class Meta:
        app_label = 'knx'
        abstract = True
        unique_together = ('group_address', 'gateway',)


class KNXAktor(KNXGroupObject):

    # todo self.rec_group_addresses im formular sichtbar machen, für status datapoints von aktoren
    # todo das dann auch an knx_handler rausgeben und damit dann in set_knx für aktoren den marker schreiben

    raum = models.ForeignKey(Raum, on_delete=models.SET_NULL, blank=True, null=True)

    objects = CachingManager()

    @property
    def ctrl_device(self):
        return self.gateway

    @property
    def intga(self):
        return int(('00' + self.group_address.split('/')[0])[-3:] + ('00' + self.group_address.split('/')[1])[-3:] + ('00' + self.group_address.split('/')[2])[-3:])

    class Meta:
        app_label = 'knx'


class KNXSensor(KNXGroupObject, AbstractSensor):

    objects = CachingManager()

    def __unicode__(self):
        return self.description if len(self.description) else self.group_address

    def get_wert(self, val='Temperature'):
        last = ch.get_last_vals("%s_%s_val" % (self.gateway.address.replace('.', '_'), self.get_url_ga()))
        params = self.get_parameters()
        if val == "Temperature" and self.datapoint_type == "9.001":
            return last
        elif val == "Relative Humidity": 
            if params.get('sensortype') == 'humidity' and self.datapoint_type == "5.001":
                return last
            else:
                return None
        else:
            return last

    @property
    def name(self):
        return self.group_address

    def get_typ(self):
        params = self.get_parameters()
        return params.get('sensortyp')

    @property
    def ctrl_device(self):
        return self.gateway

    @property
    def intga(self):
        return int(('00' + self.group_address.split('/')[0])[-3:] + ('00' + self.group_address.split('/')[1])[-3:] + ('00' + self.group_address.split('/')[2])[-3:])

    class Meta:
        app_label = 'knx'


class KNXAusgang(AbstractOutput):

    gateway = models.ForeignKey(KNXGateway, related_name='ausgaenge')
    aktor = ListField(models.ForeignKey(KNXAktor), default=[])

    objects = CachingManager()

    def get_ctrldevice(self):
        # ist an RFAusgang auch nicht implementiert
        pass

    def get_regelung(self):
        if self.regelung_id:
            regelung = self.regelung
            return regelung
        return None

    @property
    def ausgang(self):
        return ','.join([KNXAktor.objects.get(pk=_a).group_address for _a in self.aktor])

    def is010v(self):
        for aktor in [KNXAktor.objects.get(pk=_a) for _a in self.aktor]:
            if aktor.datapoint_type != "5.001":
                return False
        else:
            return True

    class Meta:
        app_label = 'knx'


@dispatch.receiver(post_init, sender=Raum)
def _set_raum_sensoren(sender, instance, **kwargs):
    knxsensoren = KNXSensor.objects.filter(raum=instance)
    try:
        instance._sensoren += list(knxsensoren)
    except OperationalError:
        pass


@dispatch.receiver(post_init, sender=KNXGateway)
def _set_knxgateway_sensoren(sender, instance, **kwargs):
    sensoren = sorted(KNXSensor.objects.filter(gateway=instance), key=lambda x: x.intga)
    for s in sensoren:
        instance._sensoren.append(s)
