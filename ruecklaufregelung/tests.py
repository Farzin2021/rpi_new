from django.test import TestCase, RequestFactory
from heizmanager.models import Haus, HausProfil, Etage, Raum, Regelung, Sensor, Gateway, GatewayAusgang
from django.core.cache import cache as memcache
import heizmanager.cache_helper as ch
import views as rlr
import logging
from heizmanager import pytz
from datetime import datetime, timedelta
from heizmanager.getandset import tset, get


class RLRTest(TestCase):

    def setUp(self):
        self.haus = Haus(name="Teststr. 1, 12345 Testhausen")
        self.haus.save()
        hausprofil = HausProfil(haus=self.haus, modules='ruecklaufregelung', module_parameters=str({}))
        hausprofil.save()
        etage = Etage(name='testetage', haus=self.haus, position=0)
        etage.save()
        self.raum = Raum(name='testraum', etage=etage, position=0, solltemp=21.0, module_parameters='{}')
        self.raum.save()
        self.gw = Gateway(name='aa-bb-cc-dd-ee-ff', haus=self.haus)
        self.gw.save()
        self.rlsensor1 = Sensor(name='11_22_33_44_55_66_77_88', raum=self.raum, haus=self.haus, gateway=self.gw)
        self.rlsensor1.save()
        self.rlsensor2 = Sensor(name='11_22_33_44_55_66_77_89', raum=self.raum, haus=self.haus, gateway=self.gw)
        self.rlsensor2.save()
        self.rtsensor = Sensor(name='11_22_33_44_55_66_77_87', raum=self.raum, haus=self.haus, gateway=self.gw)
        self.rtsensor.save()
        params = {'active': True, 'rlsensorssn': {self.rlsensor1.get_identifier(): [1]}, 'neigung': 5.5, 'offset': 20, 'kruemmung': 2.0, 'schaerfe': 0.0, 'intervall': 600}
        self.reg = Regelung(regelung='ruecklaufregelung', parameter=str(params))
        self.reg.save()
        self.raum.regelung = self.reg
        self.raum.save()

        # fuer 13 & 14
        self.ausgang = GatewayAusgang(gateway=self.gw, ausgang="1", regelung=self.reg)
        self.ausgang.save()

        berlin = pytz.timezone('Europe/Berlin')
        self.now = datetime.now(berlin)

        memcache._cache.flush_all()

        self.factory = RequestFactory()

    def testRLR01(self):
        # 1 rls, rlonly, keine werte.
        self.assertEqual(rlr.do_ausgang(self.reg, 1).content, '<0>')

    def testRLR02(self):
        # 1 rls, rlonly, kein last_get, wegen at ist < soll
        ch.set_new_temp(self.rlsensor1.name, 19.0, self.haus.id)
        memcache.set('weather_%s' % self.haus.id, [(((self.now.hour+1) % 24), 15), (((self.now.hour+2) % 24), 20)])
        self.assertEqual(rlr.do_ausgang(self.reg, 1).content, '<1>')

    def testRLR03(self):
        # 1 rls, rlonly, kein last_get, wegen at ist > soll
        ch.set_new_temp(self.rlsensor1.name, 22.0, self.haus.id)
        memcache.set('weather_%s' % self.haus.id, [(((self.now.hour+1) % 24), 22), (((self.now.hour+2) % 24), 20)])
        self.assertEqual(rlr.do_ausgang(self.reg, 1).content, '<0>')

    def testRLR04(self):
        # 1 rls, rlonly, last_get == 0 aber zu jung, wegen at ist > soll
        ch.set_new_temp(self.rlsensor1.name, 19.0, self.haus.id)
        memcache.set('weather_%s' % self.haus.id, [(((self.now.hour+1) % 24), 22), (((self.now.hour+2) % 24), 20)])
        ch.set('lastget_%s_%s' % (self.reg.id, 1), ("0", self.now-timedelta(minutes=5), False))
        self.assertEqual(rlr.do_ausgang(self.reg, 1).content, '<0>')

    def testRLR05(self):
        # 1 rls, rlonly, last_get == 0, automatisch anmachen wg. spuelung
        ch.set_new_temp(self.rlsensor1.name, 19.0, self.haus.id)
        memcache.set('weather_%s' % self.haus.id, [(((self.now.hour+1) % 24), 22), (((self.now.hour+2) % 24), 20)])
        ch.set('lastget_%s_%s' % (self.reg.id, 1), ("0", self.now-timedelta(minutes=11), False))
        self.assertEqual(rlr.do_ausgang(self.reg, 1).content, '<1>')

    def testRLR06(self):
        # 1 rls, rlonly, last_get == 1 (egal ob zu jung oder nicht), wegen at ist < soll
        ch.set_new_temp(self.rlsensor1.name, 19.0, self.haus.id)
        memcache.set('weather_%s' % self.haus.id, [(((self.now.hour+1) % 24), 22), (((self.now.hour+2) % 24), 20)])
        ch.set('lastget_%s_%s' % (self.reg.id, 1), ("1", self.now-timedelta(minutes=9), False))
        self.assertEqual(rlr.do_ausgang(self.reg, 1).content, '<1>')

    def testRLR07(self):
        # 2 rls (einer zu kalt, einer zu warm), kein last_get
        params = self.reg.get_parameters()
        params['rlsensorssn'][self.rlsensor2.get_identifier()] = [2]
        self.reg.set_parameters(params)
        ch.set_new_temp(self.rlsensor1.name, 19.0, self.haus.id)
        ch.set_new_temp(self.rlsensor2.name, 24.0, self.haus.id)
        memcache.set('weather_%s' % self.haus.id, [(((self.now.hour+1) % 24), 22), (((self.now.hour+2) % 24), 20)])
        self.assertEqual(rlr.do_ausgang(self.reg, 1).content, '<1>')
        self.assertEqual(rlr.do_ausgang(self.reg, 2).content, '<0>')

    def testRLR08(self):
        # 2 rls (einer zu kalt, einer zu warm), mit last_get: 1 bleibt, 2 geht auf spuelung trotz zu warm
        params = self.reg.get_parameters()
        params['rlsensorssn'][self.rlsensor2.get_identifier()] = [2]
        self.reg.set_parameters(params)
        ch.set_new_temp(self.rlsensor1.name, 19.0, self.haus.id)
        ch.set_new_temp(self.rlsensor2.name, 24.0, self.haus.id)
        ch.set('lastget_%s_%s' % (self.reg.id, 1), ("1", self.now-timedelta(minutes=11), False))
        ch.set('lastget_%s_%s' % (self.reg.id, 2), ("0", self.now-timedelta(minutes=11), False))
        memcache.set('weather_%s' % self.haus.id, [(((self.now.hour+1) % 24), 22), (((self.now.hour+2) % 24), 20)])
        self.assertEqual(rlr.do_ausgang(self.reg, 1).content, '<1>')
        self.assertEqual(rlr.do_ausgang(self.reg, 2).content, '<1>')

    def testRLR09(self):
        # 1 rls, 1 rt, kein wetter
        self.rtsensor.mainsensor = True
        self.rtsensor.save()
        self.raum.set_mainsensor(self.rtsensor)
        ch.set_new_temp(self.rlsensor1.name, 24.0, self.haus.id)
        ch.set_new_temp(self.rtsensor.name, 22.0, self.haus.id)
        self.assertEqual(rlr.do_ausgang(self.reg, 1).content, '<0>')

    def testRLR10(self):
        # 1 rls, 1 rt, wetter
        self.rtsensor.mainsensor = True
        self.rtsensor.save()
        self.raum.set_mainsensor(self.rtsensor)
        ch.set_new_temp(self.rlsensor1.name, 24.0, self.haus.id)
        ch.set_new_temp(self.rtsensor.name, 22.0, self.haus.id)
        memcache.set('weather_%s' % self.haus.id, [(((self.now.hour+1) % 24), 22), (((self.now.hour+2) % 24), 20)])
        self.assertEqual(rlr.do_ausgang(self.reg, 1).content, '<0>')

    def testRLR11(self):
        # 1 rls, 1 rt, wetter, last_get == 0
        self.rtsensor.mainsensor = True
        self.rtsensor.save()
        self.raum.set_mainsensor(self.rtsensor)
        ch.set_new_temp(self.rlsensor1.name, 24.0, self.haus.id)
        ch.set_new_temp(self.rtsensor.name, 22.0, self.haus.id)
        ch.set('lastget_%s-%s' % (self.reg.id, 1), ("0", self.now-timedelta(minutes=11), False))
        memcache.set('weather_%s' % self.haus.id, [(((self.now.hour+1) % 24), 22), (((self.now.hour+2) % 24), 20)])
        self.assertEqual(rlr.do_ausgang(self.reg, 1).content, '<0>')

    def testRLR12(self):
        # 1 rls, 1 rt, wetter, last_get == 1
        self.rtsensor.mainsensor = True
        self.rtsensor.save()
        self.raum.set_mainsensor(self.rtsensor)
        ch.set_new_temp(self.rlsensor1.name, 24.0, self.haus.id)
        ch.set_new_temp(self.rtsensor.name, 20.0, self.haus.id)
        ch.set('lastget_%s-%s' % (self.reg.id, 1), ("1", self.now-timedelta(minutes=11), False))
        memcache.set('weather_%s' % self.haus.id, [(((self.now.hour+1) % 24), 18), (((self.now.hour+2) % 24), 20)])
        self.assertEqual(rlr.do_ausgang(self.reg, 1).content, '<1>')

    def testRLR13(self):
        # ganz einfacher Fall, aber ueber HTTP Request
        # einmal Aufruf 03, dann Aufruf 04. Aber mit <1> expected, damit nicht default fall irgendwo auftritt.
        memcache.set('weather_%s' % self.haus.id, [(((self.now.hour+1) % 24), 15), (((self.now.hour+2) % 24), 20)])
        request = self.factory.get("/set/%s/19.00/" % self.rlsensor1.name)
        response = tset(request, self.rlsensor1.name, "19.00")
        request = self.factory.get("/get/%s/1/" % self.gw.name)
        response = get(request, self.gw.name, "1")
        self.assertEqual(response.content, "<1>")

    def testRLR14(self):
        # ganz einfacher Fall, aber ueber HTTP Request
        # Fall 04, mit manuellem setzen von last_get zuvor
        memcache.set('weather_%s' % self.haus.id, [(((self.now.hour+1) % 24), 24), (((self.now.hour+2) % 24), 20)])
        ch.set('lastget_%s_%s' % (self.reg.id, 1), ("0", self.now-timedelta(minutes=11), False))
        request = self.factory.get("/set/%s/19.00/" % self.rlsensor1.name)
        response = tset(request, self.rlsensor1.name, "19.00")
        request = self.factory.get("/get/%s/1/" % self.gw.name)
        response = get(request, self.gw.name, "1")
        self.assertEqual(response.content, "<1>")

    def testRLR15(self):
        # einfacher Fall mit RFAusgang
        from rf.models import RFAusgang, RFController, RFAktor
        rfc = RFController(name='0x12345678', haus=self.haus)
        rfc.save()
        rfa = RFAktor(name="1", controller=rfc)
        rfa.save()
        aktoren = [rfa.id]
        rfausgang = RFAusgang(regelung=self.reg, controller=rfc, aktor=aktoren)
        rfausgang.save()

        ch.set_new_temp(self.rlsensor1.name, 19.0, self.haus.id)
        memcache.set('weather_%s' % self.haus.id, [(((self.now.hour+1) % 24), 22), (((self.now.hour+2) % 24), 20)])
        ch.set('lastget_%s_%s' % (self.reg.id, 1), ("0", self.now-timedelta(minutes=11), False))
        self.assertEqual(rlr.do_rfausgang(self.reg, rfc.name), 1)