from django.apps import AppConfig


class ZweipunktregelungConfig(AppConfig):
    name = 'zweipunktregelung'
    verbose_name = 'Zweipunktregelung'

    def ready(self):
        from . import signals
