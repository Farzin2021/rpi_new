from heizmanager.models import sig_get_outputs_from_regelung, sig_get_possible_outputs_from_regelung, sig_create_regelung, Raum
from django.dispatch import receiver
import zweipunktregelung.views as zpr
import heizmanager.cache_helper as ch
import logging


@receiver(sig_get_outputs_from_regelung)
def zpr_get_outputs(sender, **kwargs):
    haus = kwargs['haus']
    raum = kwargs['raum']
    regelung = kwargs['regelung']
    if regelung.regelung == 'zweipunktregelung':
        return zpr.get_outputs(haus, raum, regelung)
    else:
        return {}


@receiver(sig_get_possible_outputs_from_regelung)
def zpr_get_possible_outputs(sender, **kwargs):
    haus = kwargs['haus']
    raumdict = kwargs.get('raumdict', {})
    return zpr.get_possible_outputs(haus, raumdict)


@receiver(sig_create_regelung)
def zpr_create_regelung(sender, **kwargs):

    regelung = kwargs['regelung']
    if regelung is None or regelung.regelung != 'zweipunktregelung':
        return

    operation = kwargs['operation']
    sensortyp = kwargs['sensortyp']
    sensor = kwargs['sensor']
    changed_sensor = kwargs['changed_sensor']
    outs = kwargs['outs']

    if operation == 'addsensor':
        if sensortyp == 'rt':
            sensor.mainsensor = True
            sensor.save()

            r = Raum.objects.get(pk=sensor.raum_id)
            r.set_mainsensor(sensor)

    elif operation == 'deletesensor':
        if changed_sensor.mainsensor and (
                            sensortyp != 'rt' or
                            (changed_sensor.raum_id and changed_sensor.raum_id != sensor.raum_id) or
                            (changed_sensor.gateway_id and changed_sensor.gateway_id != sensor.gateway_id)
        ):

            changed_sensor.mainsensor = False
            changed_sensor.save()

            r = Raum.objects.get(pk=changed_sensor.raum_id)
            r.set_mainsensor(None)

            for out in outs:
                out[3].delete()
                if len(out) > 4:
                    for o in out[4]:
                        ch.delete("reg_%s" % regelung.id)
                        ch.delete("lastget_%s_%s" % (regelung.id, o))
            if len(outs):
                return "Aufgrund der Eingaben wurden bereits zugeordnete Ausg&auml;nge modifiziert. Bitte &uuml;berpr&uuml;fen Sie die Ausgangszuordnung."
