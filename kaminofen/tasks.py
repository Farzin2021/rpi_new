# -*- coding: utf-8 -*-
from celery import shared_task
from heizmanager.models import Haus, AbstractSensor, Raum
import pytz
from datetime import datetime, timedelta


def activation_condition(s1, s2, diff_activation):
    return s2 - s1 > diff_activation


def deactivation_condition(s1, s2, diff_deactivation):
    return s2 - s1 < diff_deactivation


def do_process(haus):
    k_params = haus.get_spec_module_params('kaminofen')
    activate_rooms = list()
    deactivate_rooms = list()
    for k_id, k_param in k_params.items():
        s1 = k_param['raum_sensor']
        s2 = k_param['kaminofen_sensor']
        try:
            s1 = AbstractSensor.get_sensor(s1).get_wert()[0]
            s2 = AbstractSensor.get_sensor(s2).get_wert()[0]
        except:
            continue

        berlin = pytz.timezone("Europe/Berlin")
        now = datetime.now(berlin)
        k_params[k_id]['last_check'] = now.strftime('%Y-%m-%d %H:%M')
        k_params[k_id]['next_check'] = (now + timedelta(minutes=10)).strftime('%Y-%m-%d %H:%M')

        diff_activation = float(k_param.get('a_differenztemperatur', 3.0))
        diff_deactivation = float(k_param.get('d_differenztemperatur', 0.4))
        activation = activation_condition(s1, s2, diff_activation)
        deactivation = deactivation_condition(s1, s2, diff_deactivation)
        if activation:
            activate_rooms += k_param['rooms']
            k_params[k_id]['active'] = True
        elif deactivation:
            deactivate_rooms += k_param['rooms']
            k_params[k_id]['active'] = False
    haus.set_spec_module_params('kaminofen', k_params)

    activate_rooms = set(activate_rooms)
    deactivate_rooms = set(deactivate_rooms) - set(activate_rooms)
    # activate in rooms
    for raumid in activate_rooms:
        raum = Raum.objects.get(id=raumid)
        r_k_params = raum.get_spec_module_params('kaminofen')
        hfo_params = raum.get_hfo_params()
        if 'kept_after' not in r_k_params:
            val_to_keep = hfo_params.get('after', .5)
            hfo_params['after'] = 5.0
            r_k_params['kept_after'] = val_to_keep
        raum.set_spec_module_params('kaminofen', r_k_params)
        raum.set_spec_module_params('heizflaechenoptimierung', hfo_params)

    # deactivate in rooms
    for raumid in deactivate_rooms:
        raum = Raum.objects.get(id=raumid)
        r_k_params = raum.get_spec_module_params('kaminofen')
        hfo_params = raum.get_spec_module_params('heizflaechenoptimierung')
        if 'kept_after' in r_k_params:
            hfo_params['after'] = r_k_params['kept_after']
            del r_k_params['kept_after']
        raum.set_spec_module_params('kaminofen', r_k_params)
        raum.set_spec_module_params('heizflaechenoptimierung', hfo_params)


@shared_task()
def periodic_kaminofen_calculation():
    for haus in Haus.objects.all():
        # module activation check
        if 'kaminofen' in haus.get_modules():
            do_process(haus)
