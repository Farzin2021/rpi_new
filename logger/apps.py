from django.apps import AppConfig


class LoggerConfig(AppConfig):
    name = 'logger'
    verbose_name = 'controme heizmanager logger'

    def ready(self):
        from heizmanager.models import Haus, sig_new_output_value, sig_new_temp_value
        from models import write_temp_log, write_output_log
        sig_new_temp_value.connect(write_temp_log)
        sig_new_output_value.connect(write_output_log)
