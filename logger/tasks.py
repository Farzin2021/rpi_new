from celery import shared_task
from datetime import date, timedelta
import sqlite3
from fabric.api import local
from fabric.state import env
import csv
from heizmanager.mobile.m_error import CeleryErrorLoggingTask


env.hosts = ["localhost"]


@shared_task(base=CeleryErrorLoggingTask)
def clean_db():

    from heizmanager.models import AbstractSensor, Haus
    haus = Haus.objects.all()[0]
    try:
        vhz = float(haus.get_module_parameters().get('logger', {}).get('vhz', 14))
    except:
        vhz = 14

    today = date.today()
    cutoff = today - timedelta(days=vhz)
    if cutoff.day == 1:
        cutoffmonth = cutoff.month - 1 if cutoff.month > 1 else 12
    else:
        cutoffmonth = cutoff.month
    if cutoff.month == 1 and cutoff.day == 1:
        cutoffyear = cutoff.year - 1
    else:
        cutoffyear = cutoff.year
    try:
        local("ls /home/pi/rpi/usbmount | grep db_log", capture=True)
    except:
        return  # db existiert nicht

    conn = sqlite3.connect("/home/pi/rpi/usbmount/db_log.db")
    c = conn.cursor()

    try:
        open('/home/pi/rpi/usbmount/sensorlog_%s_%s.csv' % (cutoffyear, cutoffmonth), 'r+')
    except IOError:
        try:
            with open('/home/pi/rpi/usbmount/sensorlog_%s_%s.csv' % (cutoffyear, cutoffmonth), 'w') as f:
                f.write('Zeit,Sensorname,Wert,Sensorid\n')
        except IOError:
            # ro fs
            return

    try:
        ids = c.execute("""SELECT DISTINCT name FROM logger_simplesensorlog WHERE timestamp < '%s'""" % cutoff).fetchall()
    except:
        # OperationalError, table does not exist
        return

    id2names = dict((_id[0], AbstractSensor.get_sensor(_id[0]))
                    if 'Sensor' in _id[0] else (_id[0], _id[0])
                    for _id in ids)

    # cursor for sensorlog
    ret = c.execute("""SELECT timestamp, name, value FROM logger_simplesensorlog WHERE timestamp < '%s'""" % cutoff).fetchall()
    with open("/home/pi/rpi/usbmount/sensorlog_%s_%s.csv" % (cutoffyear, cutoffmonth), "a") as csvout:
        writer = csv.writer(csvout)
        for row in ret:
            writer.writerow([row[0], unicode(id2names[row[1]]).encode('utf-8'), row[2], row[1]])

    try:
        open('/home/pi/rpi/usbmount/outputlog_%s_%s.csv' % (cutoffyear, cutoffmonth), 'r+')
    except IOError:
        with open('/home/pi/rpi/usbmount/outputlog_%s_%s.csv' % (cutoffyear, cutoffmonth), 'w') as f:
            f.write('Zeit,Ausgang,Wert, ziel, parameter\n')

    # cursor for outputlog
    ret = c.execute(
        """SELECT timestamp, name, value, ziel, parameters FROM logger_simpleoutputlog WHERE timestamp < '%s'""" % cutoff).fetchall()
    with open("/home/pi/rpi/usbmount/outputlog_%s_%s.csv" % (cutoffyear, cutoffmonth), "a") as csvout:
        writer = csv.writer(csvout)
        for row in ret:
            writer.writerow([row[0], row[1], row[2], row[3], row[4]])

    # c.execute("""INSERT INTO logger_simplesensorlog_archive SELECT * FROM logger_simplesensorlog WHERE timestamp < '%s'""" % cutoff)
    c.execute("""DELETE FROM logger_simplesensorlog WHERE timestamp < '%s'""" % cutoff)
    # c.execute("""INSERT INTO logger_simpleoutputlog_archive SELECT * FROM logger_simpleoutputlog WHERE timestamp < '%s'""" % cutoff)
    c.execute("""DELETE FROM logger_simpleoutputlog WHERE timestamp < '%s'""" % cutoff)

    conn.commit()
    if today.weekday() == 0:
        local("sqlite3 /home/pi/rpi/usbmount/db_log.db 'vacuum logger_simplesensorlog;'")
        local("sqlite3 /home/pi/rpi/usbmount/db_log.db 'vacuum logger_simpleoutputlog;'")
        local("sqlite3 /home/pi/rpi/usbmount/db_log.db 'reindex logger_simplesensorlog;'")
        local("sqlite3 /home/pi/rpi/usbmount/db_log.db 'reindex logger_simpleoutputlog;'")

    try:
        ping = local("ping -c1 ntp.org", capture=True)
        if "100% packet loss" in ping:
            raise Exception("no internet")
        local("sudo service ntp stop")
        local("sudo ntpd -qg")
        local("sudo service ntp start")
    except:
        pass
