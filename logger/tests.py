from django.test import TestCase, Client
from models import SimpleOutputLog, SimpleSensorLog
from heizmanager.models import Haus, Etage, Raum, Sensor, Regelung, Gateway, GatewayAusgang, HausProfil
from datetime import timedelta
import logging
from django.utils import timezone
from django.core.cache import cache as memcache
from django.contrib.auth.models import User
from models import sig_new_output_value, sig_new_temp_value
from models import write_temp_log, write_output_log


class LoggingTest(TestCase):
    multi_db = True

    def setUp(self):
        self.usr = User.objects.create_user(username='test@test.de', email='test@test.de', password='test')
        self.usr.save()
        self.haus = Haus(name="Teststr. 1, 12345 Testhausen", eigentuemer=self.usr)
        self.haus.save()
        hausprofil = HausProfil(haus=self.haus, modules='logger', module_parameters=str({}))
        hausprofil.save()
        self.etage = Etage(haus=self.haus, position=0)
        self.etage.save()
        self.raum = Raum(etage=self.etage, position=0, solltemp=21.0)
        self.raum.save()

        self.gw = Gateway(name='aa-bb-cc-dd-ee-ff', haus=self.haus)
        self.gw.save()

        self.rtsensor = Sensor(haus=self.haus, raum=self.raum, name="11_22_33_44_55_66_77_01")
        self.rtsensor.save()
        self.rlsensor1 = Sensor(name='11_22_33_44_55_66_77_88', raum=self.raum, haus=self.haus, gateway=self.gw)
        self.rlsensor1.save()

        params = {'active': True, 'rlsensorssn': {self.rlsensor1.get_identifier(): [1]}, 'neigung': 5.5, 'offset': 20, 'kruemmung': 2.0, 'schaerfe': 0.0, 'intervall': 600}
        self.reg = Regelung(regelung='ruecklaufregelung', parameter=str(params))
        self.reg.save()
        self.raum.regelung = self.reg
        self.raum.save()

        self.ausgang = GatewayAusgang(gateway=self.gw, ausgang="1", regelung=self.reg)
        self.ausgang.save()

        memcache._cache.flush_all()
        self.client = Client()
        self.client.login(username='test@test.de', password='test')

    def tearDown(self):
        SimpleSensorLog.objects.all().delete()  # warum auch immer
        SimpleOutputLog.objects.all().delete()

    def testWriteLogs(self):

        now = timezone.now()
        dt = now-timedelta(days=1)

        while dt < now:
            dt += timedelta(minutes=3)

            sl = SimpleSensorLog(name=self.rtsensor.name, value=float('20.00'))
            sl.save()

            params = self.reg.get_parameters()
            params.update({'regelung': self.reg.regelung})
            ol = SimpleOutputLog(name="%s_%s" % (self.gw.name, self.ausgang.ausgang), value=bool(int(1)), ziel=self.raum.solltemp, parameters=params)
            ol.save()

        self.assertEqual(len(SimpleSensorLog.objects.all()), 480)
        self.assertEqual(len(SimpleOutputLog.objects.all()), 480)

        # hier noch mit RF testen

    def testDisConnectSignals(self):
        response = self.client.get('/set/%s/20.00/' % self.rtsensor.name)
        self.assertEqual(len(SimpleSensorLog.objects.all()), 1)

        response = self.client.get('/set/%s/21.00/' % self.rlsensor1.name)
        self.assertEqual(len(SimpleSensorLog.objects.all()), 2)

        sig_new_temp_value.disconnect(receiver=write_temp_log)

        response = self.client.get('/set/%s/21.10/' % self.rlsensor1.name)
        self.assertEqual(len(SimpleSensorLog.objects.all()), 2)

        sig_new_temp_value.connect(write_temp_log)

        response = self.client.get('/set/%s/21.20/' % self.rlsensor1.name)
        self.assertEqual(len(SimpleSensorLog.objects.all()), 3)

    def testConnectSignals(self):
        self.haus.set_modules([])
        response = self.client.get('/set/%s/20.00/' % self.rtsensor.name)
        self.assertEqual(len(SimpleSensorLog.objects.all()), 0)

        # wieder verbinden fuer die restlichen Tests
        self.haus.set_modules(['logger'])
        sig_new_temp_value.connect(write_temp_log)
        sig_new_output_value.connect(write_output_log)

    def testConfigure(self):
        pass

    def testDownload(self):
        pass