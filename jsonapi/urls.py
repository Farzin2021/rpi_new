from django.conf.urls import url
from . import views
urlpatterns = [ 
    url(r'^get/json/v(?P<jsonver>\d+)/(?P<hausid>\d+)/(?P<data>[A-Za-z_]+)/$', views.get_json),
    url(r'^get/json/v(?P<jsonver>\d+)/(?P<hausid>\d+)/(?P<data>[A-Za-z_]+)/(?P<entityid>\d+)/$', views.get_json),

    url(r'^set/json/v(?P<jsonver>\d+)/(?P<hausid>\d+)/(?P<data>[A-Za-z_]+)/$', views.set_json),
    url(r'^set/json/v(?P<jsonver>\d+)/(?P<hausid>\d+)/(?P<data>[A-Za-z_]+)/(?P<entityid>\d+)/$', views.set_json),
    ]
