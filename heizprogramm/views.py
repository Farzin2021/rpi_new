# -*- coding: utf-8 -*-

from heizmanager.render import render_response, render_redirect
from heizmanager.models import Haus
from datetime import datetime, timedelta
import heizmanager.cache_helper as ch
import logging
from django.http import HttpResponse
from django.http import JsonResponse
import json
from time import mktime
import pytz
from heizmanager.abstracts.base_time_task import TimeTask
from heizmanager.helpers import get_str_formatted_time
from temperaturszenen.views import TemperaturszenenTask

logger = logging.getLogger("logmonitor")


def set_heizprogramm_active(haus, hparams=None, heizp_id=None):

    if isinstance(haus, int):
        haus = Haus.objects.get(id=haus)

    if hparams is None:
        hparams = haus.get_module_parameters()

    if 'heizprogramms_list' not in hparams:
        heizp_list = [{'heizp_id': 0, 'heizp_name': 'Standard', 'is_active': True}]
        hparams['heizprogramms_list'] = heizp_list
        haus.set_module_parameters(hparams)

    else:
        heizp_list = hparams.get('heizprogramms_list', [])
        if heizp_id is not None:
            # active heizprogramm by given id
            for i, elem in enumerate(heizp_list):
                if elem['heizp_id'] == heizp_id:
                    heizp_list[i]['is_active'] = True
                else:
                    heizp_list[i]['is_active'] = False
            hparams['heizprogramms_list'] = heizp_list
            haus.set_module_parameters(hparams)
    # invalidate cache
    ch.delete_module_tasks(haus.id, 'heizprogramm')
    ch.delete_cache_upcoming_events(haus.id, 'heizprogramm', haus.id, "Haus")
    return heizp_list


class HeizprogrammTask(TimeTask):
    MOD = 'heizprogramm'

    method_factory = {
        'activate-heizprogramm': set_heizprogramm_active,
    }

    @staticmethod
    def get_module_tasks(haus):
        hparams = haus.get_module_parameters()
        # is heizprogramm activated
        if not hparams.get('is_switchingmode_enabled_%s' % haus.id, True):
            return []

        activated_heizp = get_activated_heizprogramm_dict(haus)
        tasks_list = list()
        entries = get_switches_list(haus)
        berlin = pytz.timezone('Europe/Berlin')
        berlin_now = datetime.now(berlin)
        # separate date from date time
        date = get_str_formatted_time(berlin_now).split(' ')[0]
        day_of_week = berlin_now.weekday()
        entries = filter(lambda x: x['day'] == day_of_week and
                         x.get('heizp_id', -1) == activated_heizp.get('heizp_id'), entries)
        for switch in entries:
            tm = "%s %s" % (date, switch['time'])
            tasks_list.append(TemperaturszenenTask('temperaturszenen', 'set-mode',
                              HeizprogrammTask.MOD, tm, hausid=haus.id, priority=1, mode_id=switch['mode_id']))
        return tasks_list


def get_cache_ttl():
    return 1800


def calculate_always_anew():
    return False


def get_offset(haus, raum=None):
    pass


def get_name():
    return 'Heizprogramm'


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/heizprogramm/'>Heizprogramm</a>" % str(haus.id)


def get_global_description_link():
    desc = u"Temperaturszenen zeitgesteuert umschalten."
    desc_link = "http://support.controme.com/heizprogramm/"
    return desc, desc_link


def get_local_settings_link(request, raum):
    pass


def activate(obj):
    if 'heizprogramm' not in obj.get_modules():
        obj.set_modules(obj.get_modules() + ['heizprogramm'])


def get_local_settings_page(request, raum):
    pass


def get_local_settings_page_haus(request, haus, action=None):

    hparams = haus.get_module_parameters()
    mode_hparams = hparams.get('temperaturmodi', {})
    
    # GET
    if request.method == "GET":
        page_param = {}
        page_param['haus'] = haus
        if 'add' in request.GET:
            return render_response(request, "m_heizprogramm_add.html", page_param)
        elif 'edit' in request.GET:
            heizp_id = int(request.GET.get('edit', 0))
            if not action:
                mode_items = []
                for index, (mode_id, mode_params) in enumerate(mode_hparams.iteritems(), 1):
                    mode_items.append((mode_id, mode_params, mode_params.get('order', index)))
                mode_items.sort(key=lambda tup: tup[2])
                page_param['mode_items'] = mode_items
                page_param['heizprogramm_id'] = heizp_id
                heizp_list = get_heizprogramm_list(haus, hparams)
                heizp = [item for item in heizp_list if item["heizp_id"] == heizp_id]
                if not len(heizp):
                    return render_redirect(request, '/m_heizprogramm/%s/' % haus.id)

                page_param['heizprogramm_name'] = heizp[0]['heizp_name']
                return render_response(request, "m_heizprogramm_edit.html", page_param)
        else:
            page_param['is_switchingmod_enabled'] = hparams.get('is_switchingmode_enabled_%s' % haus.id, True)
            page_param['heizprogramm_list'] = get_heizprogramm_list(haus, hparams)
            page_param['activated_entry'] = ch.get('fixed_entry_%s' % haus.id)
            return render_response(request, "m_heizprogramm.html", page_param)

    elif request.method == "POST":
        # invalidate cache
        ch.delete_module_tasks(haus.id, 'heizprogramm')
        ch.delete_cache_upcoming_events(haus.id, 'heizprogramm', haus.id, "Haus")
        ch.delete('heizprogramms_list_cache_%s' % haus.id)
        if 'add' in request.GET: # add new heizprogramm
            if 'heizprogramm_name' in request.POST and request.POST['heizprogramm_name'] != None:
                heizp_list = get_heizprogramm_list(haus, hparams)
                try:
                    max_id = max([d['heizp_id'] for d in heizp_list])
                except ValueError:
                    max_id = 0
                heizp_list.append({'heizp_id':max_id + 1, 'heizp_name':request.POST['heizprogramm_name'], 'is_active': len(heizp_list)<=0})
                hparams['heizprogramms_list'] = heizp_list
                haus.set_module_parameters(hparams)
                ch.set('heizprogramms_list_cache_%s' % haus.id, heizp_list)
            return render_redirect(request, "/m_heizprogramm/%s/" % haus.id)
        elif 'delete' in request.GET: # delete a heizprogramm
            heizp_id = int(request.GET.get('delete', None))
            if heizp_id != None:
                heizp_list = get_heizprogramm_list(haus, hparams)
                heizp_list[:] = [d for d in heizp_list if d.get('heizp_id') != heizp_id]
                hparams['heizprogramms_list'] = heizp_list

                # delete switches mode of deleted heizprogramm
                switches_mode_list = hparams.get('switchingmode', [])
                switches_mode_list = [d for d in switches_mode_list if ('heizp_id' not in d and heizp_id != 0) or ('heizp_id' in d and d['heizp_id'] != heizp_id)]
                hparams['switchingmode'] = switches_mode_list
                ch.set('switches_mode_list_%s' % haus.id, switches_mode_list)

                haus.set_module_parameters(hparams)
                ch.set('heizprogramms_list_cache_%s' % haus.id, heizp_list)
            return render_redirect(request, "/m_heizprogramm/%s/" % haus.id)
        elif 'edit' in request.GET: # edit a heizprogramm
            heizp_id = int(request.GET.get('edit', 0))
            if heizp_id<0:
                return HttpResponse(status=400)
            switches_mode_list = hparams.get('switchingmode', [])
            switches_mode_list_current = []
            
            # for working with older values of switches mode (add 'heizp_id' if not exist)
            for index, item in enumerate(switches_mode_list):
                if 'heizp_id' not in item:
                    item['heizp_id'] = 0
                    switches_mode_list[index] = item

            if 'fill_data' in request.POST:
                # get switches_mode_list for selected heizprogramm
                for q in switches_mode_list:
                    if heizp_id == 0 and ( q.get("heizp_id") == None or q['heizp_id'] == 0):
                        switches_mode_list_current.append(q)
                    elif q.get("heizp_id") and q['heizp_id'] == heizp_id:
                        switches_mode_list_current.append(q)
                return HttpResponse(json.dumps(switches_mode_list_current), content_type="application/json")
            # add entry to switching mod, single id shows one entry includes(day of week, time, mod)
            elif 'add_entry' in request.POST:
                import re
                pattern = re.compile("^\d\d:\d\d$")
                if not pattern.match(request.POST['time']):
                    return HttpResponse(status=400)
                sw_list = []
                if 'repeat' in request.POST:
                    sw_list = request.POST.getlist('repeat')

                time_object = datetime.strptime("2018-1-1-" + request.POST['time'], '%Y-%m-%d-%H:%M').timetuple()
                ts = mktime(time_object)
                for item in sw_list:
                    try:
                        max_id = max([d['id'] for d in switches_mode_list])
                    except ValueError:
                        max_id = 0

                    mode_id = int(request.POST.get('select-mode', -1))
                    if mode_id != -1:
                        switches_mode_list.append(
                            {
                            'id': max_id + 1,
                            'day': int(item),
                            'mode_id': mode_id,
                            'mode_name': mode_hparams.get(mode_id, {}).get("name", ""),
                            'ts': int(ts),
                            'time': request.POST['time'],
                            'heizp_id': heizp_id
                            }
                        )
                switches_mode_list = sorted(switches_mode_list, key=lambda k: k['ts'])
                ch.set('switches_mode_list_%s' % haus.id, switches_mode_list)
                hparams['switchingmode'] = switches_mode_list
                haus.set_module_parameters(hparams)
                # get switches_mode_list for selected heizprogramm
                for q in switches_mode_list:
                    if heizp_id == 0 and ( q.get("heizp_id") == None or q['heizp_id'] == 0):
                        switches_mode_list_current.append(q)
                    elif q.get("heizp_id") and q['heizp_id'] == heizp_id:
                        switches_mode_list_current.append(q)
                return HttpResponse(json.dumps(switches_mode_list_current), content_type="application/json")

            elif 'action' in request.POST:
                data_id = int(request.POST.get('data_id', 0))
                if not data_id:
                    return HttpResponse(status=400)
                # delete entry
                if request.POST['action'] == 'delete':
                    switches_mode_list = [d for d in switches_mode_list if d['id'] != data_id]
                    hparams['switchingmode'] = switches_mode_list
                    ch.set('switches_mode_list_%s' % haus.id, switches_mode_list)
                    haus.set_module_parameters(hparams)
                    return HttpResponse(json.dumps({'data_id': request.POST['data_id']}), content_type="application/json")
                # fill popup form
                if request.POST['action'] == 'fill_popup':
                    switches_mode_list = [d for d in switches_mode_list if d['id'] == data_id and d['heizp_id'] == heizp_id]
                    if(len(switches_mode_list)):
                        time = switches_mode_list[0]['time']
                        mode = switches_mode_list[0]['mode_id']
                        return HttpResponse(json.dumps({'data_id': data_id, 'time': time, 'mode': mode}), content_type="application/json")
                    else:
                        return HttpResponse(json.dumps({}), content_type="application/json")
                # edit entry
                if request.POST['action'] == 'edit':
                    for index, item in enumerate(switches_mode_list):
                        if item['id'] == data_id and item['heizp_id'] == heizp_id:
                            time_object = datetime.strptime("2018-1-1-" + request.POST['time'], '%Y-%m-%d-%H:%M').timetuple()
                            ts = mktime(time_object)

                            item['ts'] = ts
                            item['time'] = request.POST['time']
                            item['mode_id'] = int(request.POST['mode_id'])
                            item['mode_name'] = mode_hparams.get(int(request.POST['mode_id'])).get('name')
                            switches_mode_list[index] = item

                    switches_mode_list = sorted(switches_mode_list, key=lambda k: k['ts'])
                    ch.set('switches_mode_list_%s' % haus.id, switches_mode_list)
                    hparams['switchingmode'] = switches_mode_list
                    haus.set_module_parameters(hparams)
                    # get switches_mode_list for selected heizprogramm
                    for q in switches_mode_list:
                        if heizp_id == 0 and ( q.get("heizp_id") == None or q['heizp_id'] == 0):
                            switches_mode_list_current.append(q)
                        elif q.get("heizp_id") and q['heizp_id'] == heizp_id:
                            switches_mode_list_current.append(q)
                    return HttpResponse(json.dumps(switches_mode_list_current), content_type="application/json")

            elif 'reset_all' in request.POST:
                # delete switches mode of heizprogramm
                switches_mode_list = [d for d in switches_mode_list if d['heizp_id'] != heizp_id]
                #switches_mode_list = [d for d in switches_mode_list if ('heizp_id' not in d and heizp_id != 0) or ('heizp_id' in d and d['heizp_id'] != heizp_id)]
                hparams['switchingmode'] = switches_mode_list
                ch.set('switches_mode_list_%s' % haus.id, switches_mode_list)
                haus.set_module_parameters(hparams)
                return HttpResponse(status=204)

            elif 'edit_rename' in request.POST:
                set_heizprogramm_rename(haus, hparams, heizp_id, request.POST['new_name'])
                return render_redirect(request, "/m_heizprogramm/%s/" % haus.id)

        else: # sweeching between heizprogramms or enable/disable all
            if 'enabled' in request.POST:
                is_enable = request.POST.get('enabled')
                if is_enable == "true":
                    is_enable = True
                else:
                    is_enable = False
                hparams['is_switchingmode_enabled_%s' % haus.id] = is_enable
                haus.set_module_parameters(hparams)
                if is_enable == True:
                    logger.warning("%s|%s" % (0, 'Heizprogramme wurden manuell gestartet'))
                elif is_enable == False:
                    logger.warning("%s|%s" % (0, 'Heizprogramme wurden manuell gestoppt'))
                return HttpResponse(json.dumps({'enabled': is_enable}), content_type="application/json")
            elif 'activated' in request.POST:
                heizp_id_for_active = request.POST.get('activated')
                heizprogramm_list = get_heizprogramm_list(haus, hparams)
                old_heiz = ''
                new_heiz = ''
                for heiz in heizprogramm_list:
                    if heiz.get('is_active') == True:
                        old_heiz = heiz.get('heizp_name')
                    else:
                        pass
                heizp_list = set_heizprogramm_active(haus, hparams, int(heizp_id_for_active))
                ch.set('heizprogramms_list_cache_%s' % haus.id, heizp_list)
                for heiz in heizprogramm_list:
                    if heiz.get('heizp_id') == int(heizp_id_for_active):
                        new_heiz = heiz.get('heizp_name')
                    else:
                        pass
                logger.warning("%s|%s" % (0, u"Heizprogramm wurde manuell von %s auf %s gewechselt" % (old_heiz, new_heiz)))
                return HttpResponse(status=204)

        return HttpResponse('Error in sent data')


def unique(duplicate): 
    final_list = [] 
    for num in duplicate: 
        if num not in final_list: 
            final_list.append(num) 
    return final_list 


def get_heizprogramm_list(haus, hparams=None):
    if hparams == None:
        hparams = haus.get_module_parameters()
    heizp_list = []
    if 'heizprogramms_list' not in hparams:
        heizp_list = [{'heizp_id':0, 'heizp_name':'Standard', 'is_active': True}]
    else:
        heizp_list = hparams.get('heizprogramms_list', [])
    return heizp_list


def get_activated_heizprogramm_dict(haus):
    heizprogramm_list = haus.get_module_parameters().get('heizprogramms_list', [])
    try:
        heizprogramm_of_interest = next(item for item in heizprogramm_list if item["is_active"])
    except StopIteration:
        heizprogramm_of_interest = None

    # Standard is default
    if heizprogramm_of_interest is None:
        return {'heizp_id': 0, 'heizp_name': 'Standard', 'is_active': True}

    return heizprogramm_of_interest


def get_heizprogramm_active(haus, params=None):
    """
    :param haus: haus object
    :param params: give params
    :return: returns None or a dictionary like this {'heizp_id': 1, 'heizp_name': u'test', 'is_active': True}]
    """

    heiz_list = get_heizprogramm_list(haus, params)
    heiz_list = [d for d in heiz_list if d['is_active']]

    if len(heiz_list):
        heiz_list = heiz_list[0]
    else:
        heiz_list = None

    return heiz_list


def set_heizprogramm_rename(haus, hparams=None, heizp_id=None, heizp_name=None):

    if hparams is None:
        hparams = haus.get_module_parameters()

    heizp_list = []
    if 'heizprogramms_list' in hparams:
        heizp_list = hparams.get('heizprogramms_list', [])
        if heizp_id is not None and heizp_name is not None:
            # rename heizprogramm by given id
            for i, elem in enumerate(heizp_list):
                if elem['heizp_id'] == heizp_id:
                    heizp_list[i]['heizp_name'] = heizp_name
                    hparams['heizprogramms_list'] = heizp_list
                    haus.set_module_parameters(hparams)

    return heizp_list


def get_global_settings_page_help(request, haus):
    return None


def get_local_settings_link_haus(request, haus):
    pass


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    context = dict()
    h_params = haus.get_module_parameters()

    if request.method == "GET":
        context['haus'] = haus
        context['r_m_active'] = h_params.get('heizprogramm_right_menu', True)
        return render_response(request, "m_settings_heizprogramm.html", context)

    if request.method == "POST":
        if 'right_menu_ajax' in request.POST:
            h_params['heizprogramm_right_menu'] = bool(int(request.POST['right_menu_ajax']))
            haus.set_module_parameters(h_params)
            return JsonResponse({'message': 'done'})

        return render_redirect(request, '/m_setup/%s/heizprogramm/' % haus.id)


def get_switches_list(haus):
    switchings = haus.get_spec_module_params('heizprogramm')
    return switchings


def get_ts_from_time(tm):
    tm = datetime.strftime(tm, '%H:%M')
    tm = datetime.strptime("2018-1-1 " + tm, '%Y-%m-%d %H:%M').timetuple()
    return mktime(tm)


def upcoming_event_type(typ):
    types = {
        'switch': 'mode-change',
        'end_list': 'ending-list',
    }
    return types[typ]


def get_module_upcoming_events(haus, entity, entity_id, lookahead=12):
    """
    :param haus:
    :param lookahead: gives a value that shows how many hours should function calculate with
    :return: returns changing modes within given lookahead and show with
                 a list of tuples ('time', 'mode_id', 'type') type here is offset,
    """

    # switching modes are defined only in house parameters and every other entities should be ignore
    if not isinstance(entity, Haus):
        return None

    params = haus.get_module_parameters()
    # check if heizprogramm is enabled
    is_switchingmode_enabled = params.get('is_switchingmode_enabled_%s' % haus.id, True)
    berlin_timezone = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin_timezone)
    time_at_lookahead_h = datetime.now(berlin_timezone) + timedelta(hours=lookahead)
    ret = [(time_at_lookahead_h, None, upcoming_event_type('end_list'))]

    if not is_switchingmode_enabled:
        return ret
    ##

    # get activated heizprogramm
    activated_heiz = get_heizprogramm_active(haus, params)

    # get and sort switching modes
    switching_mode_list = params.get('switchingmode', [])

    # splitting time into several with the first of a day if it is between now to look ahead
    diff_d = (time_at_lookahead_h - now).days
    t_tuples = []
    temp_now = now
    for i in range(0, diff_d + 1):
        next_day = now + timedelta(days=i+1)
        next_day_start = next_day.replace(hour=0, minute=0, second=0)
        end_of_day = temp_now.replace(hour=23, minute=59, second=59)
        if now <= next_day_start <= time_at_lookahead_h:
            t_tuples.append((temp_now, end_of_day, temp_now.weekday()))
            temp_now = next_day_start

    t_tuples.append((temp_now, time_at_lookahead_h, temp_now.weekday()))
    ###

    # add entries to returning list
    for sw in switching_mode_list:
        action_ts = sw['ts']

        for tup in t_tuples:
            if get_ts_from_time(tup[0]) <= action_ts <= get_ts_from_time(tup[1])\
                    and tup[2] == sw['day']\
                    and sw['heizp_id'] == activated_heiz['heizp_id']:
                mode_id = sw['mode_id']
                ret.insert(0, (now.replace(hour=int(sw['time'].split(":")[0]), minute=int(sw['time'].split(":")[1]), second=0),
                               mode_id, upcoming_event_type('switch')))

    ret = sorted(ret, key=lambda x: x[0])
    ch.set_cache_upcoming_events('heizprogramm', haus.id, entity_id, 'Haus', ret, ttl=lookahead * 60 * 60)
    return ret


def get_jsonapi(haus, usr, entityid=None):
    hparams = haus.get_module_parameters()
    switches_mode_list = hparams.get('switchingmode', [])
    switches_mode_list = sorted(switches_mode_list, key=lambda d: d['day'])

    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)
    now.weekday()
    ret = []
    for sw in switches_mode_list:
        if sw['day'] >= now.weekday():
            day_diff = sw['day'] - now.weekday()
            sw_time = (now + timedelta(days=day_diff)).strftime('%Y-%m-%d ') + sw['time']
            ret.append({'name': sw['mode_name'], 'next_due_time': sw_time})

    return ret

