from celery import shared_task
import logging
from abstracts.base_time_task import TimeTask
from heizmanager.models import Haus
import pytz
from datetime import datetime, timedelta
import helpers
from heizmanager.mobile.m_error import CeleryErrorLoggingTask


logger = logging.getLogger('logmonitor')


@shared_task(base=CeleryErrorLoggingTask)
def every_minute_task_manager():
    berlin = pytz.timezone('Europe/Berlin')
    berlin_now = datetime.now(berlin)
    berlin_now = berlin_now - timedelta(seconds=berlin_now.second, microseconds=berlin_now.microsecond)
    next_min = berlin_now + timedelta(minutes=1)
    now_ts = helpers.get_ts_from_str_time(helpers.get_str_formatted_time(berlin_now))
    ts_next_min = helpers.get_ts_from_str_time(helpers.get_str_formatted_time(next_min))

    for haus in Haus.objects.all():
        tasks = TimeTask.get_all_modules_tasks(haus)
        # high priority tasks are for tasks that need a repopulating, like heizprogramm changing
        high_prio_tasks = filter(lambda x:  (x.ts <= now_ts < x.end_ts if x.end_tm else now_ts <= x.ts < ts_next_min)
                                 and x.p == TimeTask.HIGH, tasks)

        re_populate = False
        # tasks with high priority should be run first
        for task in high_prio_tasks:
            re_populate = True
            task.execute()

        # if there was any task with high priority we need to populate tasks again
        if re_populate:
            tasks = TimeTask.get_all_modules_tasks(haus)

        tasks = filter(lambda x: (x.ts <= now_ts < x.end_ts if x.end_tm else now_ts <= x.ts < ts_next_min)
                       and x.p != TimeTask.HIGH, tasks)
        tasks.sort(key=lambda x: x.p, reverse=True)
        for task in tasks:
            task.execute()


