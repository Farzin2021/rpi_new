from heizmanager.models import *
from users.models import UserProfile
from django.http import HttpResponse
from django.core import serializers
from importlib import import_module
import logging
from django.core.serializers import base
import heizmanager.cache_helper as ch
from django.core.exceptions import FieldError
import base64


tdict = {'hausprofil': 'HausProfil', 'raumgruppe': 'RaumGruppe',
         'vsensor': 'VSensor', 'gatewayausgang': 'GatewayAusgang'}


def set_object(sender, instance):
    import httplib2
    import urllib
    import ast

    rpi = RPi.objects.get(pk=1)
    serdata = _serialize([instance])
    encdata = rpi.crypt_keys.get().encrypt(serdata)
    data = {'user': rpi.haus.eigentuemer.username, 'data': encdata, 'rpi': rpi.name}
    body = urllib.urlencode(data)

    h = httplib2.Http()
    resp, content = h.request("http://controme-main.appspot.com/sync/set/", method="POST", body=body)
    #forecast = ast.literal_eval(content)
    #return forecast

def del_object(instance, sender):
    pass


def get_all(request):
    """
    Serialisiert ein ganzes Haus.

    Die Serialisierungsreihenfolge ist so, dass bei der
    Deserialisierung alle Abhaengigkeiten eingehalten
    werden.
    """

    ret = list()

    if request.method == "POST":

        rpi_name = request.POST.get("rpi", None)
        rpi = RPi.objects.get(name=rpi_name)

        # check: rpi muss zum haus und zur verschluesselung passen
        #   bzw: ich darf nicht fuer mein haus verschluesselung aktivieren
        #       koennen und dann ein anderes haus abfragen koennen
        #       aber das geht eh nicht, weil dann die nonce nicht im cache
        #       gefunden wuerde. oder?

        haus = rpi.haus
        data = _serialize([haus])
        ret.append(data)

        hausprofil = haus.profil.all()
        data = _serialize(hausprofil)
        ret.append(data)

        etagen = haus.etagen.all()
        data = _serialize(etagen)
        ret.append(data)

        for etage in etagen:
            raeume = etage.raeume.all()

            for raum in raeume:
                data = _serialize([raum.regelung])
                ret.append(data)

            data = _serialize(raeume)
            ret.append(data)

        rg = haus.raumgruppen.all()
        data = _serialize(rg)
        ret.append(data)

        gateways = haus.gateway_related.all()
        data = _serialize(gateways)
        ret.append(data)

        for gateway in gateways:
            ausgaenge = gateway.ausgaenge.all()
            data = _serialize(ausgaenge)
            ret.append(data)

        sensoren = haus.sensoren.all()
        data = _serialize(sensoren)
        ret.append(data)

        vsensoren = haus.vsensoren.all()
        data = _serialize(vsensoren)
        ret.append(data)

        ret = rpi.crypt_keys.get().encrypt(ret)

    return HttpResponse(ret)


def get_all_users(request):
    '''
    Selbe Bedenken wie oben: kann ich eine CryptSession aufbauen
    und dann ein 'falsches' Haus abfragen mit meinen gueltigen
    Credentials?

    returns: {"pk": {"user": {NON-OBJECT-USRDATA}, "profile": {PROFILE}}}
    '''

    ret = dict()

    #if request.method == "GET":
    if request.method == "POST":

        rpi_name = request.POST.get("rpi", None)
        rpi = RPi.objects.get(name=rpi_name)

        haus = rpi.haus
        #haus = Haus.objects.get(pk=6368543146770432)

        usr = User.objects.filter(pk=haus.eigentuemer_id).values("username", "email", "password", "id", "is_active")[0]
        ret[usr["id"]] = dict()
        ret[usr["id"]]["user"] = usr
        data = _serialize([UserProfile.objects.get(user_id=haus.eigentuemer_id)])
        ret[usr["id"]]["profile"] = data

        for b in haus.besitzer:
            if b == haus.eigentuemer_id:
                continue
            usr = User.objects.filter(pk=b).values("username", "email", "password", "id", "is_active")[0]
            ret[b] = dict()
            ret[b]["user"] = usr
            data = _serialize(UserProfile.objects.get(user_id=b))
            ret[b]["profile"] = data

        ret = rpi.crypt_keys.get().encrypt(ret)

    return HttpResponse(str(ret))


def set_all(request):

    # verschluesselung muss zum objekt passen, ie. korrektes haus
    # was passiert, wenn entschluesselung fehlschlagt?
    #   wirft pycrypto einfach fehler.

    if request.method == "POST":

        encrypted_data = request.POST.get("data", "[]")
        encrypted_data = base64.b64decode(encrypted_data)

        rpi_name = request.POST.get("rpi", None)
        rpi = RPi.objects.get(name=rpi_name)
        first_sync = False
        if not rpi.haus_id:
            first_sync = True
        data = rpi.crypt_keys.get().decrypt(encrypted_data)

        # brauchen wir spaeter fuer _get_all_pk
        haus = None

        # hier kommen ID: Objekttyp Paare rein, um spaeter remote geloeschte Objekte zu identifizieren
        remote_objs = dict()

        # hier kommen ID: Obj Paare rein, so wie sie vom RPi kommen, damit
        #   wir foreign keys von abhaengigen Objekten uebersetzen koennen
        pktrans = dict()

        # prefill mit user_id
        u = request.POST.get("user", "")
        usr = User.objects.get(username=u.lower())
        profil = UserProfile.objects.get(user=usr)
        pktrans[profil.remote_user_id] = usr

        for obj, is_new in _deserialize(data):

            # pk und remote_id vertauschen
            if is_new:  # in django 1.4 musste man pk UND id auf None setzen...
                obj.pk, obj.remote_id = None, obj.pk
            else:
                obj.pk, obj.remote_id = obj.remote_id, obj.pk

            # alle Felder anschauen, ob sie auf ein anderes Objekt per FKey verweisen
            for field, val in vars(obj).items():
                if type(val) == list:
                    _val = list()
                    for v in val:
                        if v in pktrans:
                            _val += pktrans[v].pk
                    vars(obj)[field] = _val
                    # DAS HIER FUNKTIONIERT NICHT FUER HAUS.BESITZER

                elif val in pktrans:
                    vars(obj)[field] = pktrans[val].pk

            # jetzt speichern ...
            if isinstance(obj, Haus):
                obj.save()
                haus = obj
                if first_sync:
                    rpi.haus = haus
                    rpi.save()
            else:
                try:
                    obj.save()
                except ValueError:  # Gateways und Sensoren
                    if isinstance(obj, Gateway):
                        other = Gateway.objects.get(mac=obj.name)
                        other.delete()
                    elif isinstance(obj, Sensor):
                        other = Sensor.objects.get(name=obj.name)
                        other.delete()
                    obj.save()

            # ... und objekt in pktrans tun (erst nach dem speichern, weil sonst ID noch None sein koennte)
            pktrans[obj.remote_id] = obj

            # ... auch das erst hier, weil .pk ggf erst durch .save() gesetzt wird
            remote_objs[obj.pk] = type(obj)

        # was steht in der lokalen datenbank?
        local_objs = _get_all_pk(haus)

        # wenn wir jetzt lokal mehr Objekte haben als remote, dann wurde remote was geloescht
        if len(remote_objs) < len(local_objs):
            for pk, mdl in local_objs.items():
                if pk not in remote_objs:
                    model = _get_model(mdl)
                    instance = model.objects.get(pk=pk)
                    instance.delete()

        elif len(remote_objs) == len(local_objs):
            pass
        else:  # kann eigentlich nicht passieren
            logging.warning("!!! remote_objs > local_objs !!!")

    return HttpResponse()


def _get_all_pk(haus):

    ret = dict()

    ret[haus.pk] = type(haus)

    ret[haus.profil.get().pk] = type(haus.profil.get())

    for etage in haus.etagen.all():
        ret[etage.pk] = type(etage)
        for raum in etage.raeume.all():
            ret[raum.pk] = type(raum)
            ret[raum.regelung.pk] = type(raum)

    for rg in haus.raumgruppen.all():
        ret[rg.pk] = type(rg)

    for gw in haus.gateway_related.all():
        ret[gw.pk] = type(gw)
        for ausgang in gw.ausgaenge.all():
            ret[ausgang.pk] = type(ausgang)

    for sensor in haus.sensoren.all():
        ret[sensor.pk] = type(sensor)

    for vsensor in haus.vsensoren.all():
        ret[vsensor.pk] = type(vsensor)

    return ret


def set_object(request):
    # aufpassen auf replay
    # modelid mit tatsaechlicher modelid vergleichen
    # verschluesselung muss zum objekt passen, ie. korrektes haus

    if request.method == "POST":

        u = request.POST.get("user", "")
        usr = User.objects.get(username=u)  # brauchen wir unten noch

        encrypted_data = request.POST.get("data", "[]")
        rpi_name = request.POST.get("rpi", None)
        rpi = RPi.objects.get(name=rpi_name)
        first_sync = False
        if not rpi.haus_id:
            first_sync = True
        encrypted_data = base64.b64decode(encrypted_data)
        data = rpi.crypt_keys.get().decrypt(encrypted_data)

        for obj, is_new in _deserialize(data):  # wegen yield

            # pk und remote_id vertauschen
            if is_new:  # in django 1.4 musste man pk UND id auf None setzen...
                obj.pk, obj.remote_id = None, obj.pk
            else:
                obj.pk, obj.remote_id = obj.remote_id, obj.pk

            # check, obs FK Felder gibt
            for field, val in vars(obj).items():
                if field.endswith("_id") and field != "remote_id" and field != "remote_user_id":
                    try:
                        rel = obj._meta.get_field(field[:-3]).rel.to.objects.get(remote_id=val)
                    except FieldError:
                        # passiert z.B. bei eigentuemer auf User, weil User keine remote_id hat
                        rel = usr
                    vars(obj)[field] = rel.pk

                if type(val) == list:  # das sind jetzt ListFields mit FK
                    to = obj._meta.get_field(field).__dict__['item_field'].__dict__['rel'].to
                    _val = list()
                    for v in val:
                        try:
                            _val += to.objects.get(remote_id=v).pk
                        except FieldError:
                            # passiert z.B. bei User, s.o.
                            pass
                            # HIER MUSS NOCH WAS HER FUER HAUS.BESITZER!
                    vars(obj)[field] = _val

            # speichern
            if isinstance(obj, Haus):
                obj.save()
                haus = obj
                if first_sync:
                    rpi.haus = haus
                    rpi.save()
            else:
                try:
                    obj.save()
                except ValueError:  # Gateways und Sensoren
                    if isinstance(obj, Gateway):
                        other = Gateway.objects.get(mac=obj.name)
                        other.delete()
                    elif isinstance(obj, Sensor):
                        other = Sensor.objects.get(name=obj.name)
                        other.delete()
                    obj.save()

    return HttpResponse()


def set_user(request):
    '''
    wir duerfen uns hier keinen superuser injecten lassen!
    '''

    if request.method == "POST":

        encrypted_data = request.POST.get("data", "[]")
        encrypted_data = base64.b64decode(encrypted_data)
        rpi_name = request.POST.get("rpi", None)
        rpi = RPi.objects.get(name=rpi_name)
        data = rpi.crypt_keys.get().decrypt(encrypted_data)

        username = data["username"].decode("utf-8").lower()
        try:
            usr = User.objects.get(username=username)
        except User.DoesNotExist:
            usr = User(username=username, email=username)

        # email waere die neue adresse, wenn geaendert
        email = data["email"].decode("utf-8").lower()
        try:
            nusr = User.objects.get(username=email)
            raise Exception("user already exists")
        except User.DoesNotExist:
            pass

        if email != username:
            usr.username = email
            usr.email = email

        pw = data["password"].decode("utf-8")
        usr.set_password(pw)
        usr.save()

        # remote_ids abgleichen
        profil = UserProfile.objects.get(user=usr)
        remote_usr_id = data["remote_usr_id"]
        remote_profil_id = data["remote_profil_id"]
        profil.remote_id = remote_profil_id
        profil.remote_user_id = remote_usr_id
        profil.save()

    return HttpResponse()


def del_object(request):
    # das kann sicher noch irgendeinen anderen lustigen Fehler werfen?
    # request.POST muss halt verschluesselt sein
    # und die verschluesselung muss zum objekt passen, i.e. korrektes haus
    # kann keine benutzer loeschen

    encrypted_data = request.POST.get("data", "[]")
    encrypted_data = base64.b64decode(encrypted_data)
    rpi_name = request.POST.get("rpi", None)
    rpi = RPi.objects.get(name=rpi_name)
    d = rpi.crypt_keys.get().decrypt(encrypted_data)

    model = _get_model(d["modelname"])

    try:
        instance = model.objects.get(remote_id=d["modelid"])
        instance.delete()
    except model.DoesNotExist:
        pass

    return HttpResponse()


def get_update(request):
    hausid = request.POST.get("hausid", None)
    updb = ch.update_db(hausid)
    upduser = ch.update_db_user(hausid)
    ret = "<updb>"
    if updb:
        ch.del_update_db(hausid)
        ret += "<ent/>"
    if upduser:
        ch.del_update_db_user(hausid)
        ret += "<usr/>"
    ret += "</updb>"
    return HttpResponse(ret)


def _serialize(queryset):

    json_serializer = serializers.get_serializer("json")()

    json_serializer.serialize(queryset)
    data = json_serializer.getvalue()

    return data


def _deserialize(data):

    # umbenannte Felder sind ein Problem, alles andere geht
    #  einfach verloren

    import json
    import django.core.serializers.python as python_deserializer

    python_deserializer._get_model = _get_model  # geben wir dem deserializer unsere eigene _get_model()

    for objstr in data:
        try:
            j = json.loads(objstr)

            for obj in python_deserializer.Deserializer(j, ignorenonexistent=True):

                if _is_new(j):
                    yield obj.object, True
                else:
                    yield obj.object, False

        except ValueError:  # fehler im string
            logging.error("ValueError in %s" % objstr)
        except base.DeserializationError:  # fehler im objekt
            logging.error("DeserializationError in %s" % objstr)
        except TypeError:
            logging.error("TypeError in %s" % objstr)


def _get_model(model_identifier):
    mdls = import_module('models')

    try:
        modelname = model_identifier
        if '.' in model_identifier:
            modelname = model_identifier.rsplit('.', 1)[1]
        model = getattr(mdls, tdict[modelname] if modelname in tdict else modelname.title())
    except AttributeError:  # kein Model mit diesem Namen gefunden
        raise base.DeserializationError("Invalid model identifier: '%s'" % model_identifier)
    # except TypeError:  # z.B. "argument of type 'ModelBase' is not iterable
    #     raise base.DeserializationError("argument of type 'ModelBase' is not iterable: '%s'" % model_identifier)

    return model


def _is_new(jsonobj):
    """
    Es gaebe auch den einfacheren Check, ob die (zuvor
    vertauschte?) remote_id 0 bzw -1 ist oder eben nicht
    """
    model = _get_model(jsonobj[0]["model"])
    try:
        if model == User:
            UserProfile.objects.get(remote_user_id=long(jsonobj[0]["fields"]["id"]))
        else:
            model.objects.get(pk=long(jsonobj[0]["fields"]["remote_id"]))
        return False
    except model.DoesNotExist:  # im Server Branch wird hier noch ein DatabaseError gefangen
        return True