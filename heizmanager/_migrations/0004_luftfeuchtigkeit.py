# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import caching.base


class Migration(migrations.Migration):

    dependencies = [
        ('heizmanager', '0003_rfprotocol'),
    ]

    operations = [
        migrations.CreateModel(
            name='Luftfeuchtigkeitssensor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('description', models.CharField(max_length=64)),
                ('mainsensor', models.BooleanField(default=False)),
                ('wert', models.FloatField(null=True)),
                ('offset', models.FloatField(null=True, blank=True)),
                ('linie', models.IntegerField(default=0)),
                ('gateway', models.ForeignKey(related_name=b'lfsensoren', to='heizmanager.Gateway', null=True)),
                ('haus', models.ForeignKey(related_name=b'luftfeuchtigkeitssensor_related', to='heizmanager.Haus', null=True)),
            ],
            options={
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.AddField(
            model_name='luftfeuchtigkeitssensor',
            name='raum',
            field=models.ForeignKey(related_name=b'luftfeuchtigkeitssensoren', to='heizmanager.Raum', null=True),
            preserve_default=True,
        ),
    ]