# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('heizmanager', '0002_gwparams'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rfaktor',
            name='protocol',
            field=models.CharField(max_length=16, choices=[(b'zwave', b'ZWave'), (b'btle', b'Bluetooth')]),
        ),
        migrations.AlterField(
            model_name='rfcontroller',
            name='protocol',
            field=models.CharField(max_length=16, choices=[(b'zwave', b'ZWave'), (b'btle', b'Bluetooth')]),
        ),
        migrations.AlterField(
            model_name='rfsensor',
            name='protocol',
            field=models.CharField(max_length=16, choices=[(b'zwave', b'ZWave'), (b'btle', b'Bluetooth')]),
        ),
    ]