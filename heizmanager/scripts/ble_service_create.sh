#!/usr/bin/bash

SERVICE_NAME=$1
DESCRIPTION='test'
SERVICE_PATH=$2
TIMMER=$3

# remove the double quotes
DESCRIPTION=${DESCRIPTION//'"'/}
SERVICE_NAME=${SERVICE_NAME//'"'/}
SERVICE_PATH=${SERVICE_PATH//'"'/}


sudo systemctl stop $SERVICE_NAME'.service'
sudo systemctl disable $SERVICE_NAME'.service'
sudo systemctl stop $SERVICE_NAME'.timer'
sudo systemctl disable $SERVICE_NAME'.timer'


sudo cat > /etc/systemd/system/${SERVICE_NAME//'"'/}.service << EOF
[Unit]
Description=$DESCRIPTION
[Service]
Type=oneshot
ExecStart=$SERVICE_PATH
EOF

sudo cat > /etc/systemd/system/${SERVICE_NAME//'"'/}.timer << EOF
[Unit]
Description=timer

[Timer]
OnBootSec=$TIMMER min
OnUnitActiveSec=$TIMMER min

[Install]
WantedBy=timers.target
EOF

# restart daemon, enable and start service
echo "Reloading daemon and enabling service"
sudo systemctl daemon-reload 
sudo systemctl enable $SERVICE_NAME'.timer'
sudo systemctl start $SERVICE_NAME'.timer'
sudo systemctl daemon-reload 
echo "Service Started"

exit 0
