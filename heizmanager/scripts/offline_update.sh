#!/usr/bin/bash
location=$1
file=${location//'"'/}/rpi
HOME=/home/pi
branch=offline_$(date +%Y%m%d)
if [ -e "$file" ]; then
    if [ $(ls $file | wc -l) -eq 1 ]; then
        echo -e "Checking unziped file: 1/3"
        echo $(date)
    else
        echo "It seems that your unzip file is not in it's subfolder!"
        exit
    fi
    if [ $(ls $file/*/ | wc -l) -gt 1 ]; then
        echo -e "Checking unziped file: 2/3"
        if [ -e $file/*/db.sqlite3 ]; then
            echo "Are you uploading a zip file that contains Database?? :("
            exit
        else
            echo -e "Checking unziped file: 3/3"
        fi
    else
        echo "We can't install because you have 2 subfolders!"
        exit
    fi
    echo -e "Step1: config git user and checkout to beta:"
    sudo git config --global user.name "offline_update"
    sudo git config --global user.email "update@controme.com"
    cd $HOME/rpi && sudo git checkout beta
    cd $HOME/rpi && find $HOME/rpi -name "*.pyc" -exec rm -f {} \;
    echo -e "DONE.\n\n"

    echo $(date)
    echo -e "Step2: create new branch named offline_date:" 
    cd $HOME/rpi && sudo git checkout -B $branch
    echo -e "DONE.\n\n"
    echo -e "Step3: copy files:"
    # Get backup from important files
    mkdir -p $HOME/source_backup
    mkdir -p $HOME/source_backup/rpi
    sudo cp $HOME/rpi/db.sqlite3 $HOME/source_backup
    sudo cp $HOME/rpi/rpi/{aeskey.py,server.py,secret_key.py,verification_code.py} $HOME/source_backup/rpi
    sudo chmod -R 777 $HOME/source_backup
    # Copy new branch into rpi folder
    sudo cp -r $file/*/*  $HOME/rpi

    # Disabled 
    #sudo cp -r $HOME/source_backup/* $HOME/rpi
    
    sudo chmod -R 777 $HOME/rpi
    echo -e "DONE.\n\n"

    echo $(date)
    echo -e "Step4: add files to branch offline_date:\n"
    cd $HOME/rpi && sudo git add .
    echo -e "DONE.\n\n"

    echo $(date)
    echo -e "Step5: commit all changes to branch offline_date:\n"
    cd $HOME/rpi && sudo git commit -m $branch
    echo -e "DONE.\n\n"

    echo $(date)
    echo -e "Step6: restart supervisor:\n"
    sudo supervisorctl -c /etc/supervisor/supervisord.conf restart all
    echo -e "DONE.\n\n"

    echo -e "End of log file  " $(date)
else 
    echo "File does not exist"
fi 
