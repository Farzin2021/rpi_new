# -*- coding: utf-8 -*-

from heizmanager.render import render_response, render_redirect
from heizmanager.models import HausProfil, Haus, Raum, AbstractSensor
from raumgruppen.models import Raumgruppe
from datetime import datetime, timedelta
import heizmanager.cache_helper as ch
import logging
from django.http import HttpResponse
import pytz
import requests
from heizmanager.mobile.m_setup import set_regelung_entity, get_regelung_entity

def get_name():
    return u'Google Kalender'


def is_togglable():
    return True


def get_cache_ttl():
    return 500


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/"+str(haus.id)+"/gcal/'>Google Kalender</a>"


def get_global_description_link():
    desc = u"Temperaturen durch Kalendereinträge in einem Google-Kalender steuern."
    desc_link = "http://support.controme.com/auswahl-und-aktivierung-von-modulen/modul-gkalender/"
    return desc, desc_link


def get_global_settings_page(request, haus):
    params = haus.get_module_parameters()
    
    if request.user.userprofile.get().role == 'K':
        accesserror = "Zugriff auf Kalendereinstellungen nicht gestattet."
        return render_response(request, "m_settings_cal.html", {"haus": haus, "accesserror": accesserror})
    
    if request.method == "GET":
        if 'getcal' in request.GET:
            ch.delete(str(haus.id)+'events')
            haus_offsets, raum_offsets, unattributed = get_offset(haus, raum=None, return_matches=True)
            
            ret = "<br/><p>Aktuell aktive Kalendereintr&auml;ge</p>" \
                  "<h5>Hausweite Eintr&auml;ge</h5>"
            for u in haus_offsets:
                ret += "'%s'<br/>" % u
            if not len(haus_offsets):
                ret += "keine.<br/>"
        
            ret += "<h5>Raumspezifische Eintr&auml;ge</h5>"
            summaries = dict()
            r_offsets = dict()
            raumgruppen = [r.name.lower() for r in Raumgruppe.objects.filter(haus=haus)]
            
            for k, m in raum_offsets.items():
                new_m = []
                for offset, etage, raum, summary in m:
                    if summary.split(' ', 1)[1].lower() in raumgruppen:
                        summaries.setdefault(summary, list()).append((raum, etage))
                    else:
                        new_m.append((offset, etage, raum, summary))
                r_offsets[k] = new_m
            raum_offsets = r_offsets

            for summary, raeume in summaries.items():
                if len(raeume) < 2:
                    continue
                ret += \
                    "'%s' f&uuml;r Raumgruppe %s f&uuml;r folgende R&auml;ume:<br/>" \
                    % (summary, summary.split(' ', 1)[1])
                ret += '<p><br/>'
                for raum, etage in raeume:
                    ret += \
                        "&nbsp;&nbsp;&nbsp;<i>%s</i> in Etage <i>%s</i><br/>" \
                        % (str(raum).decode('utf-8'), str(etage).decode('utf-8'))
                ret += '</p>'

            for m in raum_offsets.values():
                for offset, etage, raum, summary in m:
                    ret += "'%s' f&uuml;r Raum <i>%s</i> in Etage <i>%s</i><br/>" % (summary, raum, etage)
            if not len(raum_offsets) and not len(summaries):
                ret += "keine.<br/>"

            exist_diff, exist_vor = False, False
            hparams = haus.get_module_parameters()
            diff_params = hparams.get('differenzregelung', {})
            vor_params = hparams.get('vorlauftemperaturregelung', {})
            u_list = []
            diff_list = []
            vor_list = []
            for u in unattributed:
                try:
                    u_list.append(u[0])
                except:
                    pass

            for id, params in diff_params.iteritems():
                if 'gcal' in params.get('modules_list', {}):
                    exist_diff = True
                diff_list.append(params['name'])

            for id, params in vor_params.iteritems():
                if 'gcal' in params.get('modules_list', {}):
                    exist_vor = True
                vor_list.append(params['name'])
            diff_list_show = []
            vor_list_show = []

            for d in diff_list:
                for item in u_list:
                    if item.split(' ', 1)[1].lower() == d.lower():
                        diff_list_show.append(item)
                        u_list.remove(item)

            for v in vor_list:
                for item in u_list:
                    if item.split(' ', 1)[1].lower() == v.lower():
                        vor_list_show.append(item)
                        u_list.remove(item)

            if exist_diff:
                ret += "<h5>Differenzregelung</h5>"
                if not len(diff_list_show):
                    ret += "keine.<br/>"
                else:
                    for d in diff_list_show:
                        ret += d + ".<br/>"
            if exist_vor:
                ret += "<h5>Vorlauftemperaturregelung</h5>"
                if not(len(vor_list_show)):
                    ret += "keine.<br/>"
                else:
                    for v in vor_list_show:
                        ret += v + ".<br/>"

            ret += "<h5>Unzugeordnete Eintr&auml;ge</h5>"
            for u in u_list:
                try:
                    ret += u"'%s'<br/>" % u
                except TypeError:
                    pass

            if not len(u_list):
                ret += "keine.<br/>"

            return HttpResponse(ret)
        
        else:
            calendar_id = params.get('gcal', dict()).get('calendarId') or params.get('kalender', dict()).get('calendarId')
            active = False
            if 'gcal' in haus.get_modules():
                active = True
            return render_response(request, "m_settings_cal.html",
                                   {"haus": haus, "calendarId": calendar_id, "active": active})
        
    elif request.method == "POST" and request.user.userprofile.get().role != 'K':
        if 'clickflag' in request.POST and request.POST['clickflag'] == '1':
            
            if 'usergooglecal' in request.POST and len(request.POST['usergooglecal']):
                usergooglecal = request.POST['usergooglecal'].strip()
    
                try:
                    _ = _get_current_events(usergooglecal)  # schauen, ob Zugriff funktioniert
                except TypeError:
                    return render_response(request, "m_settings_cal.html",
                                           {'error': 'Zugriff auf Kalender nicht moeglich. Haben Sie mit uns geteilt? '
                                                     'ID korrekt eingegeben?', "haus": haus})

                params.setdefault('gcal', dict())
                params['gcal']['calendarId'] = usergooglecal
                haus.set_module_parameters(params)
    
            return render_redirect(request, "/m_setup/%s/gcal/" % str(haus.id))
        
        else:
            return render_redirect(request, "/m_setup/%s/gcal/" % haus.id)


def get_global_settings_page_help(request, haus):
    return render_response(request, "m_help_gcal.html", {'haus': haus})


def get_local_settings_link(request, raum):
    if not 'gcal' in raum.etage.haus.get_modules():
        return ""

    if 'gcal' in raum.get_modules():
        haus_offset = ch.get_module_offset_haus('gcal', raum.etage.haus.id)
        raum_offset = ch.get_module_offset_raum('gcal', raum.etage.haus.id, raum.id)
        if raum_offset is None:
            raum_offset = 0.0
        if haus_offset is None:
            haus_offset = 0.0
        offset = "%.2f" % (haus_offset + raum_offset)
    else:
        offset = "deaktiviert"

    return "Google Kalender", offset, 75, "/m_raum/%s/gcal/" % raum.id


def get_local_settings_page(request, raum):
    params = raum.etage.haus.get_module_parameters()
    mods = raum.etage.haus.get_modules()
    active = False
    if 'gcal' in raum.get_modules():
        active = True
    gactive = False
    if 'gcal' in mods:
        gactive = True
    try:
        calendar_id = params['gcal']['calendarId']
    except KeyError:
        calendar_id = ''
    
    if request.method == "GET":
        if gactive:
            matches = ''
            if not len(calendar_id):
                matches = "Kein Kalender verknüpft."
            else:
                haus_offsets, raum_offsets, unattributed = get_offset(raum.etage.haus, return_matches=True)
                matches += \
                    'Hausweiter Kalenderoffset:<br/>&nbsp;&nbsp;&nbsp; %s<br/>' % float(sum(haus_offsets.values()))
                if raum.id in raum_offsets:
                    matches += "Raumspezifische Kalendereintr&auml;ge:<br/>"
                    for offsets in raum_offsets[raum.id]:
                        matches += "&nbsp;&nbsp;&nbsp;" + offsets[3] + '<br/>'
                if not len(matches):
                    matches += 'keine Kalendereinträge gefunden.'
            return render_response(request, "m_raum_cal.html",
                                   {"haus": raum.etage.haus, "raum": raum, "matches": matches, "active": active,
                                    'gactive': gactive})
        else:
            return render_response(request, "m_raum_cal.html",
                                   {"haus": raum.etage.haus, "raum": raum, "active": active, 'gactive': gactive})
    if request.method == "POST" and request.user.userprofile.get().role != 'K':
        if 'gcal' in request.POST and request.POST['gcal'] == '1':
            if not 'gcal' in raum.get_modules():
                raum.set_modules(raum.get_modules() + ['gcal'])
        else:
            mods = raum.get_modules()
            if 'gcal' in mods:
                mods.remove('gcal')
                raum.set_modules(mods)
        return render_redirect(request, "/m_raum/%s/" % str(raum.id))


def get_local_settings_page_regelung(request, haus, modulename, objid, params):
    mods = haus.get_modules()
    hparams = haus.get_module_parameters()
    active = False
    if params:
        active = True
    gactive = False
    if 'gcal' in mods:
        gactive = True

    if request.method == "GET":
        matches = ''
        reg_obj = get_regelung_entity(haus, objid, modulename)
        page_params = {}
        page_params.update({
            "haus": haus,
            "regelung_obj": reg_obj,
            "regelung": modulename,
            "objid": objid,
            "active": active,
            "gactive": gactive,
            "matches": matches
        })
        return render_response(request, "m_regelung_gcal.html", page_params)

    if request.method == "POST":
        if 'gcal' in request.POST and request.POST['gcal'] == '1':
            set_regelung_entity(haus, modulename, objid, {"active": True}, "gcal")
        else:
            set_regelung_entity(haus, modulename, objid, {}, "gcal")
        return render_redirect(request, "/m_%s/%s/show/%s/" % (modulename, haus.id, objid))


def get_offset(haus, raum=None, return_matches=False):
    hausprofil = HausProfil.objects.get(haus=haus)
    params = hausprofil.get_module_parameters()

    if 'gcal' in params and 'calendarId' in params['gcal'] and params['gcal']['calendarId'] != '' \
            and 'gcal' in hausprofil.get_modules() and (raum is None or 'gcal' in raum.get_modules()):

        etagenundraeume = dict()
        for etage in haus.etagen.all():
            raeume = Raum.objects.filter_for_user(haus.eigentuemer, etage_id=etage.id).values_list('id', 'name')
            etagenundraeume[etage.name.lower()] = set([(_raum[0], _raum[1].lower()) for _raum in raeume])

        events = _get_calendar(haus.id)
        logging.warning(events)

        gmt = pytz.timezone('GMT')
        now = datetime.now(gmt)

        atemp = None

        haus_offsets = dict()
        raum_offsets = dict()
        unattributed = list()
        
        for event_id, (summary, start, end) in events.items():
            try:  # es ist ein haus offset, wenn man die komplette Summary nach float casten kann

                offset = float(summary.replace(',', '.'))
                logging.warning("getting offset for event: %s from %s to %s" % (summary, start, end))
                offset = _get_shifted_offset(offset, start, end, now, atemp, raumid=None)

                if offset is not None:
                    haus_offsets[summary] = offset
                del events[event_id]

            except ValueError:
                pass
        
        for etage, raeume in etagenundraeume.items():
            for rid, _raumname in raeume:
                for event_id, (summary, start, end) in events.items():

                    s = summary.replace(',', '.').lower().split(' ')

                    try:
                        offset = float(s[0])
                    except ValueError:
                        if start <= now <= end:
                            unattributed.append(summary)
                        del events[event_id]
                        continue
                    
                    if sum([1 for r in set(_raumname.split(' ')) if r in s]) == len(set(_raumname.split(' '))) \
                            and sum([1 for e in set(etage.split(' ')) if e in s]) == len(set(etage.split(' '))):

                        logging.warning("getting offset for event: %s from %s to %s" % (summary, start, end))
                        offset = _get_shifted_offset(offset, start, end, now, atemp, raumid=rid)
                        if offset is not None:
                            raum_offsets.setdefault(rid, list()).append((offset, etage, _raumname, summary))
                        del events[event_id]
        
        for raumgruppe in Raumgruppe.objects.filter(haus=haus):
            for event_id, (summary, start, end) in events.items():

                s = summary.replace(',', '.').lower()
                o = float(s.split(' ')[0])

                if raumgruppe.name.lower() in s:

                    for raum_id in raumgruppe.raeume:

                        logging.warning("getting offset for event: %s (->%s) from %s to %s" % (summary, o, start, end))
                        offset = _get_shifted_offset(o, start, end, now, atemp, raumid=raum_id)
                        if offset is not None:
                            _raum = Raum.objects.get(pk=long(raum_id))
                            raum_offsets.setdefault(int(raum_id), list()).append((offset, _raum.etage, _raum, summary))

                    del events[event_id]
        
        unattributed.extend(events.values())
        
        ch.set_module_offset_haus('gcal', haus.id, sum(haus_offsets.values()), ttl=get_cache_ttl())
        for etage, raeume in etagenundraeume.items():
            for rid, _raum in raeume:
                if rid in raum_offsets:
                    ch.set_module_offset_raum(
                        'gcal', haus.id, rid, sum([o for o, e, r, s in raum_offsets[rid]]), ttl=get_cache_ttl()
                    )
                else:
                    ch.set_module_offset_raum('gcal', haus.id, rid, 0.0, ttl=get_cache_ttl())

        if raum:
            if raum.id not in raum_offsets:
                logging.warning("%s %s" % (str(raum.id), raum_offsets))
                coffset = 0.0
            else:
                coffset = sum([r[0] for r in raum_offsets[raum.id]])
            ch.set_module_offset_raum('gcal', haus.id, raum.id, coffset, ttl=get_cache_ttl())
            return coffset
        elif return_matches:
            return haus_offsets, raum_offsets, unattributed
        else:
            ch.set_module_offset_haus('gcal', haus.id, sum(haus_offsets.values()), ttl=get_cache_ttl())
            return sum(haus_offsets.values())
        
    else:
        ch.set_module_offset_haus('gcal', haus.id, 0.0, ttl=get_cache_ttl())
        if raum:
            ch.set_module_offset_raum('gcal', haus.id, raum.id, 0.0, ttl=get_cache_ttl())
        elif return_matches:
            return [], {}, []
        return 0.0


def get_offset_regelung(haus, modulename, objid):

    offset = ch.get_module_offset_regelung('gcal', haus.id, modulename, objid)
    if offset:
        return offset
    
    hausprofil = HausProfil.objects.get(haus=haus)
    hparams = hausprofil.get_module_parameters()
    r_params = hparams.get(modulename, {})
    obj = r_params.get(objid, {})
    module_params = obj.get('module_params', {})
    offset = 0
    if 'gcal' in module_params:
        if module_params['gcal']:
            if 'gcal' in hparams and 'calendarId' in hparams['gcal'] and hparams['gcal']['calendarId'] != '':
                events = _get_calendar(haus.id)
                for event_id, (summary, start, end) in events.items():
                    if 'name' in obj:
                        splited = summary.split(" ", 1)
                        if len(splited) > 1:
                            if obj['name'].lower() == splited[1].lower():
                                offset = splited[0].replace(',', '.')
                                try:
                                    offset = float(offset)
                                except:
                                    offset = 0.0
                                ch.set_module_offset_regelung('gcal', haus.id, modulename, objid, offset, ttl=get_cache_ttl())
                                break
    return offset


def _get_shifted_offset(offset, start, end, now, atemp, raumid=None):
    from django.utils.dateparse import parse_datetime
    is_active = False

    toffsetstart = 15
    toffsetend = 15
    ist = ziel = dk = dt = None

    if isinstance(start, str):
        start = parse_datetime(start)
    if isinstance(end, str):
        end = parse_datetime(end)

    try:
        if start-timedelta(minutes=toffsetstart) <= now <= end-timedelta(minutes=toffsetend):
            is_active = True
    except TypeError:
        raise Exception("wat .. %s %s %s" % (offset, start, end))

    logging.warning("ist: %s | ziel: %s | now: %s | atemp: %s | dt: %s | "
                    "dk: %s | toffsetstart: %s | toffsetend: %s | active: %s"
                    % (ist, ziel, now.time(), atemp, dt, dk, toffsetstart, toffsetend, is_active))

    if is_active:
        return offset  # wenn ein Event verschoben aktiv ist, dann kommt einfach der offset zurueck
    return None


def is_hidden_offset():
    return False


def _get_calendar(hausid):
    events = ch.get(str(hausid)+'events')
    if events is None:
        haus = Haus.objects.get(pk=long(hausid))
        events = {}
        if 'gcal' in haus.get_modules():
            params = haus.get_module_parameters()
            if len(params['gcal'].get('calendarId', '')):
                try:
                    events = _get_current_events(params['gcal']['calendarId'])
                    ch.set(str(hausid)+'events', events, time=600)
                except Exception as e:
                    logging.warning(e)
                    pass

    return events


def _get_current_events(calendar_id):
    import socket
    try:
        s = socket.create_connection(("8.8.8.8", 53), 1)
    except Exception as e:
        logging.exception("socket error")
        return {}

    import ast
    from django.utils.dateparse import parse_datetime
    data = {'calid': calendar_id}
    try:
        r = requests.post("https://controme-main.appspot.com/calendar/get/", data=data, timeout=5)
    except:
        return {}
    try:
        events = ast.literal_eval(r.content)
    except SyntaxError:
        raise TypeError
    except ValueError:
        raise TypeError
    ret = {}
    for event_id, (desc, begin, end) in events.items():
        ret[event_id] = (desc, parse_datetime(begin), parse_datetime(end))
    logging.warning(ret)
    return ret


def get_local_settings_link_regelung(haus, modulename, objid):
    offset = get_offset_regelung(haus, modulename, objid)
    return get_name(), 'google-kalender', offset, 75, '/m_%s/%s/show/%s/gcal/' % (modulename, haus.id, objid)
