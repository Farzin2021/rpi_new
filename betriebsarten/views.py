# -*- coding: utf-8 -*-

import logging
from heizmanager.render import render_response, render_redirect
from heizmanager.models import Raum, Haus, Regelung
from .tasks import periodic_betriebsarten_evaluation
import heizmanager.cache_helper as ch
import pytz
from datetime import datetime
from operator import itemgetter
from django.http import JsonResponse
from heizmanager.mobile.m_setup import set_regelung_entity, get_regelung_entity


logger = logging.getLogger('logmonitor')


def get_name():
    return u'Betriebsarten heizen und kühlen'


def is_togglable():
    return True


def get_cache_ttl():
    return 3600


def calculate_always_anew():
    return False


def is_hidden_offset():
    return False


def get_right_menu_module_link(haus):
    hparams = haus.get_module_parameters()
    return True if 'betriebsarten' in hparams and \
                   'active_params' in hparams['betriebsarten'] and\
                   hparams['betriebsarten']['active_params'] == 1 else False


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/betriebsarten/'>Betriebsarten</a>" % str(haus.id)


def get_global_description_link():
    desc = u"Ermöglicht die Umschaltung der Betriebsarten heizen und kühlen."
    desc_link = "https://support.controme.com/betriebsarten-heizen-und-kuehlen/"
    return desc, desc_link


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    context = dict()
    b_params = haus.get_spec_module_params('betriebsarten')
    b_items = b_params.get('items', dict())

    if request.method == "GET":
        context['haus'] = haus
        context['b_params'] = b_params

        if action == 'edit':

            # collecting fps
            regs = Regelung.objects.filter(regelung="fps")
            reg_rows = []
            for reg in sorted(regs, key=lambda x: x.get_parameters()['name']):
                reg_rows.append((str(reg.pk), reg.get_parameters()['name']))
            context['regs'] = reg_rows

            # collecting room
            eundr = list()
            for etage in haus.etagen.all():
                e = {etage: []}
                for _raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    e[etage].append(_raum)
                eundr.append(e)
            context['eundr'] = eundr

            if entityid:
                b_param = b_items.get(entityid)
                context['b_param'] = b_param
                context['entity_id'] = entityid
            return render_response(request, "m_settings_betriebsarten_edit.html", context)

        if action == 'delete':
            if entityid in b_items:
                del b_items[entityid]
                b_params['items'] = b_items
                haus.set_spec_module_params('betriebsarten', b_params)
            return render_redirect(request, '/m_setup/%s/betriebsarten/' % haus.id)

        if action == 'calculation':
            periodic_betriebsarten_evaluation()
            return render_redirect(request, '/m_setup/%s/betriebsarten/' % haus.id)
        try:
            if 'betriebsarten' in haus.get_modules():
                if get_params(haus) == 1:
                    context['active'] = True
                context['b_items'] = b_items.items()
            return render_response(request, "m_settings_betriebsarten.html", context)
        except Exception as e:
            logger.warning(str(e))
            return render_redirect(request, 'https://support.controme.com/betriebsarten-heizen-und-kuehlen/')

    elif request.method == "POST":

        if action == 'edit':
            # preparing data
            data_d = dict()
            data_d['name'] = request.POST.get('name', '')
            data_d['fps_abfrage'] = request.POST.get('fps_abfrage')
            data_d['cooling'] = request.POST.get('cooling')
            rooms_lst = request.POST.getlist('rooms')
            data_d['rooms'] = rooms_lst

            # get existing data or define new id
            if entityid is None:
                ids = [0] + [int(k) for k in b_items.keys()]
                entityid = str(max(ids) + 1)
                b_items[entityid] = dict()

            # saving data
            b_items[entityid].update(data_d)
            b_params['items'] = b_items
            haus.set_spec_module_params('betriebsarten', b_params)
            return render_redirect(request, '/m_setup/%s/betriebsarten/' % haus.id)

        if 'right_menu_ajax' in request.POST:
            activeinMenu = bool(int(request.POST['right_menu_ajax']))
            set_params(haus, activeinMenu)
            return JsonResponse({'message': 'done'})

        return render_redirect(request, '/m_setup/%s/betriebsarten/' % haus.id)


def get_global_settings_page_help(request, haus):
    return render_redirect(request, 'https://support.controme.com/betriebsarten-heizen-und-kuehlen/')
   

def get_local_settings_page_haus(request, haus):    
    if request.method == "GET":        
        eundr = []
        for etage in haus.etagen.all():
            e = {etage: []}
            for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                raum.mode = str(raum.get_betriebsart())
                e[etage].append(raum)
            eundr.append(e)

        return render_response(request, "m_betriebsarten.html", {"eundr": eundr, "haus": haus})

    elif request.method == "POST":
        eundr = []
        for etage in haus.etagen.all():
            e = {etage: []}
            for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                raum.mode = str(request.POST.get("group%s" % raum.id,0))
                e[etage].append(raum)
                raum.set_betriebsart(str(request.POST.get("group%s" % raum.id, 0)))
            eundr.append(e)
                
        return render_response(request, "m_betriebsarten.html", {"eundr": eundr, "haus": haus})


def get_local_settings_link(request, raum):
    pass


def get_params(hausoderraum):    
    try:
        params = hausoderraum.get_module_parameters()
        return params['betriebsarten']['active_params']
    except Exception as e:
        return 0


def set_params(hausoderraum, active):
    try:
        params = hausoderraum.get_module_parameters()
        params.setdefault('betriebsarten', dict())['active_params'] = active
        hausoderraum.set_module_parameters(params)
    except Exception:
        pass


def activate(hausoderraum):
    if not 'betriebsarten' in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['betriebsarten'])
