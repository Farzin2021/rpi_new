from django import template

register = template.Library()


@register.filter(name='ret_name')
def ret_name(d, val):
    key = val+'_name'
    if key in d:
        return d[key]
    return


@register.filter(name='ret_show_slider')
def ret_show_slider(d, val):
    return val+'_show_slider' in d and d[val+'_show_slider']
