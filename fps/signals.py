from heizmanager.models import sig_get_outputs_from_regelung, sig_get_possible_outputs_from_regelung, sig_create_regelung, sig_create_output_regelung, Raum
from django.dispatch import receiver
import fps.views as fps
import heizmanager.cache_helper as ch
import logging


@receiver(sig_get_outputs_from_regelung)
def fps_get_outputs(sender, **kwargs):
    haus = kwargs['haus']
    raum = kwargs['raum']
    regelung = kwargs['regelung']
    return fps.get_outputs(haus, raum, regelung)


@receiver(sig_get_possible_outputs_from_regelung)
def fps_get_possible_outputs(sender, **kwargs):
    haus = kwargs['haus']
    return fps.get_possible_outputs(haus)


@receiver(sig_create_regelung)
def fps_create_regelung(sender, **kwargs):
    # called when a sensor is added or deleted and requires changes to a regelung
    # currently not necessary here
    pass


@receiver(sig_create_output_regelung)
def fps_create_output(sender, **kwargs):
    # currently not necessary
    pass
